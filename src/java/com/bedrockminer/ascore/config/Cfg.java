package com.bedrockminer.ascore.config;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import com.bedrockminer.ascore.units.FluidVolume;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.RotationSpeed;
import com.bedrockminer.ascore.units.RotationalInertia;
import com.bedrockminer.ascore.units.Temperature;
import com.bedrockminer.ascore.units.Time;
import com.bedrockminer.ascore.units.Torque;
import com.bedrockminer.ascore.units.Volume;
import com.bedrockminer.ascore.util.Log;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

public final class Cfg {

	public static Configuration config;
	public static final int CONFIG_VERSION = 1;

	public static Property version;

	// Properties
	public static Property circuitAssemblyWarn;

	public static Property unicodeInInfo;

	public static Property unitVolume;
	public static Property unitFluidVolume;
	public static Property unitPressure;
	public static Property unitTemperature;
	public static Property unitTime;
	public static Property unitRotationSpeed;
	public static Property unitTorque;
	public static Property unitRotInertia;

	public static void init(File file) {
		createConfig(file);
		// The general category is not visible in the config gui.
		config.setCategoryComment("general", "This is the configuration file for Applied Science.\nOnly change values if you know what you're doing.\nYou can edit this file more easily from the ingame GUI.");

		// ----------------
		// Config entries:

		// Server configuration
		config.setCategoryComment("server", "The server settings edit the server's behaviour.\nThey have no effect on the client.");
		config.setCategoryLanguageKey("server", "config.as.category_server");

		circuitAssemblyWarn = config.get("server", "circuit_assembly_warn", 10.0, "If the assembly of a circuit takes longer than this value (in ms) a warning will be printed\ninto the log. (Min: 1.0, Max: 2000.0)", 1.0, 2000.0).setLanguageKey("config.as.circuit_assembly_warn");

		// Client only
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
			// Client configuration
			config.setCategoryComment("client", "The client settings only affect client side.\nThis category contains settings that don't fit into another sub-category.");
			config.setCategoryLanguageKey("client", "config.as.category_client");

			unicodeInInfo = config.get("client", "unicode_in_info", true, "If this is set to true, info tabs in the GUI will use unicode font instead of default font.").setLanguageKey("config.as.unicode_in_info");

			// Unit configuration
			config.setCategoryComment("units", "The unit settings define which units are used to display the measures of the machines.\nThey only affect client side.");
			config.setCategoryLanguageKey("units", "config.as.category_units");

			unitVolume = config.get("units", "volume", "Cubicmeter", "The unit to display volumes.\nPossibilities: " + Arrays.toString(Volume.getNames()), Volume.getNames()).setLanguageKey("config.as.unit_volume");
			unitFluidVolume = config.get("units", "fluid_volume", "Cubicmeter", "The unit to display volumes of fluids.\nPossibilities: " + Arrays.toString(FluidVolume.getNames()), FluidVolume.getNames()).setLanguageKey("config.as.unit_fluid_volume");
			unitPressure = config.get("units", "pressure", "Bar (relative)", "The unit to display pressure.\nPossibilities: " + Arrays.toString(Pressure.getNames()), Pressure.getNames()).setLanguageKey("config.as.unit_pressure");
			unitTemperature = config.get("units", "temperature", "Celsius", "The unit to display temperature.\nPossibilities: " + Arrays.toString(Temperature.getNames()), Temperature.getNames()).setLanguageKey("config.as.unit_temperature");
			unitTime = config.get("units", "time", "Hours/Minutes/Seconds", "The unit to display time.\nPossibilities: " + Arrays.toString(Time.getNames()), Time.getNames()).setLanguageKey("config.as.unit_time");
			unitRotationSpeed = config.get("units", "rotation_speed", "Revolutions per Minute", "The unit to display rotation speed.\nPossibilities: " + Arrays.toString(RotationSpeed.getNames()), RotationSpeed.getNames()).setLanguageKey("config.as.unit_rotation_speed");
			unitTorque = config.get("units", "torque", "Newton Meter", "The unit to display torque.\nPossibilities: " + Arrays.toString(Torque.getNames()), Torque.getNames()).setLanguageKey("config.as.unit_torque");
			unitRotInertia = config.get("units", "rotational_inertia", "Kilogram Square Meter", "The unit to display rotational inertia.\nPossibilities: " + Arrays.toString(RotationalInertia.getNames()), RotationalInertia.getNames()).setLanguageKey("config.as.unit_rotational_inertia");
		}

		// ----------------

		config.save();
		Log.info("Configuration loaded.");
	}

	private static void createConfig(File file) {
		boolean repeat = false;
		do {
			config = new Configuration(file);
			version = config.get("general", "version", CONFIG_VERSION, "Don't edit this value. This is used internally to determine the version of this file.").setShowInGui(false);
			if (version.getInt() > CONFIG_VERSION) {
				File rename = new File(file.getAbsolutePath() + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".errored");
				Log.error("Found configuration file with a higher version than expected (%s > %s). Renaming to %s and generating a new file...", version.getInt(), CONFIG_VERSION, rename.getName());
				file.renameTo(rename);
				repeat = true;
			} else {
				repeat = false;
				if (version.getInt() < CONFIG_VERSION) {
					Log.info("Found outdated configuration file (Version %s instead of %s). Updating.", version.getInt(), CONFIG_VERSION);
					while (version.getInt() < CONFIG_VERSION)
						updateConfig(version.getInt());
				}
			}
		} while (repeat);
	}

	private static void updateConfig(int fromVersion) {
		switch (fromVersion) {

		}
		version.set(fromVersion + 1);
	}

}
