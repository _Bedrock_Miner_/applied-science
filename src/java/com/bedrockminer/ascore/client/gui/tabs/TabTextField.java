package com.bedrockminer.ascore.client.gui.tabs;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.bedrockminer.ascore.client.gui.GuiTabbedBase;

import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * A tab class for an easier use of text fields for values.
 * @author _Bedrock_Miner_
 */
public abstract class TabTextField <G extends GuiTabbedBase<?, ?>> extends Tab<G> {

	private List<GuiTextField> fields = new ArrayList<GuiTextField>();

	/**
	 * Instantiates a new TabTextField.
	 *
	 * @param gui
	 * @param slots
	 * @param displayStack
	 */
	public TabTextField(G gui, int[] slots, ItemStack displayStack) {
		super(gui, slots, displayStack);
	}

	/**
	 * Instantiates a new TabTextField.
	 *
	 * @param gui
	 * @param slots
	 * @param displayIcon
	 */
	public TabTextField(G gui, int[] slots, ResourceLocation displayIcon) {
		super(gui, slots, displayIcon);
	}

	/**
	 * Adds a new GuiTextField to this Tab.
	 *
	 * @param field the field to add
	 */
	public void addTextField(GuiTextField field) {
		this.fields.add(field);
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.gui().guiLeft(), this.gui().guiTop(), 0);

		for (GuiTextField field: this.fields) {
			if (!field.isFocused()) {
				field.setText(this.getTextForField(field.getId()));
				field.setTextColor(0xeeeeee);
			} else {
				field.setTextColor(this.isFieldValid(field.getId(), field.getText()) ? 0xffffff : 0xff8888);
			}
			field.drawTextBox();
		}

		GlStateManager.popMatrix();
	}

	@Override
	public void updateScreen() {
		for (GuiTextField field: this.fields)
			field.updateCursorCounter();
	}

	@Override
	public boolean keyTyped(char character, int keycode) {
		boolean result = false;

		for (GuiTextField field: this.fields) {
			if (field.isFocused()) {
				result |= field.textboxKeyTyped(character, keycode);
				if (keycode == Keyboard.KEY_RETURN) {
					this.submit(field.getId(), field.getText());
					field.setFocused(false);
					Keyboard.enableRepeatEvents(false);
					result = true;
				}
			}
		}
		return result;
	}

	@Override
	public boolean mouseClicked(int x, int y, int button) {
		boolean result = false;
		boolean repeat = false;

		for (GuiTextField field: this.fields) {
			boolean focused = field.isFocused();
			field.mouseClicked(x - this.gui().guiLeft(), y - this.gui().guiTop(), button);
			if (!focused && field.isFocused()) {
				field.setText(this.getEditableTextForField(field.getId()));
				repeat = true;
			}
			if (focused && !field.isFocused()) {
				this.submit(field.getId(), field.getText());
				result = true;
			}
			result |= this.gui().isPointInRegion(field.xPosition, field.yPosition, field.width, field.height, x, y);
		}

		Keyboard.enableRepeatEvents(repeat);

		return result;
	}

	@Override
	public void onOpen() {
		for (GuiTextField field: this.fields)
			field.setFocused(false);
	}

	@Override
	public void onClose() {
		for (GuiTextField field: this.fields)
			if (field.isFocused())
				this.submit(field.getId(), field.getText());
	}

	/**
	 * This method should return whether the given text is a valid input for the
	 * given text field ID. If this returns false, the text will be printed
	 * red instead of white.
	 *
	 * @param ID the text field ID
	 * @param text the text to check
	 * @return true if the input is valid
	 */
	public abstract boolean isFieldValid(int ID, String text);

	/**
	 * Returns a display text for the field that shows the exact value with
	 * it's unit and formatting, just as you would display it to the user
	 * anywhere else.
	 *
	 * @param ID the text field ID
	 * @return the text
	 */
	public abstract String getTextForField(int ID);

	/**
	 * Returns a text that contains only a number representing the current
	 * value for the textfield. The string should be "valid" in a sense that
	 * the isFieldValid method would return true when used on it. This value
	 * will be displayed once the user starts editing a field.
	 *
	 * @param ID the text field ID
	 * @return the raw text
	 */
	public abstract String getEditableTextForField(int ID);

	/**
	 * Called when the user stops editing a text field. Use this methods to send
	 * the changes to the server.
	 *
	 * @param ID the text field ID
	 * @param text the current text
	 */
	public abstract void submit(int ID, String text);

	protected boolean canParseDouble(String str) {
		try {
			Double.parseDouble(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	protected boolean canParseInt(String str) {
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
