package com.bedrockminer.ascore.client.gui.tabs;

import java.util.ArrayList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.GuiTabbedBase;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * A tab is a page in the GuiTabbedBase. It works like a small GUI itself.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class Tab <G extends GuiTabbedBase<?, ?>>  {

	/** The slotIDs that will be visible */
	private int[] slots;
	/** The controlling gui */
	private G gui;
	/** The item stack to display for the tab */
	private ItemStack displayStack;
	/** The image to display if no ItemStack was set */
	private ResourceLocation displayIcon;

	/**
	 * Instantiates a new Tab.
	 *
	 * @param slots
	 * the slot IDs that will be visible on that tab
	 */
	public Tab(G gui, int[] slots, ItemStack displayStack) {
		this.gui = gui;
		this.slots = slots;
		this.displayStack = displayStack;
	}

	/**
	 * Instantiates a new Tab.
	 *
	 * @param slots
	 * the slot IDs that will be visible on that tab
	 */
	public Tab(G gui, int[] slots, ResourceLocation displayIcon) {
		this.gui = gui;
		this.slots = slots;
		this.displayIcon = displayIcon;
	}

	/**
	 * Returns the controlling GUI.
	 *
	 * @return the GUI
	 */
	public G gui() {
		return this.gui;
	}

	/**
	 * Called when the Tab is opened
	 */
	public void onOpen() {
	}

	/**
	 * Called when the Tab is closed
	 */
	public void onClose() {
	}

	/**
	 * Draws the GUI background layer. The origin is at the top left screen
	 * corner.
	 *
	 * @param mouseX
	 * the mouse x position
	 * @param mouseY
	 * the mouse y position
	 * @param partialTicks
	 * the partial tick time
	 */
	public abstract void drawBackground(int mouseX, int mouseY, float partialTicks);

	/**
	 * Draws the GUI foreground layer. The origin is at the top left screen
	 * corner.
	 *
	 * @param mouseX
	 * the mouse x position
	 * @param mouseY
	 * the mouse y position
	 */
	public abstract void drawForeground(int mouseX, int mouseY);

	/**
	 * Returns the title that should be displayed for this tab.
	 *
	 * @return the title
	 */
	public abstract String getTitle();

	/**
	 * Returns an array of slotIDs that will be visible on this Tab.
	 *
	 * @return the visible slots
	 */
	public int[] getVisibleSlots() {
		return this.slots;
	}

	/**
	 * Returns the buttons for this tab. The list must be mutable.
	 *
	 * @return the buttons for this tab
	 */
	public List<GuiButton> getButtons() {
		return new ArrayList<GuiButton>();
	}

	/**
	 * Called every tick to perform updates.
	 */
	public void updateScreen() {
	}

	/**
	 * Called when a key is typed.
	 *
	 * @param character
	 * @param keycode
	 * @return <code>true</code> to cancel the event
	 */
	public boolean keyTyped(char character, int keycode) {
		return false;
	}

	/**
	 * Called when the mouse is clicked.
	 *
	 * @param x
	 * @param y
	 * @param button
	 * @return <code>true</code> to cancel the event
	 */
	public boolean mouseClicked(int x, int y, int button) {
		return false;
	}

	/**
	 * Called when the mouse is moved while a button is being held down.
	 *
	 * @param x
	 * @param y
	 * @param button
	 * @param time
	 * The time since the button was pressed down
	 * @return <code>true</code> to cancel the event
	 */
	public boolean mouseClickMoved(int x, int y, int button, long time) {
		return false;
	}

	/**
	 * Called when the mouse is released.
	 *
	 * @param x
	 * @param y
	 * @param button
	 * @return <code>true</code> to cancel the event
	 */
	public boolean mouseReleased(int x, int y, int button) {
		return false;
	}

	/**
	 * Called when a button is clicked
	 *
	 * @param button
	 */
	public void buttonClicked(GuiButton button) {
	}

	/**
	 * Returns the ItemStack to display for the Tab
	 *
	 * @return the display stack
	 */
	public ItemStack getDisplayStack() {
		return this.displayStack;
	}

	/**
	 * Returns the image location to display if no ItemStack is available.
	 *
	 * @return the display icon location
	 */
	public ResourceLocation getDisplayIcon() {
		return this.displayIcon;
	}

	/**
	 * Draws the tab at the given position.
	 *
	 * @param x
	 * the x coordinate
	 * @param y
	 * the y coordinate
	 * @param active
	 * if <code>true</code>, the tab is currently selected.
	 * @param hovering
	 * <code>true</code> if the mouse hovers over the tab
	 */
	public void drawTab(int x, int y, boolean active, boolean hovering) {
		// Background
		Minecraft.getMinecraft().getTextureManager().bindTexture(GuiTabbedBase.WIDGETS);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		if (active) {
			this.gui().drawTexturedModalRect(x, y, 0, 0, 24, 25);
		} else {
			if (hovering)
				this.gui().drawTexturedModalRect(x, y, 48, 0, 24, 22);
			else
				this.gui().drawTexturedModalRect(x, y, 24, 0, 24, 22);
		}

		// Icon
		if (this.getDisplayStack() != null) {
			RenderHelper.enableGUIStandardItemLighting();
			Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(this.getDisplayStack(), x + 4, y + 5);
			RenderHelper.disableStandardItemLighting();
		} else {
			Minecraft.getMinecraft().getTextureManager().bindTexture(this.getDisplayIcon());
			this.gui().drawModalRectWithCustomSizedTexture(x + 4, y + 5, 0, 0, 16, 16, 16, 16);
		}
	}

	/**
	 * Returns an array filled with values between the two given bounds. Can be
	 * used to get a full array of values for the slot parameter.
	 *
	 * @param from
	 * the first value (inclusive)
	 * @param to
	 * the last value (inclusive)
	 * @return the array
	 */
	public static int[] getFilledArray(int from, int to) {
		int[] array = new int[to - from + 1];
		for (int i = from; i <= to; i++)
			array[i - from] = i;
		return array;
	}
}
