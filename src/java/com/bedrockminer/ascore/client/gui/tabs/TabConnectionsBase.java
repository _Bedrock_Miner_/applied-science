package com.bedrockminer.ascore.client.gui.tabs;

import java.nio.FloatBuffer;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.ConnectionType;
import com.bedrockminer.ascore.client.gui.GuiTabbedBase;
import com.bedrockminer.ascore.util.BlockSide;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraft.util.Vec3i;

/**
 * The TabConnections is used to configure connections for TileEntities. This
 * tab has the size 176*176. If your GUI has a different size, you probably need
 * to create a subclass.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class TabConnectionsBase <G extends GuiTabbedBase<?, ?>> extends Tab<G> {

	public static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/connections.png");

	/** The ID of the connection being edited */
	protected int currentConnection = 0;

	/** The available positions for the current connection */
	protected Set<BlockSide> availablePositions = new HashSet<BlockSide>();
	/** The selected positions for the current connection */
	protected Set<BlockSide> selectedPositions = new HashSet<BlockSide>();
	/** The color of the current connection */
	protected EnumDyeColor color = EnumDyeColor.BLACK;
	/** The type of the current connection */
	protected ConnectionType type = ConnectionType.ERROR;

	protected int dragStartX;
	protected int dragStartY;
	protected int prevDragX;
	protected int prevDragY;
	protected boolean dragging;
	protected Matrix4f transformation;
	protected BlockSide lastChange;

	/**
	 * Instantiates a new TabConnections
	 */
	public TabConnectionsBase(G gui, ItemStack displayStack) {
		super(gui, new int[0], displayStack);
	}

	/**
	 * Instantiates a new TabConnections
	 */
	public TabConnectionsBase(G gui, ResourceLocation displayIcon) {
		super(gui, new int[0], displayIcon);
	}

	@Override
	public void onOpen() {
		this.setCurrentConnection(this.currentConnection);
	}

	// Drawing

	@Override
	public void drawBackground(int mouseX, int mouseY, float partialTicks) {
		// Background
		Minecraft.getMinecraft().getTextureManager().bindTexture(background);
		this.gui().drawTexturedModalRect(this.gui().guiLeft(), this.gui().guiTop(), 0, 0, 176, 176);
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {
		// Trying to refresh if not yet loaded properly
		if (this.type == ConnectionType.ERROR)
			this.setCurrentConnection(this.currentConnection);
		// Connection Name
		String text = this.getConnectionName(this.currentConnection);
		Minecraft.getMinecraft().fontRendererObj.drawString(text, this.gui().guiLeft() + 88 - Minecraft.getMinecraft().fontRendererObj.getStringWidth(text) / 2, this.gui().guiTop() + 22, 0);

		// Connection Type
		Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.connection.type", this.type.getLocalizedName()), this.gui().guiLeft() + 8, this.gui().guiTop() + 39, 0);
		// Connection Color
		Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.connection.color"), this.gui().guiLeft() + 8, this.gui().guiTop() + 53, 0);
		text = I18n.format("tooltip.color." + (this.color == EnumDyeColor.BLACK ? "universal" : this.color.getUnlocalizedName()));
		Minecraft.getMinecraft().fontRendererObj.drawString(text, this.gui().guiLeft() + 104 - Minecraft.getMinecraft().fontRendererObj.getStringWidth(text) / 2, this.gui().guiTop() + 53, 0xffffff);
		// Configuration
		Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.connection.configuration"), this.gui().guiLeft() + 8, this.gui().guiTop() + 67, 0);

		// Buttons
		Minecraft.getMinecraft().getTextureManager().bindTexture(GuiTabbedBase.WIDGETS);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		// Connection Selection
		if (this.getConnectionCount() > 1) {
			if (this.gui().isPointInRegion(9, 19, 10, 15, mouseX, mouseY))
				this.gui().drawTexturedModalRect(this.gui().guiLeft() + 9, this.gui().guiTop() + 19, 40, 32, 10, 15);
			else
				this.gui().drawTexturedModalRect(this.gui().guiLeft() + 9, this.gui().guiTop() + 19, 30, 32, 10, 15);

			if (this.gui().isPointInRegion(157, 19, 10, 15, mouseX, mouseY))
				this.gui().drawTexturedModalRect(this.gui().guiLeft() + 157, this.gui().guiTop() + 19, 10, 32, 10, 15);
			else
				this.gui().drawTexturedModalRect(this.gui().guiLeft() + 157, this.gui().guiTop() + 19, 0, 32, 10, 15);
		} else {
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 9, this.gui().guiTop() + 19, 50, 32, 10, 15);
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 157, this.gui().guiTop() + 19, 20, 32, 10, 15);
		}
		// Color selection
		if (this.gui().isPointInRegion(41, 50, 10, 15, mouseX, mouseY))
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 41, this.gui().guiTop() + 50, 40, 32, 10, 15);
		else
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 41, this.gui().guiTop() + 50, 30, 32, 10, 15);

		if (this.gui().isPointInRegion(157, 50, 10, 15, mouseX, mouseY))
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 157, this.gui().guiTop() + 50, 10, 32, 10, 15);
		else
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 157, this.gui().guiTop() + 50, 0, 32, 10, 15);

		// Drawing the model
		this.drawModel(mouseX, mouseY);

		// Help button
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		Minecraft.getMinecraft().getTextureManager().bindTexture(background);
		if (this.gui().isPointInRegion(11, 79, 18, 18, mouseX, mouseY)) {
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 10, this.gui().guiTop() + 78, 200, 0, 20, 20);
			this.gui().drawHoveringText(Minecraft.getMinecraft().fontRendererObj.listFormattedStringToWidth(I18n.format("container.connection.help").replace("\\n", "\n"), 250), mouseX, mouseY);
		} else {
			this.gui().drawTexturedModalRect(this.gui().guiLeft() + 10, this.gui().guiTop() + 78, 180, 0, 20, 20);
		}
	}

	/**
	 * Returns the rendering scale for the block model. This can be adjusted to
	 * fit larger multiblock structures into the viewport. Default: 50
	 *
	 * @return the scale
	 */
	protected float getModelScale() {
		return 50;
	}

	// Abstract methods

	/**
	 * Returns the amount of connections this tab can configure.
	 *
	 * @return the connection count
	 */
	protected abstract int getConnectionCount();

	/**
	 * Returns the display name for the given connection ID.
	 *
	 * @param ID
	 * the connection ID
	 * @return the display name
	 */
	protected abstract String getConnectionName(int ID);

	/**
	 * Returns the data for the given connection ID.
	 *
	 * @param ID
	 * the ID
	 * @return the data
	 */
	protected abstract NBTTagCompound getConnectionData(int ID);

	/**
	 * Sets and synchronizes the given data to the connection ID.
	 *
	 * @param ID
	 * the ID
	 * @param nbt
	 * the data
	 */
	protected abstract void setConnectionData(int ID, NBTTagCompound nbt);

	/**
	 * Returns the center of the rotation in relative coordinates to the block
	 * position.
	 *
	 * @return the rotation center
	 */
	protected abstract Vec3 getRotationCenter();

	/**
	 * Renders the block at the origin.
	 */
	protected abstract void renderBlock();

	// User interaction

	@Override
	public boolean mouseClicked(int x, int y, int button) {
		// Buttons
		if (this.gui().isPointInRegion(9, 19, 10, 15, x, y)) {
			if (--this.currentConnection < 0)
				this.currentConnection = this.getConnectionCount() - 1;
			this.setCurrentConnection(this.currentConnection);
			return true;
		}
		if (this.gui().isPointInRegion(157, 19, 10, 15, x, y)) {
			if (++this.currentConnection >= this.getConnectionCount())
				this.currentConnection = 0;
			this.setCurrentConnection(this.currentConnection);
			return true;
		}

		if (this.gui().isPointInRegion(41, 50, 10, 15, x, y)) {
			int colorID = this.color.getDyeDamage();
			if (--colorID < 0)
				colorID = 15;
			this.color = EnumDyeColor.byDyeDamage(colorID);
			this.sendChanges();
			return true;
		}
		if (this.gui().isPointInRegion(157, 50, 10, 15, x, y)) {
			int colorID = this.color.getDyeDamage();
			if (++colorID > 15)
				colorID = 0;
			this.color = EnumDyeColor.byDyeDamage(colorID);
			this.sendChanges();
			return true;
		}

		// Reset rotation
		if (this.gui().isPointInRegion(11, 79, 154, 86, x, y) && button == 1) {
			this.loadTransformation();
			return true;
		}
		// Change faces
		if (this.gui().isPointInRegion(11, 79, 154, 86, x, y) && button == 0) {
			for (BlockSide side : this.availablePositions) {
				if (this.areCoordsOnSide(x - this.gui().guiLeft() - 88, y - this.gui().guiTop() - 122, side)) {
					if (this.selectedPositions.contains(side)) {
						this.selectedPositions.remove(side);
					} else {
						this.selectedPositions.add(side);
					}
					this.lastChange = side;
					this.sendChanges();
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean mouseClickMoved(int x, int y, int button, long time) {
		boolean processed = false;

		if (!this.dragging) {
			this.prevDragX = this.dragStartX = x;
			this.prevDragY = this.dragStartY = y;
			this.dragging = true;
		}

		// Reverting change when starting to drag
		if (this.lastChange != null) {
			if (this.selectedPositions.contains(this.lastChange))
				this.selectedPositions.remove(this.lastChange);
			else
				this.selectedPositions.add(this.lastChange);
			this.lastChange = null;
			this.sendChanges();
		}

		if (this.gui().isPointInRegion(11, 79, 154, 86, this.dragStartX, this.dragStartY)) {
			int dx = x - this.prevDragX;
			int dy = this.prevDragY - y;

			if ((dx != 0 || dy != 0) && dx * dx + dy * dy < 2500) {
				Vector3f center = new Vector3f(0, 0, 1);
				Vector3f drag = new Vector3f(dx, dy, (float) Math.sqrt(2500 - dx * dx + dy * dy)).normalise(null);

				Vector3f axis = new Vector3f();
				Vector3f.cross(center, drag, axis);
				if (axis.length() > 0) {
					axis = axis.normalise(null);

					double angle = Math.acos(Vector3f.dot(center, drag));

					Matrix4f rotation = new Matrix4f();
					rotation.rotate((float) angle, axis);
					this.transformation = Matrix4f.mul(rotation, this.transformation, null);
				}
			}
			processed = true;
		}

		this.prevDragX = x;
		this.prevDragY = y;

		return processed;
	}

	@Override
	public boolean mouseReleased(int x, int y, int button) {
		this.dragging = false;
		this.lastChange = null;
		return false;
	}

	// Setter & Sync

	/**
	 * Sets the currently displayed connection.
	 *
	 * @param ID
	 * the connection ID
	 */
	public void setCurrentConnection(int ID) {
		if (ID >= 0 && ID < this.getConnectionCount()) {
			this.currentConnection = ID;
			NBTTagCompound nbt = this.getConnectionData(this.currentConnection);
			if (nbt != null) {

				this.availablePositions = Connection.readBlockSideSet(nbt.getTagList("Available", 10));
				this.selectedPositions = Connection.readBlockSideSet(nbt.getTagList("Selected", 10));

				int color = nbt.getByte("Color");
				this.color = (color < 0 || color > 15) ? EnumDyeColor.BLACK : EnumDyeColor.byDyeDamage(color);
				this.type = ConnectionType.getByID(nbt.getInteger("Type"));
				// If something goes wrong we want to have a non-null value
				if (this.type == null)
					this.type = ConnectionType.ERROR;

				this.loadTransformation();
			}
		}
	}

	/**
	 * Sends the changes that were made to the server.
	 */
	public void sendChanges() {
		NBTTagCompound nbt = new NBTTagCompound();

		nbt.setTag("Selected", Connection.writeBlockSideSet(this.selectedPositions));
		nbt.setByte("Color", (byte) this.color.getDyeDamage());

		this.setConnectionData(this.currentConnection, nbt);
	}

	// Advanced Drawing & Vector maths

	/**
	 * Loads the initial transformation matrix.
	 */
	public void loadTransformation() {
		this.transformation = new Matrix4f();
		this.transformation.scale(new Vector3f(this.getModelScale(), this.getModelScale(), this.getModelScale()));
		this.transformation.rotate((float) Math.toRadians(this.gui().mc.thePlayer.rotationPitch), new Vector3f(1, 0, 0));
		this.transformation.rotate((float) Math.toRadians(this.gui().mc.thePlayer.rotationYaw - 180), new Vector3f(0, 1, 0));
	}

	/**
	 * Draws the block model onto the screen
	 *
	 * @param mouseX
	 * @param mouseY
	 */
	protected void drawModel(int mouseX, int mouseY) {
		if (this.transformation == null)
			this.loadTransformation();

		GlStateManager.pushMatrix();
		GlStateManager.translate(this.gui().guiLeft() + 88, this.gui().guiTop() + 122, 50);
		GlStateManager.scale(-1, -1, -1);
		GlStateManager.rotate(180, 0, 1, 0);
		FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
		this.transformation.store(buffer);
		buffer.flip();
		GlStateManager.multMatrix(buffer);

		Vec3 rot = this.getRotationCenter();
		GlStateManager.translate(-rot.xCoord, -rot.yCoord, -rot.zCoord);

		GlStateManager.pushMatrix();
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		GlStateManager.enableDepth();
		GlStateManager.enableBlend();
		this.renderBlock();
		GlStateManager.popMatrix();
		this.renderSides();

		GlStateManager.popMatrix();
	}

	/**
	 * Returns <code>true</code> if a specific facing is visible in the current
	 * transformation.
	 *
	 * @param side
	 * the facing to check
	 * @return <code>true</code> if visible
	 */
	protected boolean isSideVisible(EnumFacing side) {
		Vec3i dir = side.getDirectionVec();
		Vector4f direction = new Vector4f(dir.getX(), dir.getY(), dir.getZ(), 1);
		Vector4f result = new Vector4f();
		Matrix4f.transform(this.transformation, direction, result);
		return result.z > 0;
	}

	/**
	 * This method tests whether the given x/y screen coordinates are on the
	 * given block side.
	 *
	 * @param x
	 * the x coordinate
	 * @param y
	 * the y coordinate
	 * @param side
	 * the block side
	 * @return <code>true</code> if the coordinates are on the side
	 */
	protected boolean areCoordsOnSide(double x, double y, BlockSide side) {
		if (!this.isSideVisible(side.getSide()))
			return false;
		y = -y;
		BlockPos te = this.gui().container().te().getPos();
		BlockPos pos = side.getPos().add(-te.getX(), -te.getY(), -te.getZ());
		Vec3 off = this.getRotationCenter();
		Vector3f v1 = new Vector3f(), v2 = new Vector3f(), v3 = new Vector3f(),
				v4 = new Vector3f();
		this.setSideCorners(side.getSide(), pos, v1, v2, v3, v4);
		Vector4f[] vecs = new Vector4f[] { new Vector4f(), new Vector4f(),
				new Vector4f(), new Vector4f() };
		Matrix4f.transform(this.transformation, new Vector4f((float) (v1.x - off.xCoord), (float) (v1.y - off.yCoord), (float) (v1.z - off.zCoord), 1), vecs[0]);
		Matrix4f.transform(this.transformation, new Vector4f((float) (v2.x - off.xCoord), (float) (v2.y - off.yCoord), (float) (v2.z - off.zCoord), 1), vecs[1]);
		Matrix4f.transform(this.transformation, new Vector4f((float) (v3.x - off.xCoord), (float) (v3.y - off.yCoord), (float) (v3.z - off.zCoord), 1), vecs[2]);
		Matrix4f.transform(this.transformation, new Vector4f((float) (v4.x - off.xCoord), (float) (v4.y - off.yCoord), (float) (v4.z - off.zCoord), 1), vecs[3]);

		boolean inside = false;

		for (int i = 0; i < 4; i++) {
			Vector4f A = vecs[i];
			Vector4f B = vecs[i == 3 ? 0 : i + 1];
			if ((A.y > y) != (B.y > y) && x < (B.x - A.x) * (y - A.y) / (B.y - A.y) + A.x)
				inside = !inside;
		}

		return inside;
	}

	/**
	 * Renders the sides that are available for this connection.
	 */
	protected void renderSides() {
		GlStateManager.disableTexture2D();
		GlStateManager.enableBlend();

		Tessellator t = Tessellator.getInstance();
		WorldRenderer w = t.getWorldRenderer();

		for (BlockSide side : this.availablePositions) {
			if (!this.isSideVisible(side.getSide()))
				continue;

			BlockPos te = this.gui().container().te().getPos();
			BlockPos pos = side.getPos().add(-te.getX(), -te.getY(), -te.getZ());
			Vector3f v1 = new Vector3f(), v2 = new Vector3f(),
					v3 = new Vector3f(), v4 = new Vector3f();
			this.setSideCorners(side.getSide(), pos, v1, v2, v3, v4);

			if (this.selectedPositions.contains(side))
				GlStateManager.color(0.18f, 0.29f, 0.76f, 0.4f);
			else
				GlStateManager.color(0.94f, 0.59f, 0.09f, 0.4f);

			w.begin(7, DefaultVertexFormats.POSITION);
			w.pos(v1.x, v1.y, v1.z).endVertex();
			w.pos(v2.x, v2.y, v2.z).endVertex();
			w.pos(v3.x, v3.y, v3.z).endVertex();
			w.pos(v4.x, v4.y, v4.z).endVertex();
			t.draw();

			if (this.selectedPositions.contains(side))
				GlStateManager.color(0.18f, 0.29f, 0.76f, 0.6f);
			else
				GlStateManager.color(0.94f, 0.59f, 0.09f, 0.6f);

			GL11.glLineWidth(2.0f);
			w.begin(3, DefaultVertexFormats.POSITION);
			w.pos(v1.x, v1.y, v1.z).endVertex();
			w.pos(v2.x, v2.y, v2.z).endVertex();
			w.pos(v3.x, v3.y, v3.z).endVertex();
			w.pos(v4.x, v4.y, v4.z).endVertex();
			t.draw();
		}

		GlStateManager.enableTexture2D();
	}

	/**
	 * Sets the four given vectors to the corners of the given side at the given
	 * position.
	 */
	protected void setSideCorners(EnumFacing side, BlockPos pos, Vector3f v1, Vector3f v2, Vector3f v3, Vector3f v4) {
		switch (side) {
		case DOWN:
			v1.set(pos.getX() - 0.001f, pos.getY() - 0.001f, pos.getZ() - 0.001f);
			v2.set(pos.getX() + 1.001f, pos.getY() - 0.001f, pos.getZ() - 0.001f);
			v3.set(pos.getX() + 1.001f, pos.getY() - 0.001f, pos.getZ() + 1.001f);
			v4.set(pos.getX() - 0.001f, pos.getY() - 0.001f, pos.getZ() + 1.001f);
			break;
		case EAST:
			v1.set(pos.getX() + 1.001f, pos.getY() - 0.001f, pos.getZ() - 0.001f);
			v2.set(pos.getX() + 1.001f, pos.getY() + 1.001f, pos.getZ() - 0.001f);
			v3.set(pos.getX() + 1.001f, pos.getY() + 1.001f, pos.getZ() + 1.001f);
			v4.set(pos.getX() + 1.001f, pos.getY() - 0.001f, pos.getZ() + 1.001f);
			break;
		case NORTH:
			v1.set(pos.getX() - 0.001f, pos.getY() - 0.001f, pos.getZ() - 0.001f);
			v2.set(pos.getX() - 0.001f, pos.getY() + 1.001f, pos.getZ() - 0.001f);
			v3.set(pos.getX() + 1.001f, pos.getY() + 1.001f, pos.getZ() - 0.001f);
			v4.set(pos.getX() + 1.001f, pos.getY() - 0.001f, pos.getZ() - 0.001f);
			break;
		case SOUTH:
			v1.set(pos.getX() + 1.001f, pos.getY() - 0.001f, pos.getZ() + 1.001f);
			v2.set(pos.getX() + 1.001f, pos.getY() + 1.001f, pos.getZ() + 1.001f);
			v3.set(pos.getX() - 0.001f, pos.getY() + 1.001f, pos.getZ() + 1.001f);
			v4.set(pos.getX() - 0.001f, pos.getY() - 0.001f, pos.getZ() + 1.001f);
			break;
		case UP:
			v1.set(pos.getX() + 1.001f, pos.getY() + 1.001f, pos.getZ() - 0.001f);
			v2.set(pos.getX() - 0.001f, pos.getY() + 1.001f, pos.getZ() - 0.001f);
			v3.set(pos.getX() - 0.001f, pos.getY() + 1.001f, pos.getZ() + 1.001f);
			v4.set(pos.getX() + 1.001f, pos.getY() + 1.001f, pos.getZ() + 1.001f);
			break;
		case WEST:
		default:
			v1.set(pos.getX() - 0.001f, pos.getY() - 0.001f, pos.getZ() + 1.001f);
			v2.set(pos.getX() - 0.001f, pos.getY() + 1.001f, pos.getZ() + 1.001f);
			v3.set(pos.getX() - 0.001f, pos.getY() + 1.001f, pos.getZ() - 0.001f);
			v4.set(pos.getX() - 0.001f, pos.getY() - 0.001f, pos.getZ() - 0.001f);
			break;
		}
	}
}
