package com.bedrockminer.ascore.client.gui.tabs;

import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.GuiTabbedBase;
import com.bedrockminer.ascore.config.Cfg;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * An information tab is an empty tab displaying information in text form.
 * This tab has the size 176*176. If your GUI has a different size, you probably
 * need to create a subclass.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class TabInformation <G extends GuiTabbedBase<?, ?>> extends Tab<G> {

	public static final ResourceLocation GENERIC = new ResourceLocation("ascore:textures/gui/container/information.png");

	/** The amount of lines scrolled down in the text. */
	protected int textscroll;

	protected boolean fontstate;

	/**
	 * Instantiates a new TabInformation.
	 */
	public TabInformation(G gui, ItemStack displayStack) {
		super(gui, new int[0], displayStack);
	}

	/**
	 * Instantiates a new TabInformation.
	 */
	public TabInformation(G gui, ResourceLocation displayIcon) {
		super(gui, new int[0], displayIcon);
	}

	@Override
	public void drawBackground(int mouseX, int mouseY, float partialTicks) {
		Minecraft.getMinecraft().getTextureManager().bindTexture(GENERIC);
		this.gui().drawTexturedModalRect(this.gui().guiLeft(), this.gui().guiTop(), 0, 0, 176, 176);
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {
		this.openUnicodeBlock();
		FontRenderer font = Minecraft.getMinecraft().fontRendererObj;
		List<String> text = this.layoutText();
		int lines = (this.gui().ySize() - 30) / font.FONT_HEIGHT;

		for (int i = 0; i < lines; i++) {
			if (i + this.textscroll >= text.size())
				break;
			font.drawString(text.get(i + this.textscroll), this.gui().guiLeft() + 8, this.gui().guiTop() + i * font.FONT_HEIGHT + 24, 0);
		}
		this.closeUnicodeBlock();

		if (lines < text.size()) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(GuiTabbedBase.WIDGETS);
			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			// Up button
			if (this.textscroll > 0) {
				if (this.gui().isPointInRegion(this.gui().xSize() - 23, 23, 15, 10, mouseX, mouseY))
					this.gui().drawTexturedModalRect(this.gui().guiLeft() + this.gui().xSize() - 23, this.gui().guiTop() + 23, 120, 32, 15, 10);
				else
					this.gui().drawTexturedModalRect(this.gui().guiLeft() + this.gui().xSize() - 23, this.gui().guiTop() + 23, 105, 32, 15, 10);
			} else {
				this.gui().drawTexturedModalRect(this.gui().guiLeft() + this.gui().xSize() - 23, this.gui().guiTop() + 23, 135, 32, 15, 10);
			}

			// Down button
			if (this.textscroll + lines - 1 < text.size()) {
				if (this.gui().isPointInRegion(this.gui().xSize() - 23, this.gui().ySize() - 18, 15, 10, mouseX, mouseY))
					this.gui().drawTexturedModalRect(this.gui().guiLeft() + this.gui().xSize() - 23, this.gui().guiTop() + this.gui().ySize() - 18, 75, 32, 15, 10);
				else
					this.gui().drawTexturedModalRect(this.gui().guiLeft() + this.gui().xSize() - 23, this.gui().guiTop() + this.gui().ySize() - 18, 60, 32, 15, 10);
			} else {
				this.gui().drawTexturedModalRect(this.gui().guiLeft() + this.gui().xSize() - 23, this.gui().guiTop() + this.gui().ySize() - 18, 90, 32, 15, 10);
			}
		}
	}

	@Override
	public boolean mouseClicked(int x, int y, int button) {
		this.openUnicodeBlock();
		List<String> text = this.layoutText();
		int lines = (this.gui().ySize() - 24) / Minecraft.getMinecraft().fontRendererObj.FONT_HEIGHT;
		this.closeUnicodeBlock();
		if (lines < text.size()) {
			// Up button
			if (this.textscroll > 0 && this.gui().isPointInRegion(this.gui().xSize() - 23, 23, 15, 10, x, y)) {
				this.textscroll --;
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.create(new ResourceLocation("gui.button.press"), 1.0F));
				return true;
			}

			// Down button
			if (this.textscroll + lines - 1 < text.size() && this.gui().isPointInRegion(this.gui().xSize() - 23, this.gui().ySize() - 18, 15, 10, x, y)) {
				this.textscroll ++;
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.create(new ResourceLocation("gui.button.press"), 1.0F));
				return true;
			}
		}

		return false;
	}

	/**
	 * Lays out the text so that it fits inside the frame perfectly.
	 *
	 * @return the text
	 */
	public List<String> layoutText() {
		List<String> text = this.getText();
		List<String> printed = new LinkedList<String>();
		for (String element : text) {
			if (element.length() == 0) {
				printed.add("");
				continue;
			}
			int firstNonSpace = 0;
			while (element.charAt(firstNonSpace) == ' ' || element.charAt(firstNonSpace) == '\t')
				firstNonSpace++;
			String prefix = element.substring(0, firstNonSpace);
			String secPrefix = prefix + "  ";
			element = element.substring(firstNonSpace);
			List<String> lines = Minecraft.getMinecraft().fontRendererObj.listFormattedStringToWidth(element, this.gui().xSize() - 16 - Minecraft.getMinecraft().fontRendererObj.getStringWidth(secPrefix));
			if (lines.size() > 0)
				printed.add(prefix + lines.get(0));
			if (lines.size() > 1) {
				for (int i = 1; i < lines.size(); i++)
					printed.add(secPrefix + lines.get(i));
			}
		}
		return printed;
	}

	@Override
	public void onOpen() {
		this.textscroll = 0;
	}

	/**
	 * Returns the text this tab should display. Each element gets a single
	 * line.
	 *
	 * @return the text to display
	 */
	public abstract List<String> getText();

	/**
	 * Sets the fontrenderer to unicode mode if selected in the config.
	 */
	protected void openUnicodeBlock() {
		this.fontstate = Minecraft.getMinecraft().fontRendererObj.getUnicodeFlag();
		if (Cfg.unicodeInInfo.getBoolean())
			Minecraft.getMinecraft().fontRendererObj.setUnicodeFlag(true);
	}

	/**
	 * Resets the fontrenderer to its original state.
	 */
	protected void closeUnicodeBlock() {
		Minecraft.getMinecraft().fontRendererObj.setUnicodeFlag(this.fontstate);
	}
}
