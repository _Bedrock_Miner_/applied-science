package com.bedrockminer.ascore.client.gui.tabs;

import com.bedrockminer.ascore.client.gui.GuiTabbedBase;

import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

/**
 * The warning tab is a tab similar to the information tab, however it displays
 * the number of warnings next to the warning symbol.
 * This tab has the size 176*176. If your GUI has a different size, you probably
 * need to create a subclass.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class TabWarnings <G extends GuiTabbedBase<?, ?>> extends TabInformation<G> {

	public static ResourceLocation WARNING = new ResourceLocation("ascore:textures/gui/icons/tab_warnings.png");
	public static ResourceLocation INACTIVE = new ResourceLocation("ascore:textures/gui/icons/tab_warnings_inactive.png");

	/**
	 * Instantiates a new TabWarning
	 */
	public TabWarnings(G gui) {
		super(gui, (ResourceLocation) null);
	}

	@Override
	public void drawTab(int x, int y, boolean active, boolean hovering) {
		// Background
		Minecraft.getMinecraft().getTextureManager().bindTexture(GuiTabbedBase.WIDGETS);
		if (active) {
			this.gui().drawTexturedModalRect(x, y, 0, 0, 24, 25);
		} else {
			if (hovering)
				this.gui().drawTexturedModalRect(x, y, 48, 0, 24, 22);
			else
				this.gui().drawTexturedModalRect(x, y, 24, 0, 24, 22);
		}

		// Icon
		Minecraft.getMinecraft().getTextureManager().bindTexture(this.getWarningCount() > 0 ? WARNING : INACTIVE);
		this.gui().drawModalRectWithCustomSizedTexture(x + 4, y + 5, 0, 0, 16, 16, 16, 16);

		if (this.getWarningCount() > 0) {
			String s = "" + EnumChatFormatting.BOLD + Integer.toString(this.getWarningCount());
			Minecraft.getMinecraft().fontRendererObj.drawString(s, x + 21 - Minecraft.getMinecraft().fontRendererObj.getStringWidth(s), y + 22 - Minecraft.getMinecraft().fontRendererObj.FONT_HEIGHT, 0);
		}
	}

	/**
	 * Returns the amount of warnings. This will be displayed as a number in the
	 * tab symbol.
	 *
	 * @return the number of warnings
	 */
	public abstract int getWarningCount();
}
