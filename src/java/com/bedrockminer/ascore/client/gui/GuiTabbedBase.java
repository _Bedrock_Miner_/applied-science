package com.bedrockminer.ascore.client.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.tabs.Tab;
import com.bedrockminer.ascore.inventory.ContainerBase;
import com.google.common.collect.ImmutableList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.Slot;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

/**
 * The class GuiBase is the base class for every GUI using a ContainerBase.
 * It adds support for the Tabs on top of the GUI.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class GuiTabbedBase <G extends ContainerBase<T>, T extends TileEntity> extends GuiContainer {

	/** The icons for buttons or tabs */
	public static final ResourceLocation WIDGETS = new ResourceLocation("ascore:textures/gui/widgets.png");

	/** A list of tabs this Gui has */
	private List<Tab<? extends GuiTabbedBase<G, T>>> tabs = new ArrayList<Tab<? extends GuiTabbedBase<G, T>>>();
	/** The active tab ID */
	private int activeTab = -1;
	/** The scroll position in the list of tabs */
	private int tabScroll = 0;

	/** A list of Slots that are visible on the current tab */
	private List<Slot> activeSlots = new ArrayList<Slot>();
	/** The original slots the container had */
	private List<Slot> backup;

	/**
	 * Instantiates a new GuiBase with a container.
	 *
	 * @param container the container
	 * @param width the GUI's width
	 * @param height the GUI's height
	 */
	public GuiTabbedBase(G container, int width, int height) {
		super(container);
		this.xSize = width;
		this.ySize = height;
	}

	// Tab handling

	/**
	 * Adds the given tab to the Gui. It will be appended at the end of the tab
	 * list.
	 *
	 * @param tab the tab to add
	 */
	public void addTab(Tab<? extends GuiTabbedBase<G, T>> tab) {
		this.tabs.add(tab);
	}

	/**
	 * Inserts a tab at the given index.
	 *
	 * @param index the index
	 * @param tab the tab to add
	 */
	public void insertTab(int index, Tab<? extends GuiTabbedBase<G, T>> tab) {
		if (index < 0)
			index = 0;
		if (index > this.getTabCount())
			index = this.getTabCount();
		this.tabs.add(index, tab);
	}

	/**
	 * Returns the tabs of this Gui.
	 *
	 * @return the tabs
	 */
	public List<Tab<? extends GuiTabbedBase<G, T>>> getTabs() {
		return ImmutableList.copyOf(this.tabs);
	}

	/**
	 * Returns the tab at the given index.
	 *
	 * @param index the index
	 * @return the tab
	 */
	public Tab<? extends GuiTabbedBase<G, T>> getTab(int index) {
		return this.tabs.get(index);
	}

	/**
	 * Returns the amount of tabs.
	 *
	 * @return the amount of tabs
	 */
	public int getTabCount() {
		return this.tabs.size();
	}

	/**
	 * Returns the tab that is currently active.
	 *
	 * @return the currently active tab
	 */
	public Tab<? extends GuiTabbedBase<G, T>> getCurrentTab() {
		return this.getTab(this.activeTab);
	}

	/**
	 * Sets the current tab by its index.
	 *
	 * @param index the index
	 */
	public void setCurrentTab(int index) {
		if (index < 0 || index >= this.getTabCount())
			index = 0;

		if (this.activeTab != -1)
			this.getCurrentTab().onClose();

		this.activeTab = index;

		this.getCurrentTab().onOpen();

		this.activeSlots.clear();
		int[] slots = this.getCurrentTab().getVisibleSlots();
		for (int i = 0; i < slots.length; i++) {
			if (slots[i] < this.container().inventorySlots.size())
			this.activeSlots.add(this.container().getSlot(slots[i]));
		}

		this.buttonList = this.getCurrentTab().getButtons();
	}

	/**
	 * Returns the container.
	 *
	 * @return the container
	 */
	@SuppressWarnings("unchecked")
	public G container() {
		return (G) this.inventorySlots;
	}

	@Override
	public void initGui() {
		super.initGui();
		this.setCurrentTab(0);
	}

	@Override
	public void onGuiClosed() {
		this.getCurrentTab().onClose();
		super.onGuiClosed();
	}

	// Drawing

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		// Restoring original inventory slots
		this.container().inventorySlots = this.backup;

		GlStateManager.pushMatrix();
		this.fontRendererObj.drawString(this.getCurrentTab().getTitle(), 8, 6, 0);
		GlStateManager.translate(-this.guiLeft, -this.guiTop, 0);
		this.getCurrentTab().drawForeground(mouseX, mouseY);
		GlStateManager.popMatrix();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.pushMatrix();
		this.getCurrentTab().drawBackground(mouseX, mouseY, partialTicks);
		GlStateManager.popMatrix();
		this.drawTabs(mouseX, mouseY);

		// Replacing the slot list with the list of active slots
		this.backup = this.container().inventorySlots;
		this.container().inventorySlots = this.activeSlots;
	}

	protected void drawTabs(int mouseX, int mouseY) {
		List<Integer> tabs = this.getVisibleTabIDs();
		for (int i = 0; i < tabs.size(); i++)
			this.getTab(tabs.get(i)).drawTab(this.guiLeft + 12 + 24 * i, this.guiTop - 22, this.activeTab == tabs.get(i), this.isPointInRegion(12 + 24 * i, -22, 24, 22, mouseX, mouseY));

		int maxTabCount = (this.xSize - 24) / 24;
		if (maxTabCount < this.getTabCount()) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(WIDGETS);
			// left arrow
			if (this.tabScroll > 0) {
				if (this.isPointInRegion(0, -18, 10, 15, mouseX, mouseY))
					this.drawTexturedModalRect(this.guiLeft, this.guiTop - 18, 40, 32, 10, 15);
				else
					this.drawTexturedModalRect(this.guiLeft, this.guiTop - 18, 30, 32, 10, 15);
			} else {
				this.drawTexturedModalRect(this.guiLeft, this.guiTop - 18, 50, 32, 10, 15);
			}
			// right arrow
			if (maxTabCount + this.tabScroll < this.getTabCount()) {
				if (this.isPointInRegion(this.xSize - 10, -18, 10, 15, mouseX, mouseY))
					this.drawTexturedModalRect(this.guiLeft + this.xSize - 10, this.guiTop - 18, 10, 32, 10, 15);
				else
					this.drawTexturedModalRect(this.guiLeft + this.xSize - 10, this.guiTop - 18, 0, 32, 10, 15);
			} else {
				this.drawTexturedModalRect(this.guiLeft + this.xSize - 10, this.guiTop - 18, 20, 32, 10, 15);
			}
		}

		for (int i = 0; i < tabs.size(); i++) {
			if (this.isPointInRegion(13 + 24 * i, -22, 22, 22, mouseX, mouseY))
				this.drawHoveringText(ImmutableList.of(this.getTab(tabs.get(i)).getTitle()), mouseX, mouseY);
		}
	}

	/**
	 * Returns a list of currently visible tab IDs.
	 *
	 * @return the visible tab IDs
	 */
	public List<Integer> getVisibleTabIDs() {
		int maxTabCount = (this.xSize - 24) / 24;
		if (this.tabScroll + maxTabCount > this.getTabCount())
			this.tabScroll -= this.tabScroll + maxTabCount - this.getTabCount();
		if (maxTabCount >= this.getTabCount())
			this.tabScroll = 0;

		List<Integer> visibleTabs = new ArrayList<Integer>(maxTabCount);
		for (int i = 0; i < maxTabCount; i++) {
			if (i + this.tabScroll >= this.getTabCount())
				break;
			visibleTabs.add(i, i + this.tabScroll);
		}
		return visibleTabs;
	}

	// User interaction

	protected boolean checkTabClick(int mouseX, int mouseY, int mouseButton) {
		int maxTabCount = (this.xSize - 24) / 24;
		// Left/right buttons
		if (maxTabCount < this.getTabCount()) {
			if (this.isPointInRegion(0, -18, 10, 15, mouseX, mouseY) && this.tabScroll > 0) {
				this.tabScroll --;
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.create(new ResourceLocation("gui.button.press"), 1.0F));
				return true;
			}
			if (this.isPointInRegion(this.xSize - 10, -18, 10, 15, mouseX, mouseY) && maxTabCount + this.tabScroll < this.getTabCount()) {
				this.tabScroll ++;
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.create(new ResourceLocation("gui.button.press"), 1.0F));
				return true;
			}
		}

		//Tabs
		if (this.isPointInRegion(12, -18, maxTabCount * 24, 22, mouseX, mouseY)) {
			int tab = (mouseX - this.guiLeft - 12) / 24;
			if (tab < this.getVisibleTabIDs().size()) {
				this.setCurrentTab(this.getVisibleTabIDs().get(tab));
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.create(new ResourceLocation("gui.button.press"), 1.0F));
				return true;
			}
		}

		return false;
	}

	@Override
	public void updateScreen() {
		this.getCurrentTab().updateScreen();
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		if (!this.getCurrentTab().keyTyped(typedChar, keyCode))
			super.keyTyped(typedChar, keyCode);
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		if (!this.getCurrentTab().mouseClicked(mouseX, mouseY, mouseButton) && !this.checkTabClick(mouseX, mouseY, mouseButton)) {
			List<Slot> backup = this.container().inventorySlots;
			this.container().inventorySlots = this.activeSlots;
			this.buttonList = this.getCurrentTab().getButtons();

			super.mouseClicked(mouseX, mouseY, mouseButton);

			this.container().inventorySlots = backup;
		}
	}

	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {
		if (!this.getCurrentTab().mouseClickMoved(mouseX, mouseY, clickedMouseButton, timeSinceLastClick)) {
			List<Slot> backup = this.container().inventorySlots;
			this.container().inventorySlots = this.activeSlots;
			this.buttonList = this.getCurrentTab().getButtons();

			super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);

			this.container().inventorySlots = backup;
		}
	}

	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state) {
		if (!this.getCurrentTab().mouseReleased(mouseX, mouseY, state)) {
			List<Slot> backup = this.container().inventorySlots;
			this.container().inventorySlots = this.activeSlots;
			this.buttonList = this.getCurrentTab().getButtons();

			super.mouseReleased(mouseX, mouseY, state);

			this.container().inventorySlots = backup;
		}
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		this.getCurrentTab().buttonClicked(button);
	}

	// Public access to private / protected members

	public int guiLeft() {
		return this.guiLeft;
	}

	public int guiTop() {
		return this.guiTop;
	}

	public int xSize() {
		return this.xSize;
	}

	public int ySize() {
		return this.ySize;
	}

	public float zLevel() {
		return this.zLevel;
	}

	@Override
	public boolean isPointInRegion(int left, int top, int right, int bottom, int pointX, int pointY) {
		return super.isPointInRegion(left, top, right, bottom, pointX, pointY);
	}

	@Override
	public void drawHoveringText(List<String> textLines, int x, int y) {
		super.drawHoveringText(textLines, x, y);
	}
}
