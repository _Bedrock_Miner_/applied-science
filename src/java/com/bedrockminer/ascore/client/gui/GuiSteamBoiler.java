package com.bedrockminer.ascore.client.gui;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.block.BlockSteamBoiler;
import com.bedrockminer.ascore.client.gui.tabs.Tab;
import com.bedrockminer.ascore.client.gui.tabs.TabConnectionsBase;
import com.bedrockminer.ascore.client.gui.tabs.TabInformation;
import com.bedrockminer.ascore.client.gui.tabs.TabWarnings;
import com.bedrockminer.ascore.inventory.ContainerSteamBoiler;
import com.bedrockminer.ascore.tileentity.TileSteamBoiler;
import com.bedrockminer.ascore.units.FluidVolume;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Temperature;
import com.bedrockminer.ascore.units.Time;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.units.Volume;
import com.google.common.collect.ImmutableList;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraftforge.fluids.FluidRegistry;

/**
 * The Steam boiler's GUI.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class GuiSteamBoiler extends GuiTabbedBase<ContainerSteamBoiler, TileSteamBoiler> {

	/**
	 * Instantiates a new GuiSteamBoiler.
	 *
	 * @param container
	 */
	public GuiSteamBoiler(ContainerSteamBoiler container) {
		super(container, 176, 176);
		this.addTab(new TabOverview(this));
		this.addTab(new TabConnection(this));
		this.addTab(new TabDetails(this));
		this.addTab(new TabProblems(this));
		this.addTab(new TabOptimization(this));
	}

	// Tabs

	private static class TabOverview extends Tab<GuiSteamBoiler> {

		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/steam_boiler_overview.png");
		private static final DecimalFormat SHORT_TEMP_FORMAT = new DecimalFormat("#;-#");

		public TabOverview(GuiSteamBoiler gui) {
			super(gui, Tab.getFilledArray(0, 39), new ItemStack(Blocks.chest));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			GlStateManager.pushMatrix();
			GlStateManager.translate(this.gui().guiLeft, this.gui().guiTop, 0);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

			// Background
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(0, 0, 0, 0, this.gui().xSize, this.gui().ySize);

			// Thermometer
			int length = (int) (this.gui().container().temperature * 0.3);
			if (length < 1)
				length = 1;
			if (length > 30)
				length = 30;
			this.gui().drawTexturedModalRect(145, 52 - length, 180, 30 - length, 4, length);

			// Heat display
			if (this.gui().container().heatSource != null) {
				length = 16 * this.gui().container().burnTime / TileEntityFurnace.getItemBurnTime(this.gui().container().heatSource);
				this.gui().drawTexturedModalRect(98, 76 - length, 190, 16 - length, 16, length);
			}

			// Water level
			GlStateManager.enableBlend();
			Minecraft.getMinecraft().getTextureManager().bindTexture(Minecraft.getMinecraft().getTextureMapBlocks().locationBlocksTexture);
			TextureAtlasSprite sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(FluidRegistry.WATER.getStill().toString());
			length = (int) (this.gui().container().water * 24);
			if (length < 0)
				length = 0;
			if (length > 30)
				length = 30;
			GuiHelper.renderTiledTexture(39, 52 - length, 98, length, sprite);
			GlStateManager.disableBlend();

			// Currently burning item
			if (this.gui().container().heatSource != null) {
				this.gui().itemRender.zLevel = 100.0F;
				this.gui().itemRender.renderItemAndEffectIntoGUI(this.gui().container().heatSource, 116, 60);
				this.gui().itemRender.zLevel = 0.0F;
			}
			GlStateManager.popMatrix();
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			GlStateManager.translate(this.gui().guiLeft, this.gui().guiTop, 0);
			this.gui().fontRendererObj.drawString(Units.formatFrom(this.gui().container().temperature, Temperature.CELSIUS, SHORT_TEMP_FORMAT), 138, 60, 0);
			this.gui().fontRendererObj.drawString(I18n.format("container.general.pressure", Units.formatFrom(this.gui().container().pressure, Pressure.BAR)), 8, 81, 0);
			GlStateManager.translate(-this.gui().guiLeft, -this.gui().guiTop, 0);

			// Tooltips
			//@formatter:off

			// Thermometer
			if (this.gui().isPointInRegion(144, 21, 6, 32, mouseX, mouseY))
				this.gui().drawHoveringText(ImmutableList.of(I18n.format("container.general.temperature", Units.formatFrom(this.gui().container().temperature, Temperature.CELSIUS))), mouseX, mouseY);
			// Water
			if (this.gui().isPointInRegion(38, 21, 100, 32, mouseX, mouseY))
				this.gui().drawHoveringText(ImmutableList.of(
						I18n.format("container.steam_boiler.water", Units.formatFrom(this.gui().container().water, FluidVolume.CUBICMETER)),
						I18n.format("container.general.pressure", Units.formatFrom(this.gui().container().pressure, Pressure.BAR))), mouseX, mouseY);
			// Item
			if (this.gui().isPointInRegion(116, 60, 18, 18, mouseX, mouseY) && this.gui().container().heatSource != null)
				this.gui().renderToolTip(this.gui().container().heatSource, mouseX, mouseY);
			// Flame
			if (this.gui().isPointInRegion(98, 61, 16, 16, mouseX, mouseY) && this.gui().container().heatSource != null)
				this.gui().drawHoveringText(ImmutableList.of(
						I18n.format("container.steam_boiler.burn_time", Units.formatFrom(this.gui().container().burnTime, Time.TICKS)),
						I18n.format("container.steam_boiler.burn_mat_temp", Units.formatFrom(this.gui().container().burnMaterialTemperature, Temperature.CELSIUS))), mouseX, mouseY);
			//@formatter:on
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.overview", I18n.format("container.steam_boiler"));
		}
	}

	private static class TabConnection extends TabConnectionsBase<GuiSteamBoiler> {

		public TabConnection(GuiSteamBoiler gui) {
			super(gui, new ItemStack(ASBlocks.pressurePipe));
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.connections", I18n.format("container.steam_boiler"));
		}

		@Override
		protected int getConnectionCount() {
			return 1;
		}

		@Override
		protected String getConnectionName(int ID) {
			return I18n.format("container.steam_boiler.connection_output");
		}

		@Override
		protected NBTTagCompound getConnectionData(int ID) {
			return this.gui().container().pressureOutData;
		}

		@Override
		protected void setConnectionData(int ID, NBTTagCompound nbt) {
			this.gui().container().sendNotification(0, nbt);
		}

		@Override
		protected Vec3 getRotationCenter() {
			IBlockState state = this.gui().container().te().getWorld().getBlockState(this.gui().container().te().getPos());
			EnumFacing facing = state.getValue(BlockSteamBoiler.FACING);
			return new Vec3(0.5 + facing.getFrontOffsetX() * 0.5, 0.5 + facing.getFrontOffsetY() * 0.5, 0.5 + facing.getFrontOffsetZ() * 0.5);
		}

		@Override
		protected void renderBlock() {
			BlockPos pos = this.gui().container().te().getPos();
			IBlockState state = this.gui().container().te().getWorld().getBlockState(pos);
			GlStateManager.rotate(-90, 0, 1, 0);
			Minecraft.getMinecraft().getBlockRendererDispatcher().renderBlockBrightness(state, 0.8f);
		}

		@Override
		protected float getModelScale() {
			return 35;
		}
	}

	private static class TabDetails extends TabInformation<GuiSteamBoiler> {

		public TabDetails(GuiSteamBoiler gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_details.png"));
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.details", I18n.format("container.steam_boiler"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.steam_boiler.volume", Units.formatFrom(this.gui().container().te().getOutput().getVolume(), Volume.CUBICMETER)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.steam_boiler.water", Units.formatFrom(this.gui().container().water, FluidVolume.CUBICMETER)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.general.pressure", Units.formatFrom(this.gui().container().pressure, Pressure.BAR)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.general.temperature", Units.formatFrom(this.gui().container().temperature, Temperature.CELSIUS)));
			text.add("  \u2022 " + I18n.format("container.steam_boiler.environment_temp", Units.formatFrom(this.gui().container().environmentTemperature, Temperature.CELSIUS)));
			text.add("  \u2022 " + I18n.format("container.steam_boiler.burn_mat_temp", Units.formatFrom(this.gui().container().burnMaterialTemperature, Temperature.CELSIUS)));
			text.add("  \u2022 " + I18n.format("container.steam_boiler.temperature_" + (this.gui().container().getAdjustTemperature() > this.gui().container().temperature ? "rising" : "falling")));
			text.add("  \u2022 " + I18n.format("container.steam_boiler.adjust_temp", Units.formatFrom(this.gui().container().getAdjustTemperature(), Temperature.CELSIUS)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.steam_boiler.boiling_point", Units.formatFrom(100 + 33 * Math.log(this.gui().container().pressure), Temperature.CELSIUS)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.steam_boiler.overall_burn_time", Units.formatFrom(this.gui().container().getOverallBurnTime(), Time.TICKS)));

			return text;
		}
	}

	private static class TabProblems extends TabWarnings<GuiSteamBoiler> {

		public TabProblems(GuiSteamBoiler gui) {
			super(gui);
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.problems", I18n.format("container.steam_boiler"));
		}

		@Override
		public int getWarningCount() {
			int warningCount = 0;
			if (this.gui().container().pressureOutLeaking)
				warningCount++;
			if (this.gui().container().pressureOutNotConnected)
				warningCount++;
			if (this.gui().container().temperature < 100 + 33 * Math.log(this.gui().container().pressure))
				warningCount++;
			if (this.gui().container().water <= 0)
				warningCount++;

			return warningCount;
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			if (this.gui().container().pressureOutLeaking) {
				text.add("\u26A0 " + I18n.format("container.steam_boiler.output_leaking"));
				text.add("");
			}
			if (this.gui().container().pressureOutNotConnected) {
				text.add("\u26A0 " + I18n.format("container.steam_boiler.output_not_connected"));
				text.add("");
			}
			if (this.gui().container().water <= 0) {
				text.add("\u26A0 " + I18n.format("container.steam_boiler.no_water"));
				text.add("");
			}
			if (this.gui().container().temperature < 100 + 33 * Math.log(this.gui().container().pressure)) {
				text.add("\u26A0 " + I18n.format("container.steam_boiler.too_cold"));
				text.add("");
			}
			if (text.size() == 0)
				text.add("\u2713 " + I18n.format("container.general.no_problems"));

			return text;
		}
	}

	private static class TabOptimization extends TabInformation<GuiSteamBoiler> {

		public TabOptimization(GuiSteamBoiler gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_optimization.png"));
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.optimization", I18n.format("container.steam_boiler"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.steam_boiler.hint_less_water"));
			text.add("");
			text.add("\u2022 " + I18n.format("container.steam_boiler.hint_security"));
			text.add("");
			text.add("\u2022 " + I18n.format("container.steam_boiler.hint_heating_up"));
			text.add("");
			text.add("\u2022 " + I18n.format("container.steam_boiler.hint_outer_blocks"));

			return text;
		}
	}
}
