package com.bedrockminer.ascore.client.gui;

import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.tabs.TabInformation;
import com.bedrockminer.ascore.client.gui.tabs.TabTextField;
import com.bedrockminer.ascore.inventory.ContainerPressureGauge;
import com.bedrockminer.ascore.tileentity.TilePressureGauge;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.google.common.collect.ImmutableList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class GuiPressureGauge extends GuiTabbedBase<ContainerPressureGauge, TilePressureGauge> {

	public GuiPressureGauge(ContainerPressureGauge container) {
		super(container, 176, 176);
		this.addTab(new TabSettings(this));
		this.addTab(new TabRedstone(this));
		this.addTab(new TabDetails(this));
	}

	private static class TabSettings extends TabTextField<GuiPressureGauge> {

		private static final ResourceLocation icon = new ResourceLocation("ascore:textures/gui/icons/tab_settings.png");
		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/empty.png");

		public TabSettings(GuiPressureGauge gui) {
			super(gui, new int[0], icon);

			this.addTextField(new GuiTextField(0, Minecraft.getMinecraft().fontRendererObj, 31, 35, 114, 20));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(this.gui().guiLeft, this.gui().guiTop, 0, 0, this.gui().xSize, this.gui().ySize);
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			super.drawForeground(mouseX, mouseY);

			Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.pressure_gauge.display_max_heading"), this.gui().guiLeft + 8, this.gui().guiTop + 24, 0);
		}

		@Override
		public boolean isFieldValid(int ID, String text) {
			if (!this.canParseDouble(text))
				return false;
			double d = Units.convertFromUser(Double.parseDouble(text), Pressure.BAR);
			return d >= 11 && d <= 501;
		}

		@Override
		public String getTextForField(int ID) {
			return Units.formatFrom(this.gui().container().maxDisplay, Pressure.BAR);
		}

		@Override
		public String getEditableTextForField(int ID) {
			return "" + Units.convertFromInternal(this.gui().container().maxDisplay, Pressure.BAR);
		}

		@Override
		public void submit(int ID, String text) {
			if (this.canParseDouble(text)) {
				double d = MathHelper.clamp_double(Units.convertFromUser(Double.parseDouble(text), Pressure.BAR), 11, 501);
				this.gui().container().sendNotification(0, new NBTTagDouble(d));
			}
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.settings", I18n.format("container.pressure_gauge"));
		}
	}

	private static class TabRedstone extends TabTextField<GuiPressureGauge> {

		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/empty.png");

		private GuiButton mode;

		public TabRedstone(GuiPressureGauge gui) {
			super(gui, new int[0], new ItemStack(Items.redstone));
			this.mode = new GuiButton(0, 31, 36, 114, 20, "");
			this.addTextField(new GuiTextField(0, Minecraft.getMinecraft().fontRendererObj, 31, 80, 114, 20));
			this.addTextField(new GuiTextField(1, Minecraft.getMinecraft().fontRendererObj, 31, 124, 114, 20));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(this.gui().guiLeft, this.gui().guiTop, 0, 0, this.gui().xSize, this.gui().ySize);
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			this.mode.displayString = I18n.format("container.pressure_gauge.redstone_" + (this.gui().container().redstoneBinary ? "binary" : "floating"));
			this.mode.xPosition = this.gui().guiLeft + 31;
			this.mode.yPosition = this.gui().guiTop + 36;

			super.drawForeground(mouseX, mouseY);

			// Text
			Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.pressure_gauge.redstone_mode_heading"), this.gui().guiLeft + 8, this.gui().guiTop + 24, 0);
			Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.pressure_gauge.min_redstone_pressure"), this.gui().guiLeft + 8, this.gui().guiTop + 68, 0);
			Minecraft.getMinecraft().fontRendererObj.drawString(I18n.format("container.pressure_gauge.max_redstone_pressure"), this.gui().guiLeft + 8, this.gui().guiTop + 112, 0);
		}

		@Override
		public List<GuiButton> getButtons() {
			return ImmutableList.of(this.mode);
		}

		@Override
		public void buttonClicked(GuiButton button) {
			this.gui().container().redstoneBinary = !this.gui().container().redstoneBinary;
			this.gui().container().sendNotification(1, new NBTTagByte(this.gui().container().redstoneBinary ? (byte) 1 : (byte) 0));
		}

		@Override
		public boolean isFieldValid(int ID, String text) {
			if (!this.canParseDouble(text))
				return false;
			double d = Units.convertFromUser(Double.parseDouble(text), Pressure.BAR);
			switch (ID) {
			case 0:
				return d >= 0 && d <= this.gui().container().te().getMaximumPressure() && d <= this.gui().container().maxRedstonePressure;
			case 1:
				return d >= 0 && d <= this.gui().container().te().getMaximumPressure() && d >= this.gui().container().minRedstonePressure;
			}
			return false;
		}

		@Override
		public String getTextForField(int ID) {
			switch (ID) {
			case 0:
				return Units.formatFrom(this.gui().container().minRedstonePressure, Pressure.BAR);
			case 1:
				return Units.formatFrom(this.gui().container().maxRedstonePressure, Pressure.BAR);
			}
			return "";
		}

		@Override
		public String getEditableTextForField(int ID) {
			switch (ID) {
			case 0:
				return "" + Units.convertFromInternal(this.gui().container().minRedstonePressure, Pressure.BAR);
			case 1:
				return "" + Units.convertFromInternal(this.gui().container().maxRedstonePressure, Pressure.BAR);
			}
			return "";
		}

		@Override
		public void submit(int ID, String text) {
			if (this.canParseDouble(text)) {
				double d = Units.convertFromUser(Double.parseDouble(text), Pressure.BAR);

				switch (ID) {
				case 0:
					d = MathHelper.clamp_double(d, 0, Math.min(this.gui().container().te().getMaximumPressure(), this.gui().container().maxRedstonePressure));
					this.gui().container().sendNotification(2, new NBTTagDouble(d));
					break;
				case 1:
					d = MathHelper.clamp_double(d, Math.max(0, this.gui().container().minRedstonePressure), this.gui().container().te().getMaximumPressure());
					this.gui().container().sendNotification(3, new NBTTagDouble(d));
					break;
				}
			}
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.redstone", I18n.format("container.pressure_gauge"));
		}
	}

	private static class TabDetails extends TabInformation<GuiPressureGauge> {

		public TabDetails(GuiPressureGauge gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_details.png"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.general.pressure", Units.formatFrom(this.gui().container().pressure, Pressure.BAR)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.pressure_gauge.redstone_mode", I18n.format("container.pressure_gauge.redstone_" + (this.gui().container().redstoneBinary ? "binary" : "floating"))));

			if (this.gui().container().redstoneBinary) {
				text.add("  \u2022 " + I18n.format("container.pressure_gauge.redstone_binary_details", Units.formatFrom(this.gui().container().minRedstonePressure, Pressure.BAR), Units.formatFrom(this.gui().container().maxRedstonePressure, Pressure.BAR)));
			} else {
				text.add("  \u2022 " + I18n.format("container.pressure_gauge.redstone_floating_details"));

				text.add("");
				text.add("  " + I18n.format("container.pressure_gauge.redstone_floating_strength_min", 0, Units.formatFrom(this.getPressureFromRedstone(1), Pressure.BAR)));

				for (int i = 1; i < 15; i++) {
					text.add("  " + I18n.format("container.pressure_gauge.redstone_floating_strength", i, Units.formatFrom(this.getPressureFromRedstone(i), Pressure.BAR), Units.formatFrom(this.getPressureFromRedstone(i + 1), Pressure.BAR)));
				}

				text.add("  " + I18n.format("container.pressure_gauge.redstone_floating_strength_max", 15, Units.formatFrom(this.getPressureFromRedstone(15), Pressure.BAR)));
			}

			return text;
		}

		private double getPressureFromRedstone(int redstone) {
			return ((redstone - 1) / 15.0 * (this.gui().container().maxRedstonePressure - this.gui().container().minRedstonePressure + 1)) + this.gui().container().minRedstonePressure;
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.details", I18n.format("container.pressure_gauge"));
		}

	}
}
