package com.bedrockminer.ascore.client.gui;

import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.client.gui.tabs.TabConnectionsBase;
import com.bedrockminer.ascore.client.gui.tabs.TabInformation;
import com.bedrockminer.ascore.client.gui.tabs.TabTextField;
import com.bedrockminer.ascore.client.gui.tabs.TabWarnings;
import com.bedrockminer.ascore.inventory.ContainerCreativeMotor;
import com.bedrockminer.ascore.tileentity.TileCreativeMotor;
import com.bedrockminer.ascore.units.RotationSpeed;
import com.bedrockminer.ascore.units.RotationalInertia;
import com.bedrockminer.ascore.units.Torque;
import com.bedrockminer.ascore.units.Units;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;

/**
 * The Creative Motor's GUI.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class GuiCreativeMotor extends GuiTabbedBase<ContainerCreativeMotor, TileCreativeMotor> {

	public GuiCreativeMotor(ContainerCreativeMotor container) {
		super(container, 176, 176);
		this.addTab(new TabSettings(this));
		this.addTab(new TabConnections(this));
		this.addTab(new TabDetails(this));
		this.addTab(new TabProblems(this));
	}

	private static class TabSettings extends TabTextField<GuiCreativeMotor> {

		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/empty.png");
		private static final ResourceLocation icon = new ResourceLocation("ascore:textures/gui/icons/tab_settings.png");

		public TabSettings(GuiCreativeMotor gui) {
			super(gui, new int[0], icon);

			this.addTextField(new GuiTextField(0, Minecraft.getMinecraft().fontRendererObj, 31, 36, 114, 20));
			this.addTextField(new GuiTextField(1, Minecraft.getMinecraft().fontRendererObj, 31, 80, 114, 20));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(this.gui().guiLeft, this.gui().guiTop, 0, 0, this.gui().xSize, this.gui().ySize);
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			super.drawForeground(mouseX, mouseY);

			// Title lines
			this.gui().fontRendererObj.drawString(I18n.format("container.creative_motor.max_speed"), this.gui().guiLeft + 8, this.gui().guiTop + 24, 0);
			this.gui().fontRendererObj.drawString(I18n.format("container.creative_motor.torque"), this.gui().guiLeft + 8, this.gui().guiTop + 68, 0);
		}

		@Override
		public boolean isFieldValid(int ID, String text) {
			if (!this.canParseDouble(text))
				return false;
			double d = Double.parseDouble(text);

			switch (ID) {
			case 0:
				d = Units.convertFromUser(d, RotationSpeed.INTERNAL);
				return d >= 0 && d <= this.gui().container().te().getSpeedTreshold();
			case 1:
				d = Units.convertFromUser(d, Torque.NEWTONMETER);
				return d >= 0 && d <= this.gui().container().te().getTorqueTreshold();
			}
			return false;
		}

		@Override
		public String getTextForField(int ID) {
			switch (ID) {
			case 0:
				return Units.formatFrom(this.gui().container().maxSpeed, RotationSpeed.INTERNAL);
			case 1:
				return Units.formatFrom(this.gui().container().torque, Torque.NEWTONMETER);
			}
			return "";
		}

		@Override
		public String getEditableTextForField(int ID) {
			switch (ID) {
			case 0:
				return "" + Units.convertFromInternal(this.gui().container().maxSpeed, RotationSpeed.INTERNAL);
			case 1:
				return "" + Units.convertFromInternal(this.gui().container().torque, Torque.NEWTONMETER);
			}
			return "";
		}

		@Override
		public void submit(int ID, String text) {
			if (this.canParseDouble(text)) {
				double d = Double.parseDouble(text);

				switch (ID) {
				case 0:
					d = Units.convertFromUser(d, RotationSpeed.INTERNAL);
					this.gui().container().sendNotification(0, new NBTTagDouble(MathHelper.clamp_double(d, 0, this.gui().container().te().getSpeedTreshold())));
					break;
				case 1:
					d = Units.convertFromUser(d, Torque.NEWTONMETER);
					this.gui().container().sendNotification(1, new NBTTagDouble(MathHelper.clamp_double(d, 0, this.gui().container().te().getTorqueTreshold())));
					break;
				}
			}
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.settings", I18n.format("container.creative_motor"));
		}
	}

	private static class TabConnections extends TabConnectionsBase<GuiCreativeMotor> {

		public TabConnections(GuiCreativeMotor gui) {
			super(gui, new ItemStack(ASBlocks.rotationAxle));
		}

		@Override
		protected int getConnectionCount() {
			return 1;
		}

		@Override
		protected String getConnectionName(int ID) {
			return I18n.format("container.creative_motor.connection");
		}

		@Override
		protected NBTTagCompound getConnectionData(int ID) {
			return this.gui().container().connectionData;
		}

		@Override
		protected void setConnectionData(int ID, NBTTagCompound nbt) {
			this.gui().container().sendNotification(2, nbt);
		}

		@Override
		protected Vec3 getRotationCenter() {
			return new Vec3(0.5, 0.5, 0.5);
		}

		@Override
		protected void renderBlock() {
			BlockPos pos = this.gui().container().te().getPos();
			IBlockState state = this.gui().container().te().getWorld().getBlockState(pos);
			GlStateManager.rotate(-90, 0, 1, 0);
			Minecraft.getMinecraft().getBlockRendererDispatcher().renderBlockBrightness(state, 0.8f);
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.connections", I18n.format("container.creative_motor"));
		}
	}

	private static class TabDetails extends TabInformation<GuiCreativeMotor> {

		public TabDetails(GuiCreativeMotor gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_details.png"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.general.rot_inertia", Units.formatFrom(50, RotationalInertia.KGM2)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.general.resistance", Units.formatFrom(2, Torque.NEWTONMETER)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.creative_motor.max_speed_val", Units.formatFrom(this.gui().container().maxSpeed, RotationSpeed.INTERNAL)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.creative_motor.torque_val", Units.formatFrom(this.gui().container().torque, Torque.NEWTONMETER)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.general.rotation_speed", Units.formatFrom(this.gui().container().speed, RotationSpeed.INTERNAL)));

			return text;
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.details", I18n.format("container.creative_motor"));
		}
	}

	private static class TabProblems extends TabWarnings<GuiCreativeMotor> {

		public TabProblems(GuiCreativeMotor gui) {
			super(gui);
		}

		@Override
		public int getWarningCount() {
			return this.gui().container().connectionNotConnected ? 1 : 0;
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();
			if (this.gui().container().connectionNotConnected)
				text.add("\u26A0 " + I18n.format("container.creative_motor.output_not_connected"));
			else
				text.add("\u2713 " + I18n.format("container.general.no_problems"));
			return text;
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.problems", I18n.format("container.creative_motor"));
		}
	}
}
