package com.bedrockminer.ascore.client.gui;

import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.tabs.TabInformation;
import com.bedrockminer.ascore.client.gui.tabs.TabTextField;
import com.bedrockminer.ascore.inventory.ContainerAirFlowLimiter;
import com.bedrockminer.ascore.tileentity.TileAirFlowLimiter;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.units.Volume;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class GuiAirFlowLimiter extends GuiTabbedBase<ContainerAirFlowLimiter, TileAirFlowLimiter> {

	public GuiAirFlowLimiter(ContainerAirFlowLimiter container) {
		super(container, 176, 176);
		this.addTab(new TabSettings(this));
		this.addTab(new TabDetails(this));
	}

	private static class TabSettings extends TabTextField<GuiAirFlowLimiter> {

		private static final ResourceLocation icon = new ResourceLocation("ascore:textures/gui/icons/tab_settings.png");
		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/empty.png");

		public TabSettings(GuiAirFlowLimiter gui) {
			super(gui, new int[0], icon);

			this.addTextField(new GuiTextField(0, Minecraft.getMinecraft().fontRendererObj, 31, 36, 114, 20));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(this.gui().guiLeft, this.gui().guiTop, 0, 0, this.gui().xSize, this.gui().ySize);
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			super.drawForeground(mouseX, mouseY);

			// Text
			this.gui().fontRendererObj.drawString(I18n.format("container.air_flow_limiter.limit_heading"), this.gui().guiLeft + 8, this.gui().guiTop + 24, 0);
		}

		@Override
		public boolean isFieldValid(int ID, String text) {
			if (!this.canParseDouble(text))
				return false;
			double d = Units.convertFromUser(Double.parseDouble(text), Volume.CUBICMETER) / 20.0;
			return d > 0 && d <= this.gui().container().te().getMaxFlowLimit();
		}

		@Override
		public String getTextForField(int ID) {
			return Units.formatFrom(this.gui().container().limit * 20.0, Volume.CUBICMETER) + "/s";
		}

		@Override
		public String getEditableTextForField(int ID) {
			return "" + Units.convertFromInternal(this.gui().container().limit * 20.0, Volume.CUBICMETER);
		}

		@Override
		public void submit(int ID, String text) {
			if (this.canParseDouble(text)) {
				double d = MathHelper.clamp_double(Units.convertFromUser(Double.parseDouble(text), Volume.CUBICMETER) / 20.0, 1E-10, this.gui().container().te().getMaxFlowLimit());
				this.gui().container().sendNotification(0, new NBTTagDouble(d));
			}
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.settings", I18n.format("container.air_flow_limiter"));
		}
	}

	private static class TabDetails extends TabInformation<GuiAirFlowLimiter> {

		public TabDetails(GuiAirFlowLimiter gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_details.png"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.air_flow_limiter.flow_limit", Units.formatFrom(20 * this.gui().container().limit, Volume.CUBICMETER) + "/s"));
			text.add("");
			if (this.gui().container().avgPressure == 1)
				text.add("\u2022 " + I18n.format("container.air_flow_limiter.leak", Units.formatFrom(1, Pressure.BAR)));
			else
				text.add("\u2022 " + I18n.format("container.air_flow_limiter.avg_pressure", Units.formatFrom(this.gui().container().avgPressure, Pressure.BAR)));

			text.add("  \u2022 " + I18n.format("container.air_flow_limiter.pressure_a", this.gui().container().te().getOrientation().getName(), Units.formatFrom(this.gui().container().pressureA, Pressure.BAR)));
			text.add("  \u2022 " + I18n.format("container.air_flow_limiter.pressure_b", this.gui().container().te().getOrientation().getName(), Units.formatFrom(this.gui().container().pressureB, Pressure.BAR)));

			return text;
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.details", I18n.format("container.air_flow_limiter"));
		}
	}
}
