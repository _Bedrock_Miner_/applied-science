package com.bedrockminer.ascore.client.gui.config;

import static net.minecraft.util.EnumChatFormatting.GRAY;
import static net.minecraft.util.EnumChatFormatting.ITALIC;
import static net.minecraft.util.EnumChatFormatting.RED;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.bedrockminer.ascore.AppliedScience;
import com.bedrockminer.ascore.config.Cfg;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;

public class ASConfigGui implements IModGuiFactory {

	@Override
	public void initialize(Minecraft minecraftInstance) {
	}

	@Override
	public Class<? extends GuiScreen> mainConfigGuiClass() {
		return ASConfigGuiClass.class;
	}

	@Override
	public Set<RuntimeOptionCategoryElement> runtimeGuiCategories() {
		return null;
	}

	@Override
	public RuntimeOptionGuiHandler getHandlerFor(RuntimeOptionCategoryElement element) {
		return null;
	}

	public static class ASConfigGuiClass extends GuiConfig {

		public ASConfigGuiClass(GuiScreen parentScreen) {
			super(parentScreen, getElements(), AppliedScience.MODID, false, false, "" + RED + "Applied Science Configuration", "" + GRAY + ITALIC + "root");
		}

		public static List<IConfigElement> getElements() {
			List<IConfigElement> elements = new ArrayList<IConfigElement>();
			for (String category: Cfg.config.getCategoryNames())
				if (!category.equals("general"))
					elements.add(new ConfigElement(Cfg.config.getCategory(category)));

			return elements;
		}

		@Override
		public void onGuiClosed() {
			super.onGuiClosed();
			if (!(this.parentScreen instanceof GuiConfig))
				Cfg.config.save();
		}
	}
}
