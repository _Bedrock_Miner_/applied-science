package com.bedrockminer.ascore.client.gui;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

/**
 * This class contains some helper methods for easier GUI rendering.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class GuiHelper {

	public static void renderTiledTexture(int x, int y, int width, int height, TextureAtlasSprite texture) {
		Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer worldrenderer = tessellator.getWorldRenderer();
		worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX);

		int dx = 0;
		while (dx < width) {
			int imgwidth = Math.min(width - dx, texture.getIconWidth());
			int dy = 0;
			while (dy < height) {
				int imgheight = Math.min(height - dy, texture.getIconHeight());
				//@formatter:off
		        worldrenderer.pos(x + dx, 				y + dy + imgheight, 0).tex(texture.getMinU(), texture.getInterpolatedV((double) imgheight / texture.getIconHeight() * 16)).endVertex();
		        worldrenderer.pos(x + dx + imgwidth, 	y + dy + imgheight, 0).tex(texture.getInterpolatedU((double) imgwidth / texture.getIconWidth() * 16), texture.getInterpolatedV((double) imgheight / texture.getIconHeight() * 16)).endVertex();
		        worldrenderer.pos(x + dx + imgwidth,	y + dy, 			0).tex(texture.getInterpolatedU((double) imgwidth / texture.getIconWidth() * 16), texture.getMinV()).endVertex();
		        worldrenderer.pos(x + dx, 				y + dy, 			0).tex(texture.getMinU(), texture.getMinV()).endVertex();
		        //@formatter:on

		        dy += imgheight;
			}
			dx += imgwidth;
		}

		tessellator.draw();
	}
}
