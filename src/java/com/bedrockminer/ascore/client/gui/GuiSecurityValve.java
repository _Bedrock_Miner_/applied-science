package com.bedrockminer.ascore.client.gui;

import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.tabs.TabInformation;
import com.bedrockminer.ascore.client.gui.tabs.TabTextField;
import com.bedrockminer.ascore.inventory.ContainerSecurityValve;
import com.bedrockminer.ascore.tileentity.TileSecurityValve;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class GuiSecurityValve extends GuiTabbedBase<ContainerSecurityValve, TileSecurityValve> {

	public GuiSecurityValve(ContainerSecurityValve container) {
		super(container, 176, 176);
		this.addTab(new TabSettings(this));
		this.addTab(new TabDetails(this));
	}

	private static class TabSettings extends TabTextField<GuiSecurityValve> {

		private static final ResourceLocation icon = new ResourceLocation("ascore:textures/gui/icons/tab_settings.png");
		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/empty.png");

		public TabSettings(GuiSecurityValve gui) {
			super(gui, new int[0], icon);
			this.addTextField(new GuiTextField(0, Minecraft.getMinecraft().fontRendererObj, 31, 36, 114, 20));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(this.gui().guiLeft, this.gui().guiTop, 0, 0, this.gui().xSize, this.gui().ySize);
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			super.drawForeground(mouseX, mouseY);

			// Text
			this.gui().fontRendererObj.drawString(I18n.format("container.security_valve.treshold_heading"), this.gui().guiLeft + 8, this.gui().guiTop + 24, 0);
			this.gui().fontRendererObj.drawSplitString(I18n.format("container.security_valve.pressure_info"), this.gui().guiLeft + 8, this.gui().guiTop + 72, 160, 0);
		}


		@Override
		public boolean isFieldValid(int ID, String text) {
			if (!this.canParseDouble(text))
				return false;
			double d = Units.convertFromUser(Double.parseDouble(text), Pressure.BAR);
			return d > 1 && d <= this.gui().container().te().getMaxPressureTreshold();
		}

		@Override
		public String getTextForField(int ID) {
			return Units.formatFrom(this.gui().container().treshold, Pressure.BAR);
		}

		@Override
		public String getEditableTextForField(int ID) {
			return "" + Units.convertFromInternal(this.gui().container().treshold, Pressure.BAR);
		}

		@Override
		public void submit(int ID, String text) {
			if (this.canParseDouble(text)) {
				double d = MathHelper.clamp_double(Units.convertFromUser(Double.parseDouble(text), Pressure.BAR), 1E-10, this.gui().container().te().getMaxPressureTreshold());
				this.gui().container().sendNotification(0, new NBTTagDouble(d));
			}
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.settings", I18n.format("container.security_valve"));
		}
	}

	private static class TabDetails extends TabInformation<GuiSecurityValve> {

		public TabDetails(GuiSecurityValve gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_details.png"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.security_valve.treshold", Units.formatFrom(this.gui().container().treshold, Pressure.BAR)));
			text.add("");
			text.add("\u2022 " + I18n.format("container.general.pressure", Units.formatFrom(this.gui().container().pressure, Pressure.BAR)));

			return text;
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.details", I18n.format("container.security_valve"));
		}
	}
}
