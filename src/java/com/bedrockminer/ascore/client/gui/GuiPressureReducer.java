package com.bedrockminer.ascore.client.gui;

import java.util.LinkedList;
import java.util.List;

import com.bedrockminer.ascore.client.gui.tabs.TabInformation;
import com.bedrockminer.ascore.client.gui.tabs.TabTextField;
import com.bedrockminer.ascore.inventory.ContainerPressureReducer;
import com.bedrockminer.ascore.tileentity.TilePressureReducer;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class GuiPressureReducer extends GuiTabbedBase<ContainerPressureReducer, TilePressureReducer> {

	public GuiPressureReducer(ContainerPressureReducer container) {
		super(container, 176, 176);
		this.addTab(new TabSettings(this));
		this.addTab(new TabDetails(this));
	}

	private static class TabSettings extends TabTextField<GuiPressureReducer> {

		private static final ResourceLocation icon = new ResourceLocation("ascore:textures/gui/icons/tab_settings.png");
		private static final ResourceLocation background = new ResourceLocation("ascore:textures/gui/container/empty.png");

		public TabSettings(GuiPressureReducer gui) {
			super(gui, new int[0], icon);

			this.addTextField(new GuiTextField(0, Minecraft.getMinecraft().fontRendererObj, 31, 36, 114, 20));
		}

		@Override
		public void drawBackground(int mouseX, int mouseY, float partialTicks) {
			Minecraft.getMinecraft().getTextureManager().bindTexture(background);
			this.gui().drawTexturedModalRect(this.gui().guiLeft, this.gui().guiTop, 0, 0, this.gui().xSize, this.gui().ySize);
		}

		@Override
		public void drawForeground(int mouseX, int mouseY) {
			super.drawForeground(mouseX, mouseY);

			// Text
			this.gui().fontRendererObj.drawString(I18n.format("container.pressure_reducer.limit_heading"), this.gui().guiLeft + 8, this.gui().guiTop + 24, 0);
			this.gui().fontRendererObj.drawSplitString(I18n.format("container.pressure_reducer.connections"), this.gui().guiLeft + 8, this.gui().guiTop + 80, 160, 0);
		}

		@Override
		public boolean isFieldValid(int ID, String text) {
			if (!this.canParseDouble(text))
				return false;
			double d = Units.convertFromUser(Double.parseDouble(text), Pressure.BAR);
			return d > 0 && d <= 501;
		}

		@Override
		public String getTextForField(int ID) {
			return Units.formatFrom(this.gui().container().limit, Pressure.BAR);
		}

		@Override
		public String getEditableTextForField(int ID) {
			return "" + Units.convertFromInternal(this.gui().container().limit, Pressure.BAR);
		}

		@Override
		public void submit(int ID, String text) {
			if (this.canParseDouble(text)) {
				double d = MathHelper.clamp_double(Units.convertFromUser(Double.parseDouble(text), Pressure.BAR), 1, 501);
				this.gui().container().sendNotification(0, new NBTTagDouble(d));
			}
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.settings", I18n.format("container.air_flow_limiter"));
		}
	}

	private static class TabDetails extends TabInformation<GuiPressureReducer> {

		public TabDetails(GuiPressureReducer gui) {
			super(gui, new ResourceLocation("ascore:textures/gui/icons/tab_details.png"));
		}

		@Override
		public List<String> getText() {
			List<String> text = new LinkedList<String>();

			text.add("\u2022 " + I18n.format("container.pressure_reducer.limit", Units.formatFrom(this.gui().container().limit, Pressure.BAR)));
			text.add("");
			text.add("  \u2022 " + I18n.format("container.pressure_reducer.input_pressure", Units.formatFrom(this.gui().container().pressureIn, Pressure.BAR)));
			text.add("  \u2022 " + I18n.format("container.pressure_reducer.output_pressure", Units.formatFrom(this.gui().container().pressureOut, Pressure.BAR)));

			return text;
		}

		@Override
		public String getTitle() {
			return I18n.format("container.tab.details", I18n.format("container.air_flow_limiter"));
		}
	}
}
