package com.bedrockminer.ascore.client.event;

import com.bedrockminer.ascore.client.render.block.models.ModelBase;

import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;

public class ResourceReloadListener implements IResourceManagerReloadListener {

	@Override
	public void onResourceManagerReload(IResourceManager resourceManager) {
		for (ModelBase model: ModelBase.models) {
			model.loadSubmodels();
		}
	}

}
