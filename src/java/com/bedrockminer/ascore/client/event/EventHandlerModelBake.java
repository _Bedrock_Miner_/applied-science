package com.bedrockminer.ascore.client.event;

import com.bedrockminer.ascore.client.render.block.models.ModelAirFlowLimiter;
import com.bedrockminer.ascore.client.render.block.models.ModelPressurePipe;
import com.bedrockminer.ascore.client.render.block.models.ModelPressureReducer;
import com.bedrockminer.ascore.client.render.block.models.ModelPressureValve;
import com.bedrockminer.ascore.client.render.block.models.ModelSecurityValve;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * An event handler for adding code driven models to different blocks.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class EventHandlerModelBake {

	@SubscribeEvent
	public void onBakeModels(ModelBakeEvent e) {
		e.modelRegistry.putObject(new ModelResourceLocation("ascore:pressure_pipe", null), new ModelPressurePipe());
		e.modelRegistry.putObject(new ModelResourceLocation("ascore:pressure_valve", null), new ModelPressureValve());
		e.modelRegistry.putObject(new ModelResourceLocation("ascore:security_valve", null), new ModelSecurityValve());
		e.modelRegistry.putObject(new ModelResourceLocation("ascore:air_flow_limiter", null), new ModelAirFlowLimiter());
		e.modelRegistry.putObject(new ModelResourceLocation("ascore:pressure_gauge", null), new ModelPressurePipe());
		e.modelRegistry.putObject(new ModelResourceLocation("ascore:pressure_reducer", null), new ModelPressureReducer());
	}
}
