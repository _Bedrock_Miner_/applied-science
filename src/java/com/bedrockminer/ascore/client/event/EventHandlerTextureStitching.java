package com.bedrockminer.ascore.client.event;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * An event handler for adding textures to the texture map.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public final class EventHandlerTextureStitching {

	@SubscribeEvent
	public void onStitchingTextures(TextureStitchEvent.Pre e) {
		// Slot icons
		r(e, "gui/icons/slot_coal");
		r(e, "gui/icons/slot_bucket");
		// Textures for code driven renderer
		r(e, "blocks/pressure_pipe");
		r(e, "blocks/pressure_valve");
		r(e, "blocks/pressure_valve_closed");
		r(e, "blocks/pressure_valve_handle_open");
		r(e, "blocks/pressure_valve_handle_closed");
		r(e, "blocks/security_valve");
		r(e, "blocks/air_flow_limiter");
		r(e, "blocks/pressure_reducer_pipe");
		r(e, "blocks/flywheel_outer");
		r(e, "blocks/flywheel_inner");
	}

	private static void r(TextureStitchEvent.Pre e, String texture) {
		e.map.registerSprite(new ResourceLocation("ascore:" + texture));
	}
}
