package com.bedrockminer.ascore.client.render.block;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.client.render.item.SimpleMeshDefinition;

import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

public final class BlockRenderRegistrar {

	public static void preInit() {
		ignoreAll(ASBlocks.pressurePipe);
		ignoreAll(ASBlocks.pressureValve);
		ignoreAll(ASBlocks.securityValve);
		ignoreAll(ASBlocks.airFlowLimiter);
		ignoreAll(ASBlocks.pressureGauge);
		ignoreAll(ASBlocks.pressureReducer);
		ignoreAll(ASBlocks.flywheel);
	}

	public static void init() {
		regMeshDefinition(ASBlocks.pressurePipe, new SimpleMeshDefinition(new ModelResourceLocation("ascore:pressure_pipe")));
		regMeshDefinition(ASBlocks.pressureValve, new SimpleMeshDefinition(new ModelResourceLocation("ascore:pressure_valve")));
		regMeshDefinition(ASBlocks.securityValve, new SimpleMeshDefinition(new ModelResourceLocation("ascore:security_valve")));
		regMeshDefinition(ASBlocks.airFlowLimiter, new SimpleMeshDefinition(new ModelResourceLocation("ascore:air_flow_limiter")));
		reg(ASBlocks.pressureGauge);
		regMeshDefinition(ASBlocks.pressureReducer, new SimpleMeshDefinition(new ModelResourceLocation("ascore:pressure_reducer")));
		reg(ASBlocks.pressureStorage);
		reg(ASBlocks.steamBoiler);
		reg(ASBlocks.creativePressureGenerator);

		reg(ASBlocks.rotationAxle);
		regMeshDefinition(ASBlocks.flywheel, new SimpleMeshDefinition(new ModelResourceLocation("ascore:flywheel")));
		reg(ASBlocks.creativeMotor);
	}

	// ========================================================================
	// Utility methods

	public static void reg(Block block) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), 0, new ModelResourceLocation("ascore:" + block.getUnlocalizedName().substring(5), "inventory"));
	}

	public static void regForAllMeta(Block block) {
		for (int i = 0; i < 16; i++)
			reg(block, i);
	}

	public static void reg(Block block, int meta) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
	    .register(Item.getItemFromBlock(block), meta, new ModelResourceLocation("ascore:" + block.getUnlocalizedName().substring(5), "inventory"));
	}

	public static void ignoreAll(Block block) {
		StateMap.Builder b = new StateMap.Builder();
		BlockState state = block.getBlockState();
		for (IProperty<?> property: state.getProperties()) {
			b.ignore(property);
		}
		ModelLoader.setCustomStateMapper(block, b.build());
	}

	public static void regMeshDefinition(Block block, ItemMeshDefinition def) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), def);
	}
}
