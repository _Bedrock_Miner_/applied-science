package com.bedrockminer.ascore.client.render.block.models;

import static net.minecraftforge.client.model.TRSRTransformation.quatFromYXZDegrees;

import javax.vecmath.Vector3f;

import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.block.model.ModelBlock;
import net.minecraft.client.resources.model.ModelRotation;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ITransformation;
import net.minecraftforge.client.model.TRSRTransformation;

@SuppressWarnings("deprecation")
public abstract class ModelPipeBase extends ModelBase {

	protected ModelBlock pipe_part;
	protected ModelBlock pipe_cap;

	@Override
	public void loadSubmodels() {
		this.pipe_part = this.loadSubmodel(new ResourceLocation("ascore:models/block/pressure_pipe_part.json"));
		this.pipe_cap = this.loadSubmodel(new ResourceLocation("ascore:models/block/pressure_pipe_cap.json"));
	}

	@Override
	public void putQuads() {
		if (this.isItem()) {
			this.addSubmodel(this.pipe_part, ModelRotation.X0_Y0);
			this.addSubmodel(this.pipe_part, ModelRotation.X0_Y180);
			this.addSubmodel(this.pipe_cap, ModelRotation.X0_Y90);
			this.addSubmodel(this.pipe_cap, ModelRotation.X90_Y0);
			this.addSubmodel(this.pipe_cap, ModelRotation.X0_Y270);
			this.addSubmodel(this.pipe_cap, ModelRotation.X270_Y0);
		} else {
			for (EnumFacing direction: EnumFacing.values()) {
				ModelBlock m = this.pipe_cap;
				if (this.pipePart(direction))
					m = this.pipe_part;
				this.addSubmodel(m, new TRSRTransformation(direction));
			}
		}
	}

	public abstract boolean pipePart(EnumFacing side);

	@Override
	public ITransformation getTransformation(TransformType renderType) {
		switch (renderType) {
		case THIRD_PERSON:
			return new TRSRTransformation(new Vector3f(0, 0.09375f, -0.171875f), quatFromYXZDegrees(new Vector3f(10, -45, 170)), new Vector3f(0.375f, 0.375f, 0.375f), null);
		case FIXED:
			return new TRSRTransformation(new Vector3f(0, 0, -0.02f), quatFromYXZDegrees(new Vector3f(0, 90, 0)), new Vector3f(1.2f, 1.2f, 1.2f), null);
		default:
			return ModelRotation.X0_Y0;
		}
	}

}
