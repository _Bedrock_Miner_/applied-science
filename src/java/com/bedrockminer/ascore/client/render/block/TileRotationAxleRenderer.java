package com.bedrockminer.ascore.client.render.block;

import java.util.Iterator;
import java.util.Set;

import org.lwjgl.opengl.GL11;

import com.bedrockminer.ascore.block.base.BlockPipeBase;
import com.bedrockminer.ascore.circuitry.connections.RotationConnection;
import com.bedrockminer.ascore.tileentity.TileRotationAxle;
import com.bedrockminer.ascore.util.BlockSide;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;
import net.minecraft.util.ResourceLocation;

public class TileRotationAxleRenderer extends TileEntitySpecialRenderer<TileRotationAxle> {

	public static final ResourceLocation AXLE = new ResourceLocation("ascore:textures/blocks/rotation_axle.png");

	@SuppressWarnings("incomplete-switch")
	@Override
	public void renderTileEntityAt(TileRotationAxle te, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, z);

		Set<EnumFacing> sides = te.getPipeConnections();
		Axis linear = null;
		if (sides.size() == 2) {
			Iterator<EnumFacing> it = sides.iterator();
			if (it.next().getAxis() == it.next().getAxis())
				linear = sides.iterator().next().getAxis();
		}

		for (EnumFacing side : sides) {
			GlStateManager.pushMatrix();
			GlStateManager.translate(0.5, 0.5, 0.5);
			switch (side) {
			case UP:
				GlStateManager.rotate(90, 1, 0, 0);
				break;
			case DOWN:
				GlStateManager.rotate(-90, 1, 0, 0);
				break;
			case SOUTH:
				GlStateManager.rotate(180, 0, 1, 0);
				break;
			case EAST:
				GlStateManager.rotate(-90, 0, 1, 0);
				break;
			case WEST:
				GlStateManager.rotate(90, 0, 1, 0);
			}
			GlStateManager.translate(-0.5, -0.5, -0.5);

			this.renderAxlePart(((RotationConnection) te.getConnection()).getRotation(), side.getAxisDirection() == AxisDirection.NEGATIVE, linear != null);
			GlStateManager.popMatrix();
		}

		this.renderCenter(((RotationConnection) te.getConnection()).getRotation(), linear);
		renderConnection((RotationConnection) te.getConnection(), false);

		GlStateManager.popMatrix();
	}

	public static void renderAxlePart(double rotation, boolean right, boolean linear) {
		if (right)
			rotation = -rotation;

		GlStateManager.pushMatrix();
		GlStateManager.translate(0.5, 0.5, 0);
		GlStateManager.rotate((float) rotation * 360.0f, 0, 0, 1);
		GlStateManager.translate(-0.5, -0.5, 0);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		GlStateManager.disableLighting();

		Tessellator t = Tessellator.getInstance();
		WorldRenderer wr = t.getWorldRenderer();

		double linfac = linear ? 0.5 : 0;

		Minecraft.getMinecraft().getTextureManager().bindTexture(AXLE);
		wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		// TOP
		wr.pos(0.5625, 0.5625, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		wr.pos(0.4375, 0.5625, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.4375, 0.5625, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.5625, 0.5625, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		// BOTTOM
		wr.pos(0.4375, 0.4375, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		wr.pos(0.5625, 0.4375, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.5625, 0.4375, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.4375, 0.4375, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		// LEFT
		wr.pos(0.5625, 0.4375, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		wr.pos(0.5625, 0.5625, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.5625, 0.5625, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.5625, 0.4375, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		// RIGHT
		wr.pos(0.4375, 0.5625, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		wr.pos(0.4375, 0.4375, 0.0625).tex(right ? 0.9375 : 0.0625, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.4375, 0.4375, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.1875 : 0.3125)).endVertex();
		wr.pos(0.4375, 0.5625, 0.375).tex(right ? 0.625 : 0.375, linfac + (right ? 0.3125 : 0.1875)).endVertex();
		t.draw();
		GlStateManager.popMatrix();
	}

	public void renderCenter(double rotation, Axis linear) {
		Tessellator t = Tessellator.getInstance();
		WorldRenderer wr = t.getWorldRenderer();
		if (linear == null) {
			// Block
			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			GlStateManager.disableLighting();
			Minecraft.getMinecraft().getTextureManager().bindTexture(AXLE);
			wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
			// TOP
			wr.pos(0.625, 0.625, 0.375).tex(0.375, 0.375).endVertex();
			wr.pos(0.375, 0.625, 0.375).tex(0.375, 0.125).endVertex();
			wr.pos(0.375, 0.625, 0.625).tex(0.625, 0.125).endVertex();
			wr.pos(0.625, 0.625, 0.625).tex(0.625, 0.375).endVertex();
			// BOTTOM
			wr.pos(0.375, 0.375, 0.375).tex(0.375, 0.125).endVertex();
			wr.pos(0.625, 0.375, 0.375).tex(0.375, 0.375).endVertex();
			wr.pos(0.625, 0.375, 0.625).tex(0.625, 0.375).endVertex();
			wr.pos(0.375, 0.375, 0.625).tex(0.625, 0.125).endVertex();
			// LEFT
			wr.pos(0.625, 0.375, 0.375).tex(0.375, 0.375).endVertex();
			wr.pos(0.625, 0.625, 0.375).tex(0.375, 0.125).endVertex();
			wr.pos(0.625, 0.625, 0.625).tex(0.625, 0.125).endVertex();
			wr.pos(0.625, 0.375, 0.625).tex(0.625, 0.375).endVertex();
			// RIGHT
			wr.pos(0.375, 0.625, 0.375).tex(0.375, 0.375).endVertex();
			wr.pos(0.375, 0.375, 0.375).tex(0.375, 0.125).endVertex();
			wr.pos(0.375, 0.375, 0.625).tex(0.625, 0.125).endVertex();
			wr.pos(0.375, 0.625, 0.625).tex(0.625, 0.375).endVertex();
			// FRONT
			wr.pos(0.625, 0.375, 0.375).tex(0.375, 0.375).endVertex();
			wr.pos(0.375, 0.375, 0.375).tex(0.375, 0.125).endVertex();
			wr.pos(0.375, 0.625, 0.375).tex(0.625, 0.125).endVertex();
			wr.pos(0.625, 0.625, 0.375).tex(0.625, 0.375).endVertex();
			// BACK
			wr.pos(0.375, 0.375, 0.625).tex(0.375, 0.125).endVertex();
			wr.pos(0.625, 0.375, 0.625).tex(0.375, 0.375).endVertex();
			wr.pos(0.625, 0.625, 0.625).tex(0.625, 0.375).endVertex();
			wr.pos(0.375, 0.625, 0.625).tex(0.625, 0.125).endVertex();
			t.draw();
		} else {
			// Linear connection
			GlStateManager.pushMatrix();
			GlStateManager.translate(0.5, 0.5, 0.5);
			if (linear == Axis.X)
				GlStateManager.rotate(90, 0, 1, 0);
			if (linear == Axis.Y)
				GlStateManager.rotate(-90, 1, 0, 0);
			GlStateManager.rotate((float) -rotation * 360.0f, 0, 0, 1);
			GlStateManager.translate(-0.5, -0.5, -0.5);
			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			GlStateManager.disableLighting();

			Minecraft.getMinecraft().getTextureManager().bindTexture(AXLE);
			wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
			// TOP
			wr.pos(0.5625, 0.5625, 0.375).tex(0.375, 0.6875).endVertex();
			wr.pos(0.4375, 0.5625, 0.375).tex(0.375, 0.8125).endVertex();
			wr.pos(0.4375, 0.5625, 0.625).tex(0.625, 0.8125).endVertex();
			wr.pos(0.5625, 0.5625, 0.625).tex(0.625, 0.6875).endVertex();
			// BOTTOM
			wr.pos(0.4375, 0.4375, 0.375).tex(0.375, 0.6875).endVertex();
			wr.pos(0.5625, 0.4375, 0.375).tex(0.375, 0.8125).endVertex();
			wr.pos(0.5625, 0.4375, 0.625).tex(0.625, 0.8125).endVertex();
			wr.pos(0.4375, 0.4375, 0.625).tex(0.625, 0.6875).endVertex();
			// LEFT
			wr.pos(0.5625, 0.4375, 0.375).tex(0.375, 0.6875).endVertex();
			wr.pos(0.5625, 0.5625, 0.375).tex(0.375, 0.8125).endVertex();
			wr.pos(0.5625, 0.5625, 0.625).tex(0.625, 0.8125).endVertex();
			wr.pos(0.5625, 0.4375, 0.625).tex(0.625, 0.6875).endVertex();
			// RIGHT
			wr.pos(0.4375, 0.5625, 0.375).tex(0.375, 0.6875).endVertex();
			wr.pos(0.4375, 0.4375, 0.375).tex(0.375, 0.8125).endVertex();
			wr.pos(0.4375, 0.4375, 0.625).tex(0.625, 0.8125).endVertex();
			wr.pos(0.4375, 0.5625, 0.625).tex(0.625, 0.6875).endVertex();
			t.draw();
			GlStateManager.popMatrix();
		}
	}

	@SuppressWarnings("incomplete-switch")
	public static void renderConnection(RotationConnection c, boolean all) {
		Set<BlockSide> sides= all ? c.getSelectedPositions() : c.getConnectedPositions();
		for (BlockSide side : sides) {
			GlStateManager.pushMatrix();
			GlStateManager.translate(side.getPos().getX() - c.getTE().getPos().getX() + 0.5, side.getPos().getY() - c.getTE().getPos().getY() + 0.5, side.getPos().getZ() - c.getTE().getPos().getZ() + 0.5);
			switch (side.getSide()) {
			case UP:
				GlStateManager.rotate(90, 1, 0, 0);
				break;
			case DOWN:
				GlStateManager.rotate(-90, 1, 0, 0);
				break;
			case SOUTH:
				GlStateManager.rotate(180, 0, 1, 0);
				break;
			case EAST:
				GlStateManager.rotate(-90, 0, 1, 0);
				break;
			case WEST:
				GlStateManager.rotate(90, 0, 1, 0);
			}
			float angle = (float) c.getRotation() * 360.0f;
			if (side.getSide().getAxisDirection() == AxisDirection.NEGATIVE)
				angle = -angle;
			GlStateManager.rotate(angle, 0, 0, 1);
			GlStateManager.translate(-0.5, -0.5, -0.5);
			Minecraft.getMinecraft().getTextureManager().bindTexture(AXLE);
			Tessellator t = Tessellator.getInstance();
			WorldRenderer wr = t.getWorldRenderer();

			int r = (BlockPipeBase.dye_colors[c.getConnectionColor().getDyeDamage()] >> 16) & 255;
			int g = (BlockPipeBase.dye_colors[c.getConnectionColor().getDyeDamage()] >> 8) & 255;
			int b = BlockPipeBase.dye_colors[c.getConnectionColor().getDyeDamage()] & 255;

			wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);

			double dm = 0.421875;
			double dp = 0.578125;
			double tm = dm - 0.25;
			double tp = dp - 0.25;
			double zm = 0;
			double zp = 0.0625;

			// OUTLINE
			// TOP
			wr.pos(dp, dp, zm).tex(zm, tm).color(r, g, b, 150).endVertex();
			wr.pos(dm, dp, zm).tex(zm, tp).color(r, g, b, 150).endVertex();
			wr.pos(dm, dp, zp).tex(zp, tp).color(r, g, b, 150).endVertex();
			wr.pos(dp, dp, zp).tex(zp, tm).color(r, g, b, 150).endVertex();
			// BOTTOM
			wr.pos(dm, dm, zm).tex(zm, tp).color(r, g, b, 150).endVertex();
			wr.pos(dp, dm, zm).tex(zm, tm).color(r, g, b, 150).endVertex();
			wr.pos(dp, dm, zp).tex(zp, tm).color(r, g, b, 150).endVertex();
			wr.pos(dm, dm, zp).tex(zp, tp).color(r, g, b, 150).endVertex();
			// LEFT
			wr.pos(dp, dm, zm).tex(zm, tp).color(r, g, b, 150).endVertex();
			wr.pos(dp, dp, zm).tex(zm, tm).color(r, g, b, 150).endVertex();
			wr.pos(dp, dp, zp).tex(zp, tm).color(r, g, b, 150).endVertex();
			wr.pos(dp, dm, zp).tex(zp, tp).color(r, g, b, 150).endVertex();
			// RIGHT
			wr.pos(dm, dp, zm).tex(zm, tp).color(r, g, b, 150).endVertex();
			wr.pos(dm, dm, zm).tex(zm, tm).color(r, g, b, 150).endVertex();
			wr.pos(dm, dm, zp).tex(zp, tm).color(r, g, b, 150).endVertex();
			wr.pos(dm, dp, zp).tex(zp, tp).color(r, g, b, 150).endVertex();
			// FRONT Colored
			wr.pos(dp, dm, zm + 0.001).tex(dp, dm).color(r, g, b, 150).endVertex();
			wr.pos(dm, dm, zm + 0.001).tex(dm, dm).color(r, g, b, 150).endVertex();
			wr.pos(dm, dp, zm + 0.001).tex(dm, dp).color(r, g, b, 150).endVertex();
			wr.pos(dp, dp, zm + 0.001).tex(dp, dp).color(r, g, b, 150).endVertex();
			// BACK Colored
			wr.pos(dm, dm, zp - 0.001).tex(dm, dm).color(r, g, b, 150).endVertex();
			wr.pos(dp, dm, zp - 0.001).tex(dp, dm).color(r, g, b, 150).endVertex();
			wr.pos(dp, dp, zp - 0.001).tex(dp, dp).color(r, g, b, 150).endVertex();
			wr.pos(dm, dp, zp - 0.001).tex(dm, dp).color(r, g, b, 150).endVertex();
			// FRONT Center
			wr.pos(0.5625, 0.4375, zm).tex(0.5625, 0.4375).color(255, 255, 255, 255).endVertex();
			wr.pos(0.4375, 0.4375, zm).tex(0.4375, 0.4375).color(255, 255, 255, 255).endVertex();
			wr.pos(0.4375, 0.5625, zm).tex(0.4375, 0.5625).color(255, 255, 255, 255).endVertex();
			wr.pos(0.5625, 0.5625, zm).tex(0.5625, 0.5625).color(255, 255, 255, 255).endVertex();
			// BACK Center
			wr.pos(0.4375, 0.4375, zp).tex(0.4375, 0.4375).color(255, 255, 255, 255).endVertex();
			wr.pos(0.5625, 0.4375, zp).tex(0.5625, 0.4375).color(255, 255, 255, 255).endVertex();
			wr.pos(0.5625, 0.5625, zp).tex(0.5625, 0.5625).color(255, 255, 255, 255).endVertex();
			wr.pos(0.4375, 0.5625, zp).tex(0.4375, 0.5625).color(255, 255, 255, 255).endVertex();

			t.draw();
			GlStateManager.popMatrix();
		}
	}
}
