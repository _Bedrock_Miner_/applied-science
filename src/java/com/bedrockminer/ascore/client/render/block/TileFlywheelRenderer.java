package com.bedrockminer.ascore.client.render.block;

import com.bedrockminer.ascore.client.render.RenderUtil;
import com.bedrockminer.ascore.tileentity.TileFlywheel;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;

public class TileFlywheelRenderer extends TileEntitySpecialRenderer<TileFlywheel> {

	public static final ResourceLocation OUTER = new ResourceLocation("ascore:textures/blocks/flywheel_outer.png");
	public static final ResourceLocation INNER = new ResourceLocation("ascore:textures/blocks/flywheel_inner.png");

	@SuppressWarnings("incomplete-switch")
	@Override
	public void renderTileEntityAt(TileFlywheel te, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, z);
		GlStateManager.enableLighting();

		GlStateManager.pushMatrix();
		GlStateManager.translate(0.5, 0.5, 0.5);
		switch(te.getOrientation()) {
		case X:
			GlStateManager.rotate(90, 0, 1, 0);
			break;
		case Y:
			GlStateManager.rotate(-90, 1, 0, 0);
		}
		GlStateManager.rotate((float) -te.getConnection().getRotation() * 360.0f, 0, 0, 1);
		GlStateManager.pushMatrix();
		GlStateManager.scale(0.9, 0.9, 0.9);
		GlStateManager.translate(-0.5, -0.5, 0);
		renderFlywheel(0.375, 0.25);
		GlStateManager.translate(0.5, 0.5, 0);
		GlStateManager.popMatrix();
		GlStateManager.rotate((float) te.getConnection().getRotation() * 360.0f, 0, 0, 1);
		GlStateManager.translate(-0.5, -0.5, -0.5);
		TileRotationAxleRenderer.renderAxlePart((float) te.getConnection().getRotation(), true, true);
		GlStateManager.translate(0.5, 0.5, 0.5);
		GlStateManager.rotate(180, 0, 1, 0);
		GlStateManager.translate(-0.5, -0.5, -0.5);
		TileRotationAxleRenderer.renderAxlePart((float) te.getConnection().getRotation(), false, true);
		GlStateManager.popMatrix();
		TileRotationAxleRenderer.renderConnection(te.getConnection(), true);
		GlStateManager.popMatrix();
	}

	public static void renderFlywheel(double outerWidth, double innerWidth) {
		Minecraft.getMinecraft().getTextureManager().bindTexture(OUTER);
		RenderUtil.renderSolidifiedFace(0, 0, 1, 1, outerWidth);
		// Offsetting to avoid flickering at overlaying faces
		GlStateManager.translate(0.001, 0.001, 0.001);
		Minecraft.getMinecraft().getTextureManager().bindTexture(INNER);
		RenderUtil.renderSolidifiedFace(0, 0, 1, 1, innerWidth);
	}
}
