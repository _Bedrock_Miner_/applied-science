package com.bedrockminer.ascore.client.render.block;

import com.bedrockminer.ascore.tileentity.TileFlywheel;
import com.bedrockminer.ascore.tileentity.TilePressureGauge;
import com.bedrockminer.ascore.tileentity.TileRotationAxle;
import com.bedrockminer.ascore.tileentity.TileSteamBoiler;

import net.minecraftforge.fml.client.registry.ClientRegistry;

public final class SpecialRenderer {

	public static void init() {
		ClientRegistry.bindTileEntitySpecialRenderer(TileSteamBoiler.class, new TileSteamBoilerRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TilePressureGauge.class, new TilePressureGaugeRenderer());

		ClientRegistry.bindTileEntitySpecialRenderer(TileRotationAxle.class, new TileRotationAxleRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TileFlywheel.class, new TileFlywheelRenderer());
	}

}
