package com.bedrockminer.ascore.client.render.block.models;

import static net.minecraftforge.client.model.TRSRTransformation.quatFromYXZDegrees;

import javax.vecmath.Vector3f;

import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.resources.model.ModelRotation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ITransformation;
import net.minecraftforge.client.model.TRSRTransformation;

@SuppressWarnings("deprecation")
public class ModelFlywheel extends ModelBase {

	private ModelSolidifiedTexture outer;
	private ModelSolidifiedTexture inner;

	public ModelFlywheel() {
	}

	@Override
	public TextureAtlasSprite getParticleResource() {
		return this.getSprite(new ResourceLocation("ascore:blocks/flywheel_outer"));
	}

	@Override
	public VertexFormat getVertexFormat() {
		return super.getVertexFormat();
	}

	@Override
	public void putQuads() {
		if (this.outer == null || this.inner == null) {
			Vector3f translation = new Vector3f(0, 0, 0.5f);
			TRSRTransformation transform = new TRSRTransformation(translation, null, null, null);
			this.outer = new ModelSolidifiedTexture(this.getSprite(new ResourceLocation("ascore:blocks/flywheel_outer")), 1.0, 1.0, 0.25, 0xffffff, transform);
			translation.add(new Vector3f(0.001f, 0.001f, 0.001f));
			transform = new TRSRTransformation(translation, null, null, null);
			this.inner = new ModelSolidifiedTexture(this.getSprite(new ResourceLocation("ascore:blocks/flywheel_inner")), 1.0, 1.0, 0.25, 0xffffff, transform);
		}

		this.addSubmodel(this.outer);
		this.addSubmodel(this.inner);
	}

	@Override
	public ITransformation getTransformation(TransformType renderType) {
		switch (renderType) {
		case GUI:
			return new TRSRTransformation(null, quatFromYXZDegrees(new Vector3f(0, -67.5f, 0)), new Vector3f(1.2f, 1.2f, 1.2f), null);
		case FIRST_PERSON:
			return new TRSRTransformation(null, quatFromYXZDegrees(new Vector3f(0, -120, 0)), null, null);
		case THIRD_PERSON:
			return new TRSRTransformation(new Vector3f(-.025f, 0.09375f, -0.135f), quatFromYXZDegrees(new Vector3f(10, -60, 170)), new Vector3f(0.5f, 0.5f, 0.5f), null);
		case FIXED:
			return new TRSRTransformation(null, null, new Vector3f(1.6f, 1.6f, 1.6f), null);
		default:
			return ModelRotation.X0_Y0;
		}
	}
}
