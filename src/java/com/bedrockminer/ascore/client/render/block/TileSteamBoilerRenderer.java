package com.bedrockminer.ascore.client.render.block;

import org.lwjgl.opengl.GL11;

import com.bedrockminer.ascore.block.BlockSteamBoiler;
import com.bedrockminer.ascore.tileentity.TileSteamBoiler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.fluids.FluidRegistry;

public class TileSteamBoilerRenderer extends TileEntitySpecialRenderer<TileSteamBoiler> {

	@SuppressWarnings("incomplete-switch")
	@Override
	public void renderTileEntityAt(TileSteamBoiler te, double x, double y, double z, float partialTicks, int destroyStage) {
		EnumFacing direction = te.getWorld().getBlockState(te.getPos()).getValue(BlockSteamBoiler.FACING);

		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 0.5, y + 0.5, z + 0.5);
		switch (direction) {
		case NORTH:
			GlStateManager.rotate(180, 0, 1, 0);
			break;
		case EAST:
			GlStateManager.rotate(90, 0, 1, 0);
			break;
		case WEST:
			GlStateManager.rotate(-90, 0, 1, 0);
			break;
		}
		GlStateManager.translate(-0.5, -0.5, -0.5);

		double height = te.getWater() / 1.25 * 0.875;
		if (height > 0.875)
			height = 0.875;
		double width = Math.sqrt(0.4375 * 0.4375 - (height - 0.4375) * (height - 0.4375));

		Tessellator t = Tessellator.getInstance();
		WorldRenderer w = t.getWorldRenderer();

		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);

		Minecraft.getMinecraft().getTextureManager().bindTexture(Minecraft.getMinecraft().getTextureMapBlocks().locationBlocksTexture);
		TextureAtlasSprite sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(FluidRegistry.WATER.getStill().toString());

		w.begin(7, DefaultVertexFormats.POSITION_TEX);

		// Front texture
		for (double d = 0.0625; d < 0.0625 + height; d += Math.min(0.0625, 0.0625 + height - d)) {
			double he = Math.min(0.0625, 0.0625 + height - d);
			double wi = Math.sqrt(0.4375 * 0.4375 - (d - 0.4375) * (d - 0.4375));

			w.pos(0.5 - wi, d, 0.09375).tex(sprite.getInterpolatedU(8 - 16 * wi), sprite.getInterpolatedV(16 - 16 * d)).endVertex();
			w.pos(0.5 - wi, d + he, 0.09375).tex(sprite.getInterpolatedU(8 - 16 * wi), sprite.getInterpolatedV(16 - 16 * (he + d))).endVertex();
			w.pos(0.5 + wi, d + he, 0.09375).tex(sprite.getInterpolatedU(8 + 16 * wi), sprite.getInterpolatedV(16 - 16 * (he + d))).endVertex();
			w.pos(0.5 + wi, d, 0.09375).tex(sprite.getInterpolatedU(8 + 16 * wi), sprite.getInterpolatedV(16 - 16 * d)).endVertex();
		}

		// Top texture
		w.pos(0.5 - width, 0.0625 + height, 0.09375).tex(sprite.getInterpolatedU(8 - 16 * width), sprite.getInterpolatedV(14.5)).endVertex();
		w.pos(0.5 - width, 0.0625 + height, 1).tex(sprite.getInterpolatedU(8 - 16 * width), sprite.getInterpolatedV(0)).endVertex();
		w.pos(0.5 + width, 0.0625 + height, 1).tex(sprite.getInterpolatedU(8 + 16 * width), sprite.getInterpolatedV(0)).endVertex();
		w.pos(0.5 + width, 0.0625 + height, 0.09375).tex(sprite.getInterpolatedU(8 + 16 * width), sprite.getInterpolatedV(14.5)).endVertex();

		w.pos(0.5 - width, 0.0625 + height, 1).tex(sprite.getInterpolatedU(8 - 16 * width), sprite.getInterpolatedV(16)).endVertex();
		w.pos(0.5 - width, 0.0625 + height, 1.9375).tex(sprite.getInterpolatedU(8 - 16 * width), sprite.getInterpolatedV(1)).endVertex();
		w.pos(0.5 + width, 0.0625 + height, 1.9375).tex(sprite.getInterpolatedU(8 + 16 * width), sprite.getInterpolatedV(1)).endVertex();
		w.pos(0.5 + width, 0.0625 + height, 1).tex(sprite.getInterpolatedU(8 + 16 * width), sprite.getInterpolatedV(16)).endVertex();

		t.draw();

		GlStateManager.popMatrix();
	}

}
