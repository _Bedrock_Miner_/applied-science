package com.bedrockminer.ascore.client.render.block.models;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.apache.commons.lang3.tuple.Pair;

import com.bedrockminer.ascore.block.base.BlockTileEntityProvider;
import com.bedrockminer.ascore.util.Log;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.BlockPart;
import net.minecraft.client.renderer.block.model.BlockPartFace;
import net.minecraft.client.renderer.block.model.FaceBakery;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.block.model.ModelBlock;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.resources.model.IBakedModel;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IFlexibleBakedModel;
import net.minecraftforge.client.model.IPerspectiveAwareModel;
import net.minecraftforge.client.model.ISmartBlockModel;
import net.minecraftforge.client.model.ISmartItemModel;
import net.minecraftforge.client.model.ITransformation;
import net.minecraftforge.client.model.pipeline.LightUtil;
import net.minecraftforge.client.model.pipeline.UnpackedBakedQuad;

/**
 * A base class for code driven model implementations.
 * This class provides methods for easy rendering and inclusion of "normal"
 * file-driven submodels.
 * @author _Bedrock_Miner_
 */
@SuppressWarnings("deprecation")
public abstract class ModelBase implements ISmartBlockModel, ISmartItemModel, IPerspectiveAwareModel {

	private static FaceBakery face = new FaceBakery();
	public static List<ModelBase> models = new LinkedList<ModelBase>();

	private BlockPos renderPos;
	private IBlockState state;
	private ItemStack stack;
	private boolean item;

	protected List<BakedQuad> list;
	private UnpackedBakedQuad.Builder builder;

	private TextureAtlasSprite texture;
	private EnumFacing orientation;

	private Vector3f pos;
	private Vector3f normal;
	private Vector2f uv;
	private Vector4f color;

	/**
	 * Instantiates a new ModelBase.
	 */
	public ModelBase() {
		this.loadSubmodels();
		models.add(this);
	}

	// Implementing the Interfaces

	@Override
	public List<BakedQuad> getFaceQuads(EnumFacing facing) {
		this.list = new LinkedList<BakedQuad>();
		this.putFaceQuads(facing);
		return this.list;
	}

	@Override
	public List<BakedQuad> getGeneralQuads() {
		this.list = new LinkedList<BakedQuad>();
		this.putQuads();
		return this.list;
	}

	@Override
	public boolean isAmbientOcclusion() {
		return true;
	}

	@Override
	public boolean isGui3d() {
		return true;
	}

	@Override
	public boolean isBuiltInRenderer() {
		return false;
	}

	@Override
	public TextureAtlasSprite getParticleTexture() {
		return this.getParticleResource();
	}

	@Override
	public ItemCameraTransforms getItemCameraTransforms() {
		return ItemCameraTransforms.DEFAULT;
	}

	@Override
	public IBakedModel handleItemState(ItemStack stack) {
		this.stack = stack;
		this.state = null;
		this.renderPos = null;
		this.item = true;
		return this;
	}

	@Override
	public IBakedModel handleBlockState(IBlockState state) {
		if (state.getBlock() instanceof BlockTileEntityProvider)
			this.renderPos = ((BlockTileEntityProvider)state.getBlock()).renderPosition;
		else
			this.renderPos = null;
		this.state = state;
		this.stack = null;
		this.item = false;
		return this;
	}

	@Override
	public VertexFormat getFormat() {
		return this.getVertexFormat();
	}

	@Override
	public Pair<? extends IFlexibleBakedModel, Matrix4f> handlePerspective(TransformType cameraTransformType) {
		return Pair.of(this, this.getTransformation(cameraTransformType).getMatrix());
	}

	// Overridable Methods

	/**
	 * Returns the TextureAtlasSprite that will be used for the particles of the
	 * block. To get a sprite, you can use {@link #getSprite(ResourceLocation)}.
	 *
	 * @return the sprite to use for the particles.
	 */
	public abstract TextureAtlasSprite getParticleResource();

	/**
	 * Returns the vertex format to use while rendering. This is either
	 * {@link DefaultVertexFormats#BLOCK} or
	 * {@link DefaultVertexFormats#ITEM}.
	 *
	 * @return the vertex format to use.
	 */
	public VertexFormat getVertexFormat() {
		if (this.isItem())
			return DefaultVertexFormats.ITEM;
		else
			return DefaultVertexFormats.BLOCK;
	}

	/**
	 * This is the main "rendering" method. Here, the faces are assembled using
	 * and pushed to the internal face stack.
	 */
	public abstract void putQuads();

	/**
	 * Returns the transformation for the given render type.
	 *
	 * @param renderType the render type
	 * @return the transformation
	 */
	public abstract ITransformation getTransformation(TransformType renderType);

	/**
	 * In this method you can render faces that are adjacent to a special side
	 * of the block. If that side is blocked, the face won't be rendered.
	 *
	 * @param facing the face
	 */
	public void putFaceQuads(EnumFacing facing) {
	}

	/**
	 * If you need file-driven submodels in your model, you can load them in
	 * this method using {@link #loadSubmodel(ResourceLocation)}.
	 */
	public void loadSubmodels() {
	}

	// Getter Methods

	/**
	 * Returns whether the rendering is called for an item or a block.
	 *
	 * @return true if an item is rendered.
	 */
	public boolean isItem() {
		return this.item;
	}

	/**
	 * Returns the item stack being rendered or null if a block is drawn.
	 *
	 * @return the item stack
	 */
	public ItemStack getItemStack() {
		return this.stack;
	}

	/**
	 * Returns the blockstate of the block being rendered or null if an item is
	 * drawn.
	 *
	 * @return the blockstate
	 */
	public IBlockState getBlockState() {
		return this.state;
	}

	/**
	 * Returns the position of the block currently being rendered. If the block
	 * is not an instance of BlockTileEntityProvider or if an item is being
	 * rendered, this method returns null.
	 *
	 * @return the block position
	 */
	public BlockPos getRenderPos() {
		return this.renderPos;
	}

	// Utility Methods

	/**
	 * Returns a sprite based on the given resource location. Remember to
	 * register "new" textures from the TextureStitchEvent.Pre first.
	 *
	 * @param resource the resource location
	 * @return the sprite
	 */
	protected TextureAtlasSprite getSprite(ResourceLocation resource) {
		return Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(resource.toString());
	}

	/**
	 * Loads a submodel from the given file.
	 *
	 * @param resource the file resource
	 * @return the submodel
	 */
	protected ModelBlock loadSubmodel(ResourceLocation resource) {
		Reader r = null;
		try {
			r = new InputStreamReader(Minecraft.getMinecraft().getResourceManager().getResource(resource).getInputStream());
			ModelBlock model = ModelBlock.deserialize(r);
			model.name = resource.toString();
			return model;
		} catch (IOException e) {
			Log.fatal("IO Exception while loading model %s: %s", resource, e.getMessage());
			Log.printStackTrace(e);
		} finally {
			try {
				r.close();
			} catch (Exception e) {
				Log.fatal("Exception while closing stream", e.getMessage());
				Log.printStackTrace(e);
			}
		}
		return null;
	}

	/**
	 * Includes the given submodel into the currently drawn block model.
	 *
	 * @param model the submodel
	 * @param transf the transformations applied to the model
	 */
	protected void addSubmodel(ModelBlock model, ITransformation transf) {
		if (model == null)
			return;

		for (BlockPart part: model.getElements()) {
			for (EnumFacing side : part.mapFaces.keySet()) {
				BlockPartFace face = part.mapFaces.get(side);
                TextureAtlasSprite tex = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(new ResourceLocation(model.resolveTextureName(face.texture)).toString());
                BakedQuad quad = this.face.makeBakedQuad(part.positionFrom, part.positionTo, face, tex, side, transf, part.partRotation, false, part.shade);
                this.list.add(quad);
            }
		}
	}

	/**
	 * Includes the given baked submodel into the currently drawn block model.
	 *
	 * @param model the submodel
	 */
	protected void addSubmodel(IBakedModel model) {
		this.addQuads(model.getGeneralQuads());
	}

	/**
	 * Adds the given list of baked quads to the currently drawn model.
	 *
	 * @param quads the quads to add.
	 */
	protected void addQuads(List<BakedQuad> quads) {
		this.list.addAll(quads);
	}

	/**
	 * Starts a new quadrilateral face.
	 *
	 * @param texture the texture sprite
	 * @param orientation the (general) orientation of the face
	 */
	protected void newQuad(TextureAtlasSprite texture, EnumFacing orientation) {
		this.builder = new UnpackedBakedQuad.Builder(this.getVertexFormat());
		this.builder.setQuadOrientation(orientation);

		this.texture = texture;
		this.orientation = orientation;
	}

	/**
	 * Finished a face and pushes it to the list.
	 */
	protected void emitQuad() {
		this.list.add(this.builder.build());
		this.normal = null;
	}

	/**
	 * Finished the old face and starts a new one.
	 *
	 * @param texture the texture sprite
	 * @param orientation the (general) orientation of the face
	 */
	protected void nextQuad(TextureAtlasSprite texture, EnumFacing orientation) {
		this.emitQuad();
		this.newQuad(texture, orientation);
	}

	/**
	 * Marks this face as colored.
	 */
	protected void setColored() {
		this.builder.setQuadColored();
	}

	/**
	 * Sets the face tint ID for this face. Default is -1 (no tint).
	 *
	 * @param tintid the tintid
	 */
	protected void setTintID(int tintid) {
		this.builder.setQuadTint(tintid);
	}

	/**
	 * Sets the position for the current vertex.
	 *
	 * @param x
	 * @param y
	 * @param z
	 * @return this object
	 */
	protected ModelBase pos(float x, float y, float z) {
		this.pos = new Vector3f(x, y, z);
		return this;
	}

	/**
	 * Sets the normal for this vertex. This value can stay the same for
	 * multiple vertices without calling the method again.
	 *
	 * @param x
	 * @param y
	 * @param z
	 * @return this object
	 */
	protected ModelBase normal(float x, float y, float z) {
		this.normal = new Vector3f(x, y, z);
		return this;
	}

	/**
	 * Sets the texture uv coordinates on the given sprite for the current
	 * vertex.
	 *
	 * @param u
	 * @param v
	 * @return this object
	 */
	protected ModelBase uv(float u, float v) {
		this.uv = new Vector2f(u, v);
		return this;
	}

	/**
	 * Sets the color for the current vertex.
	 *
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 * @return this object
	 */
	protected ModelBase color(float r, float g, float b, float a) {
		this.color = new Vector4f(r, g, b, a);
		return this;
	}

	/**
	 * Sets the color for the current vertex.
	 *
	 * @param rgb
	 * @return this object
	 */
	protected ModelBase color(int rgb) {
		float r = (rgb >> 16 & 255) / 255.0f;
		float g = (rgb >> 8 & 255) / 255.0f;
		float b = (rgb & 255) / 255.0f;
		return this.color(r, g, b, 1.0f);
	}

	/**
	 * Ends this vertex and proceeds to the next one.
	 */
	protected void endVertex() {
		this.putVertexData();
		this.color = null;
	}

	private void putVertexData() {
		for (int e = 0; e < this.getVertexFormat().getElementCount(); e++) {
			switch (this.getVertexFormat().getElement(e).getUsage()) {

			case POSITION:
				this.builder.put(e, this.pos.x, this.pos.y, this.pos.z, 1);
				break;

			case COLOR:
				float d = this.normal == null ? LightUtil.diffuseLight(this.orientation) : LightUtil.diffuseLight(this.normal.x, this.normal.y, this.normal.z);
				if (this.isItem())
					d = 1.0f;
				if (this.color != null) {
					this.builder.put(e, d * this.color.x, d * this.color.y, d * this.color.z, this.color.w);
				} else {
					this.builder.put(e, d, d, d, 1);
				}
				break;

			case UV:
				int index = this.getVertexFormat().getElement(e).getIndex();
				if (index == 0) {
					this.builder.put(e, this.texture.getInterpolatedU(this.uv.x * 16), this.texture.getInterpolatedV((1 - this.uv.y) * 16), 0, 1);
				} else {
					this.builder.put(e, 0, 0, 0, 1);
				}
				break;

			case NORMAL:
				if (this.normal != null) {
					this.builder.put(e, this.normal.x, this.normal.y, this.normal.z, 0);
				} else {
					this.builder.put(e, this.orientation.getDirectionVec().getX(), this.orientation.getDirectionVec().getY(), this.orientation.getDirectionVec().getZ());
				}
				break;

			default:
				this.builder.put(e);
			}
		}
	}
}

