package com.bedrockminer.ascore.client.render.block.models;

import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.util.EnumFacing;

public abstract class ModelCached extends ModelBase {

	private Map<EnumFacing, List<BakedQuad>> faceQuads = new EnumMap<EnumFacing, List<BakedQuad>>(EnumFacing.class);
	private List<BakedQuad> generalQuads;

	@Override
	public List<BakedQuad> getFaceQuads(EnumFacing facing) {
		if (this.faceQuads.get(facing) == null) {
			this.list = new LinkedList<BakedQuad>();
			this.putFaceQuads(facing);
			this.faceQuads.put(facing, this.list);
		}
		return this.faceQuads.get(facing);
	}

	@Override
	public List<BakedQuad> getGeneralQuads() {
		if (this.generalQuads == null) {
			this.list = new LinkedList<BakedQuad>();
			this.putQuads();
			this.generalQuads = this.list;
		}
		return this.generalQuads;
	}

	public void clearCache() {
		this.generalQuads = null;
		this.faceQuads.clear();
	}
}
