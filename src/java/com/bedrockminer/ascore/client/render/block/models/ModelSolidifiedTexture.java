package com.bedrockminer.ascore.client.render.block.models;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;

import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.model.ModelRotation;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.client.model.ITransformation;

/**
 * This model can be used as a submodel to include solidified faces in a
 * larger model part. It uses the POSITION_TEX_COLOR_NORMAL vertex format.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
@SuppressWarnings("deprecation")
public class ModelSolidifiedTexture extends ModelCached {

	private TextureAtlasSprite sprite;
	private float width;
	private float height;
	private float thickness;
	private int color = 0xffffff;
	private Matrix4f transformation;

	public ModelSolidifiedTexture(TextureAtlasSprite sprite, double width, double height, double thickness, int color, ITransformation transformation) {
		this.sprite = sprite;
		this.width = (float) width;
		this.height = (float) height;
		this.thickness = (float) (thickness / 2.0);
		this.color = color;
		this.transformation = transformation.getMatrix();
	}

	public void setColor(int color) {
		this.color = color;
	}

	@Override
	public TextureAtlasSprite getParticleResource() {
		return this.sprite;
	}

	@Override
	public void putQuads() {
		// Front face
		this.newQuad(this.sprite, EnumFacing.NORTH);
		Vector4f p = this.transform(this.width, 0, -this.thickness);
		this.pos(p.x, p.y, p.z).uv(0, 0).color(this.color).endVertex();
		p = this.transform(0, 0, -this.thickness);
		this.pos(p.x, p.y, p.z).uv(1, 0).color(this.color).endVertex();
		p = this.transform(0, this.height, -this.thickness);
		this.pos(p.x, p.y, p.z).uv(1, 1).color(this.color).endVertex();
		p = this.transform(this.width, this.height, -this.thickness);
		this.pos(p.x, p.y, p.z).uv(0, 1).color(this.color).endVertex();

		// Back face
		this.nextQuad(this.sprite, EnumFacing.SOUTH);
		p = this.transform(0, this.height, this.thickness);
		this.pos(p.x, p.y, p.z).uv(1, 1).color(this.color).endVertex();
		p = this.transform(0, 0, this.thickness);
		this.pos(p.x, p.y, p.z).uv(1, 0).color(this.color).endVertex();
		p = this.transform(this.width, 0, this.thickness);
		this.pos(p.x, p.y, p.z).uv(0, 0).color(this.color).endVertex();
		p = this.transform(this.width, this.height, this.thickness);
		this.pos(p.x, p.y, p.z).uv(0, 1).color(this.color).endVertex();

		// Side faces
		for (int tu = 0; tu < this.sprite.getIconWidth(); tu++) {
			for (int tv = 0; tv < this.sprite.getIconWidth(); tv++) {
				Vector4f min = new Vector4f((float) tu / this.sprite.getIconWidth() * this.width, (float) tv / this.sprite.getIconHeight() * this.height, -this.thickness, 1);
				Vector4f max = new Vector4f((float) (tu + 1) / this.sprite.getIconWidth() * this.width, (float) (tv + 1) / this.sprite.getIconHeight() * this.height, this.thickness, 1);
				Vector2f uvmin = new Vector2f(1 - (float) tu / this.sprite.getIconWidth() - 0.001f, (float) tv / this.sprite.getIconHeight() + 0.001f);
				Vector2f uvmax = new Vector2f(1 - (float) (tu + 1) / this.sprite.getIconWidth() + 0.001f, (float) (tv + 1) / this.sprite.getIconHeight() - 0.001f);

				// Top face
				this.nextQuad(this.sprite, EnumFacing.UP);
				p = this.transform(max.x, max.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(min.x, max.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(min.x, max.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmax.y).color(this.color).endVertex();
				p = this.transform(max.x, max.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmax.y).color(this.color).endVertex();
				// Bottom face
				this.nextQuad(this.sprite, EnumFacing.DOWN);
				p = this.transform(min.x, min.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(max.x, min.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(max.x, min.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(min.x, min.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmax.y).color(this.color).endVertex();
				// West face
				this.nextQuad(this.sprite, EnumFacing.WEST);
				p = this.transform(min.x, max.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(min.x, min.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(min.x, min.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmax.y).color(this.color).endVertex();
				p = this.transform(min.x, max.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmax.y).color(this.color).endVertex();
				// East face
				this.nextQuad(this.sprite, EnumFacing.EAST);
				p = this.transform(max.x, min.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(max.x, max.y, min.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(max.x, max.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmax.x, uvmin.y).color(this.color).endVertex();
				p = this.transform(max.x, min.y, max.z);
				this.pos(p.x, p.y, p.z).uv(uvmin.x, uvmax.y).color(this.color).endVertex();
			}
		}

		this.emitQuad();
	}

	private Vector4f transform(float x, float y, float z) {
		Vector4f vec = new Vector4f(x, y, z, 1);
		this.transformation.transform(vec);
		return vec;
	}

	@Override
	public ITransformation getTransformation(TransformType renderType) {
		return ModelRotation.X0_Y0;
	}
}
