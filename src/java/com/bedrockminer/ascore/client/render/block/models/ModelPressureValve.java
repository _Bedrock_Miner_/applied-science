package com.bedrockminer.ascore.client.render.block.models;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.tileentity.TilePressureValve;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelBlock;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.resources.model.ModelRotation;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.TRSRTransformation;

public class ModelPressureValve extends ModelPipeBase {

	private ModelBlock valve_handle_open;
	private ModelBlock valve_handle_closed;

	@Override
	public void loadSubmodels() {
		super.loadSubmodels();
		this.valve_handle_open = this.loadSubmodel(new ResourceLocation("ascore:models/block/pressure_valve_handle_open.json"));
		this.valve_handle_closed = this.loadSubmodel(new ResourceLocation("ascore:models/block/pressure_valve_handle_closed.json"));
	}

	@Override
	public TextureAtlasSprite getParticleResource() {
		return this.getSprite(new ResourceLocation("ascore:blocks/pressure_valve"));
	}

	@Override
	public VertexFormat getVertexFormat() {
		return DefaultVertexFormats.POSITION_TEX_COLOR;
	}

	@Override
	public boolean pipePart(EnumFacing side) {
		if (this.getRenderPos() != null) {
			TilePressureValve te = (TilePressureValve) ASBlocks.pressureValve.getTE(Minecraft.getMinecraft().theWorld, this.getRenderPos());
			if (te != null)
				return side.getAxis() == te.getOrientation();
		}
		return side.getAxis() == Axis.Z;
	}

	@Override
	public void putQuads() {
		TilePressureValve te = null;
		if (this.getRenderPos() != null)
			te = (TilePressureValve) ASBlocks.pressureValve.getTE(Minecraft.getMinecraft().theWorld, this.getRenderPos());
		String tex = "ascore:blocks/pressure_valve" + (!this.isItem() && te != null && te.isClosed() ? "_closed" : "");
		this.pipe_part.textures.put("pipe", tex);
		this.pipe_cap.textures.put("pipe", tex);

		super.putQuads();

		if (this.isItem())
			this.addSubmodel(this.valve_handle_open, ModelRotation.X270_Y0);
		else
			this.addSubmodel(te != null && te.isClosed() ? this.valve_handle_closed : this.valve_handle_open, new TRSRTransformation(te != null ? te.getHandleFacing() : EnumFacing.UP));
	}
}
