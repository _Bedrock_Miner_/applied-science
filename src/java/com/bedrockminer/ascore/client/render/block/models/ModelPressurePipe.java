package com.bedrockminer.ascore.client.render.block.models;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.tileentity.TilePressurePipe;
import com.bedrockminer.ascore.util.BlockSide;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;

public class ModelPressurePipe extends ModelPipeBase {

	@Override
	public void loadSubmodels() {
		super.loadSubmodels();
		this.pipe_part.textures.put("pipe", "ascore:blocks/pressure_pipe");
		this.pipe_cap.textures.put("pipe", "ascore:blocks/pressure_pipe");
	}

	@Override
	public TextureAtlasSprite getParticleResource() {
		return this.getSprite(new ResourceLocation("ascore:blocks/pressure_pipe"));
	}

	@Override
	public VertexFormat getVertexFormat() {
		return DefaultVertexFormats.POSITION_TEX_COLOR;
	}

	@Override
	public boolean pipePart(EnumFacing side) {
		if (this.getRenderPos() != null) {
			TilePressurePipe te = (TilePressurePipe) ASBlocks.pressurePipe.getTE(Minecraft.getMinecraft().theWorld, this.getRenderPos());
			if (te != null)
				return te.getConnection().getConnectedPositions().contains(new BlockSide(this.getRenderPos(), side));
		}
		return side.getAxis() == Axis.Z;
	}
}
