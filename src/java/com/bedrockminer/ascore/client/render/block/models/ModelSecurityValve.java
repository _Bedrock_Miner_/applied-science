package com.bedrockminer.ascore.client.render.block.models;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.tileentity.TileSecurityValve;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;

public class ModelSecurityValve extends ModelPipeBase {

	@Override
	public void loadSubmodels() {
		super.loadSubmodels();
		this.pipe_part.textures.put("pipe", "ascore:blocks/security_valve");
		this.pipe_cap.textures.put("pipe", "ascore:blocks/security_valve");
	}

	@Override
	public TextureAtlasSprite getParticleResource() {
		return this.getSprite(new ResourceLocation("ascore:blocks/security_valve"));
	}

	@Override
	public VertexFormat getVertexFormat() {
		return DefaultVertexFormats.POSITION_TEX_COLOR;
	}

	@Override
	public boolean pipePart(EnumFacing side) {
		if (this.getRenderPos() != null) {
			TileSecurityValve te = (TileSecurityValve) ASBlocks.securityValve.getTE(Minecraft.getMinecraft().theWorld, this.getRenderPos());
			if (te != null)
				return side.getAxis() == te.getOrientation();
		}
		return side.getAxis() == Axis.Z;
	}
}
