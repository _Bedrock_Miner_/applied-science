package com.bedrockminer.ascore.client.render.block;

import org.lwjgl.opengl.GL11;

import com.bedrockminer.ascore.block.base.BlockPipeBase;
import com.bedrockminer.ascore.tileentity.TilePressureGauge;
import com.bedrockminer.ascore.units.IUnit;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;

public class TilePressureGaugeRenderer extends TileEntitySpecialRenderer<TilePressureGauge> {

	private static final ResourceLocation gauge = new ResourceLocation("ascore:textures/blocks/pressure_gauge.png");

	@SuppressWarnings("incomplete-switch")
	@Override
	public void renderTileEntityAt(TilePressureGauge te, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 0.5, y + 0.5, z + 0.5);

		Tessellator t = Tessellator.getInstance();
		WorldRenderer wr = t.getWorldRenderer();

		switch (te.getGaugeFacing()) {
		case UP:
			GlStateManager.rotate(-90, 1, 0, 0);
			break;
		case DOWN:
			GlStateManager.rotate(90, 1, 0, 0);
			break;
		case NORTH:
			GlStateManager.rotate(180, 0, 1, 0);
			break;
		case EAST:
			GlStateManager.rotate(90, 0, 1, 0);
			break;
		case WEST:
			GlStateManager.rotate(270, 0, 1, 0);
			break;
		}

		if (te.getGaugeFacing().getAxis() == Axis.Y) {
			switch (te.getGaugeOrientation()) {
			case NORTH:
				GlStateManager.rotate(180, 0, 0, 1);
				break;
			case EAST:
				GlStateManager.rotate(te.getGaugeFacing() == EnumFacing.DOWN ? -90 : 90, 0, 0, 1);
				break;
			case WEST:
				GlStateManager.rotate(te.getGaugeFacing() == EnumFacing.DOWN ? 90 : -90, 0, 0, 1);
				break;
			}
		}
		GlStateManager.translate(-0.5, -0.5, -0.5);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		GlStateManager.disableLighting();

		Minecraft.getMinecraft().getTextureManager().bindTexture(gauge);
		wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);

		// Front Face
		wr.pos(0.25, 0.25, 0.75).tex(0.25, 0.75).endVertex();
		wr.pos(0.75, 0.25, 0.75).tex(0.75, 0.75).endVertex();
		wr.pos(0.75, 0.75, 0.75).tex(0.75, 0.25).endVertex();
		wr.pos(0.25, 0.75, 0.75).tex(0.25, 0.25).endVertex();
		// Top Face
		wr.pos(0.25, 0.75, 0.75).tex(0.25, 0.75).endVertex();
		wr.pos(0.75, 0.75, 0.75).tex(0.75, 0.75).endVertex();
		wr.pos(0.75, 0.75, 0.625).tex(0.75, 0.875).endVertex();
		wr.pos(0.25, 0.75, 0.625).tex(0.25, 0.875).endVertex();
		// Bottom Face
		wr.pos(0.25, 0.25, 0.625).tex(0.25, 0.875).endVertex();
		wr.pos(0.75, 0.25, 0.625).tex(0.75, 0.875).endVertex();
		wr.pos(0.75, 0.25, 0.75).tex(0.75, 0.75).endVertex();
		wr.pos(0.25, 0.25, 0.75).tex(0.25, 0.75).endVertex();
		// Left Face
		wr.pos(0.25, 0.25, 0.625).tex(0.125, 0.75).endVertex();
		wr.pos(0.25, 0.25, 0.75).tex(0.25, 0.75).endVertex();
		wr.pos(0.25, 0.75, 0.75).tex(0.25, 0.25).endVertex();
		wr.pos(0.25, 0.75, 0.625).tex(0.125, 0.25).endVertex();
		// Right Face
		wr.pos(0.75, 0.25, 0.75).tex(0.75, 0.75).endVertex();
		wr.pos(0.75, 0.25, 0.625).tex(0.875, 0.75).endVertex();
		wr.pos(0.75, 0.75, 0.625).tex(0.875, 0.25).endVertex();
		wr.pos(0.75, 0.75, 0.75).tex(0.75, 0.25).endVertex();
		// Back Face Top Part
		wr.pos(0.75, 0.625, 0.625).tex(0.25, 0.125).endVertex();
		wr.pos(0.25, 0.625, 0.625).tex(0.75, 0.125).endVertex();
		wr.pos(0.25, 0.75, 0.625).tex(0.75, 0.25).endVertex();
		wr.pos(0.75, 0.75, 0.625).tex(0.25, 0.25).endVertex();
		// Back Face Bottom Part
		wr.pos(0.75, 0.25, 0.625).tex(0.25, 0.75).endVertex();
		wr.pos(0.25, 0.25, 0.625).tex(0.75, 0.75).endVertex();
		wr.pos(0.25, 0.375, 0.625).tex(0.75, 0.875).endVertex();
		wr.pos(0.75, 0.375, 0.625).tex(0.25, 0.875).endVertex();
		// Back Face Left Part
		wr.pos(0.375, 0.375, 0.625).tex(0.125, 0.625).endVertex();
		wr.pos(0.25, 0.375, 0.625).tex(0.25, 0.625).endVertex();
		wr.pos(0.25, 0.625, 0.625).tex(0.25, 0.375).endVertex();
		wr.pos(0.375, 0.625, 0.625).tex(0.125, 0.375).endVertex();
		// Back Face Right Part
		wr.pos(0.75, 0.375, 0.625).tex(0.75, 0.625).endVertex();
		wr.pos(0.625, 0.375, 0.625).tex(0.875, 0.625).endVertex();
		wr.pos(0.625, 0.625, 0.625).tex(0.875, 0.375).endVertex();
		wr.pos(0.75, 0.625, 0.625).tex(0.75, 0.375).endVertex();
		t.draw();

		// Outline

		int color = BlockPipeBase.dye_colors[te.getPipeColor().getDyeDamage()];
		float r = ((color >> 16) & 255) / 255.0f;
		float g = ((color >> 8) & 255) / 255.0f;
		float b = (color & 255) / 255.0f;
		GlStateManager.color(r, g, b);
		GlStateManager.disableTexture2D();
		GL11.glLineWidth(2.0f);
		wr.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION);
		wr.pos(0.25, 0.25, 0.75).endVertex();
		wr.pos(0.75, 0.25, 0.75).endVertex();
		wr.pos(0.75, 0.75, 0.75).endVertex();
		wr.pos(0.25, 0.75, 0.75).endVertex();
		wr.pos(0.25, 0.25, 0.75).endVertex();
		t.draw();
		GlStateManager.enableTexture2D();

		// Displayed Data

		// Text
		GlStateManager.pushMatrix();
		GlStateManager.translate(0.5, 0.5, 0.5);
		GlStateManager.rotate(180, 1, 0, 0);
		GlStateManager.translate(0, 0, -0.251);
		GlStateManager.scale(1 / 128.0, 1 / 128.0, 1 / 128.0);

		// Current Pressure
		String s = Units.formatFrom(te.getPressure(), Pressure.BAR);
		int l = Minecraft.getMinecraft().fontRendererObj.getStringWidth(s);
		Minecraft.getMinecraft().fontRendererObj.drawString(s, -l / 2, 16, 0xffffff);

		// Scale

		// Preparing scale steps
		double max = Units.convertFromInternal(te.getGaugeMaximum(), Pressure.BAR);
		int step = (int) (max / 4);
		max += Units.convertDiffFromInternal((te.getGaugeMaximum() - 1) / 10.0, Pressure.BAR);

		int power = (int) Math.floor(Math.pow(10, Math.floor(Math.log10(step))));
		if (power == 0)
			power = 1;
		int stepl = step / power;
		if (stepl < 2) {
			step = power;
		} else if (stepl >= 2 && stepl < 4) {
			step = 2 * power;
		} else if (stepl >= 4 && stepl < 7) {
			step = 5 * power;
		} else {
			step = 10 * power;
		}
		if (max / step > 6)
			step *= 2;

		// Rendering scale
		for (int p = 0; p <= max; p += step) {
			this.renderValue(te, p);
		}

		GlStateManager.popMatrix();

		// Indicator
		GlStateManager.pushMatrix();
		double percentage = (te.getPressure() - 1) / (te.getGaugeMaximum() - 1);
		GlStateManager.translate(0.5, 0.5, 0.5);
		GlStateManager.rotate((float) Math.toDegrees(this.getAngleFromPercentage(percentage)), 0, 0, 1);
		GlStateManager.translate(-0.5, -0.5, -0.5);
		GlStateManager.disableTexture2D();
		GlStateManager.shadeModel(GL11.GL_SMOOTH);
		wr.begin(GL11.GL_TRIANGLES, DefaultVertexFormats.POSITION_COLOR);
		wr.pos(0.53125, 0.5, 0.752).color(0.0f, 0.0f, 0.0f, 1.0f).endVertex();
		wr.pos(0.46875, 0.5, 0.752).color(0.0f, 0.0f, 0.0f, 1.0f).endVertex();
		wr.pos(0.5, 0.3, 0.752).color(0.5f, 0.5f, 0.5f, 1.0f).endVertex();
		t.draw();
		GlStateManager.enableTexture2D();
		GlStateManager.shadeModel(GL11.GL_FLAT);
		GlStateManager.popMatrix();

		GlStateManager.enableLighting();
		GlStateManager.popMatrix();
	}

	private String format(double pressure) {
		IUnit unit = Pressure.BAR.getUnitFromConfig();
		if (unit.getSpecialNumberFormat() != null)
			return unit.getSpecialNumberFormat().format(pressure);
		return Units.DECIMAL_FORMAT.format(pressure);
	}

	private void renderValue(TilePressureGauge te, int display) {
		double percentage = (Units.convertFromUser(display, Pressure.BAR) - 1) / (te.getGaugeMaximum() - 1);
		String str = this.format(display);
		if (str.startsWith("+"))
			str = str.substring(1);
		boolean small = str.length() > 2;

		float angle = (float) (180 - Math.toDegrees(this.getAngleFromPercentage(percentage)));
		GlStateManager.rotate(angle, 0, 0, 1);
		if (small)
			GlStateManager.scale(2.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0);
		Minecraft.getMinecraft().fontRendererObj.drawString(str, -Minecraft.getMinecraft().fontRendererObj.getStringWidth(str) / 2, small ? -38 : -25, 0);
		if (small)
			GlStateManager.scale(1.5, 1.5, 1.5);
		GlStateManager.disableTexture2D();
		GlStateManager.color(0.0f, 0.0f, 0.0f, 1.0f);
		GL11.glLineWidth(2.0f);
		Tessellator.getInstance().getWorldRenderer().begin(GL11.GL_LINES, DefaultVertexFormats.POSITION);
		Tessellator.getInstance().getWorldRenderer().pos(0, -25, 0).endVertex();
		Tessellator.getInstance().getWorldRenderer().pos(0, -28, 0).endVertex();
		Tessellator.getInstance().draw();
		GlStateManager.enableTexture2D();

		GlStateManager.rotate(-angle, 0, 0, 1);
	}

	/**
	 * Returns the angle a point with the given percentage will be visible at.
	 *
	 * @param percentage
	 * the percentage
	 * @return the angle in radians
	 */
	private double getAngleFromPercentage(double percentage) {
		return -1.418 - 3.447 * percentage;
	}
}
