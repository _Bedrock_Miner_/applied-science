package com.bedrockminer.ascore.client.render.block.models;

import static net.minecraftforge.client.model.TRSRTransformation.quatFromYXZDegrees;

import javax.vecmath.Vector3f;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.tileentity.TilePressureReducer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.block.model.ModelBlock;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.resources.model.ModelRotation;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ITransformation;
import net.minecraftforge.client.model.TRSRTransformation;

@SuppressWarnings("deprecation")
public class ModelPressureReducer extends ModelBase {

	private ModelBlock pipe;

	@Override
	public void loadSubmodels() {
		this.pipe = this.loadSubmodel(new ResourceLocation("ascore:models/block/pressure_pipe_full.json"));
		this.pipe.textures.put("pipe", "ascore:blocks/pressure_reducer_pipe");
	}

	@Override
	public TextureAtlasSprite getParticleResource() {
		return this.getSprite(new ResourceLocation("ascore:blocks/pressure_reducer_pipe"));
	}

	@Override
	public VertexFormat getVertexFormat() {
		return DefaultVertexFormats.POSITION_TEX_COLOR;
	}

	@Override
	public void putQuads() {
		EnumFacing facing = EnumFacing.NORTH;
		if (this.getRenderPos() != null) {
			TilePressureReducer te = (TilePressureReducer) ASBlocks.pressureReducer.getTE(Minecraft.getMinecraft().theWorld, this.getRenderPos());
			if (te != null) {
				facing = te.getOrientation();
			}
		}
		this.addSubmodel(this.pipe, new TRSRTransformation(facing));
	}

	@Override
	public ITransformation getTransformation(TransformType renderType) {
		switch (renderType) {
		case THIRD_PERSON:
			return new TRSRTransformation(new Vector3f(0, 0.09375f, -0.171875f), quatFromYXZDegrees(new Vector3f(10, -45, 170)), new Vector3f(0.375f, 0.375f, 0.375f), null);
		case FIXED:
			return new TRSRTransformation(new Vector3f(0, 0, -0.02f), quatFromYXZDegrees(new Vector3f(0, 90, 0)), new Vector3f(1.2f, 1.2f, 1.2f), null);
		default:
			return ModelRotation.X0_Y0;
		}
	}
}
