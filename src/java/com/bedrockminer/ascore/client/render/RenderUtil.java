package com.bedrockminer.ascore.client.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class RenderUtil {

	/**
	 * Draws a solidified face at the x/y plane with the given width, similar to
	 * the item renderer. The image ranges from 0|0 to 1|1, the front is
	 * pointing north, to -z.
	 *
	 * @param u
	 * @param v
	 * @param U
	 * @param V
	 * @param thickness
	 */
	public static void renderSolidifiedFace(double u, double v, double U, double V, double thickness) {
		int imgwidth = GL11.glGetTexLevelParameteri(GL11.GL_TEXTURE_2D, 0, GL11.GL_TEXTURE_WIDTH);
		int imgheight = GL11.glGetTexLevelParameteri(GL11.GL_TEXTURE_2D, 0, GL11.GL_TEXTURE_HEIGHT);
		int width = (int) (imgwidth * (U - u));
		int height = (int) (imgheight * (V - v));

		Tessellator t = Tessellator.getInstance();
		WorldRenderer wr = t.getWorldRenderer();

		wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_NORMAL);

		double z = -thickness / 2.0;
		double Z = thickness / 2.0;

		// Front face
		wr.pos(0, 0, Z).tex(u, V).normal(0, 0, -1).endVertex();
		wr.pos(1, 0, Z).tex(U, V).normal(0, 0, -1).endVertex();
		wr.pos(1, 1, Z).tex(U, v).normal(0, 0, -1).endVertex();
		wr.pos(0, 1, Z).tex(u, v).normal(0, 0, -1).endVertex();
		// Back face
		wr.pos(0, 0, z).tex(u, V).normal(0, 0, 1).endVertex();
		wr.pos(0, 1, z).tex(u, v).normal(0, 0, 1).endVertex();
		wr.pos(1, 1, z).tex(U, v).normal(0, 0, 1).endVertex();
		wr.pos(1, 0, z).tex(U, V).normal(0, 0, 1).endVertex();

		for (int pu = 0; pu < width; pu++) {
			for (int pv = 0; pv < height; pv++) {
				double x = (double) pu / width;
				double y = (double) pv / height;
				double X = (double) (pu + 1) / width;
				double Y = (double) (pv + 1) / height;
				double iu = u + (U - u) * x;
				double iU = u + (U - u) * X;
				double iv = v + (V - v) * (1 - y);
				double iV = v + (V - v) * (1 - Y);

				// Top Faces
				wr.pos(X, Y, z).tex(iU, iv).normal(0, 1, 0).endVertex();
				wr.pos(x, Y, z).tex(iu, iv).normal(0, 1, 0).endVertex();
				wr.pos(x, Y, Z).tex(iu, iV).normal(0, 1, 0).endVertex();
				wr.pos(X, Y, Z).tex(iU, iV).normal(0, 1, 0).endVertex();
				// Bottom Faces
				wr.pos(x, y, z).tex(iu, iv).normal(0, -1, 0).endVertex();
				wr.pos(X, y, z).tex(iU, iv).normal(0, -1, 0).endVertex();
				wr.pos(X, y, Z).tex(iU, iV).normal(0, -1, 0).endVertex();
				wr.pos(x, y, Z).tex(iu, iV).normal(0, -1, 0).endVertex();
				// West Faces
				wr.pos(x, Y, z).tex(iU, iv).normal(-1, 0, 0).endVertex();
				wr.pos(x, y, z).tex(iu, iv).normal(-1, 0, 0).endVertex();
				wr.pos(x, y, Z).tex(iu, iV).normal(-1, 0, 0).endVertex();
				wr.pos(x, Y, Z).tex(iU, iV).normal(-1, 0, 0).endVertex();
				// East Faces
				wr.pos(X, y, z).tex(iu, iv).normal(1, 0, 0).endVertex();
				wr.pos(X, Y, z).tex(iU, iv).normal(1, 0, 0).endVertex();
				wr.pos(X, Y, Z).tex(iU, iV).normal(1, 0, 0).endVertex();
				wr.pos(X, y, Z).tex(iu, iV).normal(1, 0, 0).endVertex();
			}
		}
		t.draw();
	}
}
