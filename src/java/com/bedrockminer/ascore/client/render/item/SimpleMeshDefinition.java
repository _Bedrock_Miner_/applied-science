package com.bedrockminer.ascore.client.render.item;

import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.ItemStack;

/**
 * A simple ItemMeshDefinition that returns a constant model.
 * @author _Bedrock_Miner_
 */
public class SimpleMeshDefinition implements ItemMeshDefinition {

	private ModelResourceLocation model;

	public SimpleMeshDefinition(ModelResourceLocation model) {
		this.model = model;
	}

	@Override
	public ModelResourceLocation getModelLocation(ItemStack stack) {
		return this.model;
	}

}
