package com.bedrockminer.ascore;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.config.Cfg;
import com.bedrockminer.ascore.item.ASItems;
import com.bedrockminer.ascore.network.gui.ASGuiHandler;
import com.bedrockminer.ascore.network.packets.ASPackets;
import com.bedrockminer.ascore.registries.TemperatureRegistry;
import com.bedrockminer.ascore.tileentity.ASTileEntities;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent e) {
		Cfg.init(e.getSuggestedConfigurationFile());
		ASBlocks.init();
		ASItems.init();
	}

	public void init(FMLInitializationEvent e) {
		ASTileEntities.init();
		ASPackets.init();

		TemperatureRegistry.registerVanilla();

		NetworkRegistry.INSTANCE.registerGuiHandler(AppliedScience.instance, new ASGuiHandler());

		// Registering this mod with WAILA
		FMLInterModComms.sendMessage("Waila", "register", "com.bedrockminer.ascore.modintegration.waila.WailaTileHandler.registerWailaData");
	}

	public void postInit(FMLPostInitializationEvent e) {

	}
}
