package com.bedrockminer.ascore;

import com.bedrockminer.ascore.client.event.EventHandlerModelBake;
import com.bedrockminer.ascore.client.event.EventHandlerTextureStitching;
import com.bedrockminer.ascore.client.event.ResourceReloadListener;
import com.bedrockminer.ascore.client.render.block.BlockRenderRegistrar;
import com.bedrockminer.ascore.client.render.block.SpecialRenderer;
import com.bedrockminer.ascore.client.render.item.ItemRenderRegistrar;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.SimpleReloadableResourceManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);
		BlockRenderRegistrar.preInit();
	}

	@Override
	public void init(FMLInitializationEvent e) {
		super.init(e);
		BlockRenderRegistrar.init();
		ItemRenderRegistrar.init();
		SpecialRenderer.init();

		MinecraftForge.EVENT_BUS.register(new EventHandlerTextureStitching());
		MinecraftForge.EVENT_BUS.register(new EventHandlerModelBake());
		((SimpleReloadableResourceManager)Minecraft.getMinecraft().getResourceManager()).registerReloadListener(new ResourceReloadListener());
	}

	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}

}
