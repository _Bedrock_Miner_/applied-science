package com.bedrockminer.ascore.circuitry;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.resources.I18n;

/**
 * Each connection (and each circuit) has a special connection type. This can
 * be, for instance, PRESSURE or ROTATION. Connections of a type can only
 * connect to other connections of the same type. This class is designed as a
 * registry type to allow adding more types, maybe through addon-mods or
 * similar.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public final class ConnectionType {

	/** A list of all registered types */
	private static List<ConnectionType> types = new ArrayList<ConnectionType>();

	/** The maximum ID */
	private static int maxID = -1;

	/** The ID of the type */
	private int ID;
	/** The identifier of the type */
	private String identifier;

	// ========================================================================
	// Type constants

	/** Error type, used when transmission failed */
	public static final ConnectionType ERROR = new ConnectionType("error");

	/** The type for pressure transmission */
	public static final ConnectionType PRESSURE = new ConnectionType("pressure");

	/** The type for rotation transmission */
	public static final ConnectionType ROTATION = new ConnectionType("rotation");

	// ========================================================================

	/**
	 * Instantiates a new ConnectionType and registers it.
	 *
	 * @param identifier the identifier.
	 */
	public ConnectionType(String identifier) {
		this.ID = ++maxID;
		this.identifier = identifier;

		this.types.add(this);
	}

	/**
	 * Returns the ID of the ConnectionType.
	 *
	 * @return the ID
	 */
	public int getID() {
		return this.ID;
	}

	/**
	 * Returns the identifier of the ConnectionType.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return this.identifier;
	}

	/**
	 * Returns the translated name.
	 *
	 * @return the translated name
	 */
	public String getLocalizedName() {
		return I18n.format("connection_type." + this.getIdentifier());
	}

	@Override
	public int hashCode() {
		return this.getID();
	}

	@Override
	public boolean equals(Object other) {
		return other.getClass() == this.getClass() && ((ConnectionType)other).getID() == this.getID();
	}

	@Override
	public String toString() {
		return "Connection type " + this.getIdentifier();
	}

	/**
	 * Returns a ConnectionType by it's ID.
	 *
	 * @param ID the ID to look for
	 * @return the ConnectionType
	 */
	public static ConnectionType getByID(int ID) {
		if (ID < 0 || ID >= types.size())
			return null;
		return types.get(ID);
	}

	/**
	 * Returns a ConnectionType by it's identifier.
	 *
	 * @param identifier the identifier to look for
	 * @return the ConnectionType
	 */
	public static ConnectionType getByIdentifier(String identifier) {
		for (ConnectionType type: types) {
			if (type.getIdentifier().equals(identifier))
				return type;
		}
		return null;
	}
}
