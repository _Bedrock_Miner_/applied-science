package com.bedrockminer.ascore.circuitry.connections;

import com.bedrockminer.ascore.circuitry.Circuit;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.ConnectionType;
import com.bedrockminer.ascore.circuitry.circuits.RotationCircuit;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;

import net.minecraft.nbt.NBTTagCompound;

/**
 * The rotation connection is used in rotation circuits. Its properties are its
 * friction, its rotation speed and its rotational inertia.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class RotationConnection extends Connection {

	/** The rotational inertia of this connection in kg*m� */
	private double rotationalInertia = 1;
	/** The resistance of this connection in N*m, usually used for friction */
	private double resistance = 0;
	/** The stored rotation speed used when assembling the circuit in 1/s */
	private double rotationSpeed = 0;
	/** The torque on the rotation axis for the next calculation step in N*m */
	private double torque;
	/** The current rotation in revolutions. Capped at 2048 */
	private double rotation = 0;

	/**
	 * Instantiates a new RotationConnection and assigns an ID to it. The ID
	 * must be unique within the scope of the controlling TileEntity, persistent
	 * on restart of Minecraft and can be any value between 0 and 127. The
	 * passive value can make this connection a passive one. This should be set
	 * if the TileEntity will never change the torque of the connection to a
	 * value other than 0. This can be used for mechanical components.
	 *
	 * @param ID
	 * the Connection's ID
	 * @param passive
	 * <code>true</code> if this connection is passive
	 * @param configurable
	 * <code>true</code> to allow changing selected connections instead of using
	 * all available connections.
	 * @param te
	 * the controlling TileEntity
	 */
	public RotationConnection(int ID, boolean passive, boolean configurable, TileCircuitElement te) {
		super(ID, passive, configurable, te);
	}

	@Override
	public ConnectionType getConnectionType() {
		return ConnectionType.ROTATION;
	}

	@Override
	protected Circuit createCircuit() {
		if (this.isSetUp())
			return new RotationCircuit(this.getTE().getWorld(), this);
		return null;
	}

	@Override
	public RotationCircuit getCircuit() {
		return (RotationCircuit) super.getCircuit();
	}

	// Rotation properties

	/**
	 * Returns the rotational inertia of the connection.
	 *
	 * @return the rotational inertia
	 */
	public double getInertia() {
		return this.rotationalInertia;
	}

	/**
	 * Sets the rotational inertia of this connection.
	 *
	 * @param inertia the inertia to set
	 */
	public void setInertia(double inertia) {
		if (this.hasCircuit())
			this.getCircuit().inertiaChanged(inertia - this.rotationalInertia);
		this.rotationalInertia = inertia;
	}

	/**
	 * Returns the resistance of this connection.
	 *
	 * @return the resistance
	 */
	public double getResistance() {
		return this.resistance;
	}

	/**
	 * Sets the resistance of this connection.
	 *
	 * @param resistance the resistance to set
	 */
	public void setResistance(double resistance) {
		if (resistance < 0)
			resistance = 0;
		if (this.isPassive() && this.hasCircuit())
			this.getCircuit().resistanceChanged(resistance - this.resistance);
		this.resistance = resistance;
	}

	/**
	 * Returns the current rotation speed. Use this to calculate your changes.
	 * <p>
	 * <em>SERVER SIDE ONLY!</em>
	 *
	 * @return the rotation speed
	 */
	public double getRotationSpeed() {
		return this.getCircuit().getSpeed();
	}

	/**
	 * Returns the rotation speed that was stored in this connection before the
	 * last save.
	 *
	 * @return the stored rotation speed
	 */
	public double getStoredRotationSpeed() {
		return this.rotationSpeed;
	}

	/**
	 * Returns the rotation speed for client side.
	 *
	 * @return the rotation speed
	 */
	public double getRotationSpeedClient() {
		return this.rotationSpeed;
	}

	@Override
	public void storeCircuitData() {
		if (this.hasCircuit())
			this.rotationSpeed = this.getRotationSpeed();
	}

	/**
	 * Returns the torque on the connection for the next calculation step.
	 *
	 * @return the torque
	 */
	public double getTorque() {
		return this.torque;
	}

	/**
	 * Sets the torque on the connection for the nect calculation step.
	 *
	 * @param torque the torque to set.
	 */
	public void setTorque(double torque) {
		this.torque = torque;
	}

	/**
	 * Returns the current rotation in revolutions. Capped at 2048.
	 *
	 * @return the rotation
	 */
	public double getRotation() {
		return this.rotation;
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();
		if (this.isSetUp() && this.rotationSpeed > 0) {
			this.rotation += this.rotationSpeed / (40 * Math.PI);
			if (this.rotation > 2048)
				this.rotation %= 2048;
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		if (!this.getTE().getWorld().isRemote)
			nbt.setDouble("Speed", this.getRotationSpeed());
		nbt.setDouble("Inertia", this.getInertia());
		nbt.setDouble("Resistance", this.getResistance());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		this.rotationSpeed = nbt.getDouble("Speed");
		this.setInertia(nbt.getDouble("Inertia"));
		this.setResistance(nbt.getDouble("Resistance"));
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setDouble("Speed", this.getRotationSpeed());
		nbt.setDouble("Rotation", this.getCircuit().getRotation());
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		this.rotationSpeed = nbt.getDouble("Speed");
		this.rotation = nbt.getDouble("Rotation");
	}
}
