package com.bedrockminer.ascore.circuitry.connections;

import com.bedrockminer.ascore.circuitry.Circuit;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.ConnectionType;
import com.bedrockminer.ascore.circuitry.circuits.PressureCircuit;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;

import net.minecraft.nbt.NBTTagCompound;

/**
 * The pressure connection is used in pressure circuits. Its properties are its
 * volume and the volume of air stored inside it.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class PressureConnection extends Connection {

	/** The internal volume of this connection (in m^3) */
	private double volume = 0.25;
	/** The stored value for the air volume, used when assembling the circuit */
	private double airVolume = -1;
	/** The change of air for the next calculation step */
	private double airChange;

	/**
	 * Instantiates a new PressureConnection and assigns an ID to it. The ID
	 * must be unique within the scope of the controlling TileEntity, persistent
	 * on restart of Minecraft and can be any value between 0 and 127. The
	 * passive value can make this connection a passive one. This should be set
	 * if the TileEntity will never change the airChange of the connection to a
	 * value other than 0. This can be used for pipes or storages.
	 *
	 * @param ID
	 * the Connection's ID
	 * @param passive
	 * <code>true</code> if this connection is passive
	 * @param configurable
	 * <code>true</code> to allow changing selected connections instead of using
	 * all available connections.
	 * @param te
	 * the controlling TileEntity
	 */
	public PressureConnection(int ID, boolean passive, boolean configurable, TileCircuitElement te) {
		super(ID, passive, configurable, te);
	}

	@Override
	public ConnectionType getConnectionType() {
		return ConnectionType.PRESSURE;
	}

	@Override
	protected Circuit createCircuit() {
		if (this.isSetUp())
			return new PressureCircuit(this.getTE().getWorld(), this);
		return null;
	}

	@Override
	public PressureCircuit getCircuit() {
		return (PressureCircuit) super.getCircuit();
	}

	// Pressure Properties

	/**
	 * Sets the internal volume of this connection. The default is 0.25
	 *
	 * @param volume
	 * the volume to set
	 * @return this object
	 */
	public PressureConnection setVolume(double volume) {
		if (this.hasCircuit())
			this.getCircuit().volumeChanged(volume - this.volume);
		this.volume = volume;
		return this;
	}

	/**
	 * Returns the internal volume of this connection.
	 *
	 * @return the volume of this connection
	 */
	public double getVolume() {
		return this.volume;
	}

	/**
	 * Returns the amount of air stored inside this connection. Use this to
	 * calculate your changes.
	 * <p>
	 * <em>SERVER SIDE ONLY!</em>
	 *
	 * @return the last volume of air
	 */
	public double getAirVolume() {
		return this.getPressure() * this.getVolume();
	}

	/**
	 * Returns the volume of air that was stored in this pipe before the last
	 * save.
	 *
	 * @return the amount of air in the pipe before the last save
	 */
	public double getStoredAirVolume() {
		if (this.airVolume == -1)
			this.airVolume = this.getVolume();
		return this.airVolume;
	}

	@Override
	public void storeCircuitData() {
		if (this.hasCircuit())
			this.airVolume = this.getAirVolume();
	}

	/**
	 * Sets the change of air for the next calculation tick.
	 *
	 * @param air
	 * the volume of air
	 */
	public void setAirChange(double air) {
		if (Double.isNaN(air))
			air = 0;
		this.airChange = air;
	}

	/**
	 * Returns the change of air for the next calculation tick.
	 *
	 * @return the volume of air
	 */
	public double getAirChange() {
		return this.airChange;
	}

	/**
	 * Returns the pressure in this connection in bar. The returned pressure is
	 * absolute, thus the default value is 1.
	 * <p>
	 * <em>SERVER SIDE ONLY!</em>
	 *
	 * @return the pressure
	 */
	public double getPressure() {
		return this.getCircuit().getPressure();
	}

	// Saving data

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		if (!this.getTE().getWorld().isRemote)
			nbt.setDouble("Air", this.getAirVolume());
		nbt.setDouble("Volume", this.getVolume());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		this.airVolume = nbt.getDouble("Air");
		this.setVolume(nbt.getDouble("Volume"));
	}
}
