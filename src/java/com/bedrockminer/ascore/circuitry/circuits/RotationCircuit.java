package com.bedrockminer.ascore.circuitry.circuits;

import java.util.HashSet;
import java.util.Set;

import com.bedrockminer.ascore.circuitry.Circuit;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.ConnectionType;
import com.bedrockminer.ascore.circuitry.connections.RotationConnection;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.Sets;

import net.minecraft.world.World;

/**
 * A RotationCircuit manages Connections of the ConnectionType ROTATION.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class RotationCircuit extends Circuit {

	/**
	 * The rotational inertia of the whole circuit with the factors taken into
	 * account. in kg*m�
	 */
	private double inertia;
	/** The resistance of the axles in this circuit in N*m */
	private double axleResistance;
	/** The rotation speed of the whole circuit in 1/s */
	private double rotationSpeed;
	private double prevRotationSpeed;
	/** The current rotation in revolutions. Capped at 2048 */
	private double rotation;

	/** All machines in this circuit */
	private Set<RotationConnection> machines;
	/** All pipes in this circuit */
	private Set<RotationConnection> axles;

	public RotationCircuit(World world, Connection start) {
		super(world, start);
	}

	@Override
	public ConnectionType getCircuitType() {
		return ConnectionType.ROTATION;
	}

	// Assembly procedure

	@Override
	protected void startAssembly() {
		this.inertia = 0;
		this.axleResistance = 0;
		this.rotationSpeed = 0;
		this.rotation = 0;

		this.machines = new HashSet<RotationConnection>();
		this.axles = new HashSet<RotationConnection>();
	}

	@Override
	protected void foundConnection(Connection connection) {
		if (connection instanceof RotationConnection) {
			if (connection.isPassive())
				this.axles.add((RotationConnection) connection);
			else
				this.machines.add((RotationConnection) connection);
		}
	}

	@Override
	protected void foundLeak(BlockSide position) {
		// ignored
	}

	@Override
	protected void finishAssembly() {
		Set<RotationConnection> connections = Sets.union(this.machines, this.axles);
		double angularMomentum = 0;

		for (RotationConnection c : connections) {
			this.inertia += c.getInertia();
			angularMomentum += c.getStoredRotationSpeed() * c.getInertia();
			if (c.getRotation() != 0)
				this.rotation = c.getRotation();
		}
		for (RotationConnection c : this.axles) {
			this.axleResistance += c.getResistance();
		}
		this.rotationSpeed = angularMomentum / this.inertia;
	}

	@Override
	public Set<Connection> getAllConnections() {
		return Sets.<Connection> union(this.machines, this.axles);
	}

	/**
	 * Returns the rotational inertia of the whole circuit.
	 *
	 * @return the inertia
	 */
	public double getInertia() {
		return this.inertia;
	}

	/**
	 * Returns the rotation speed of the whole circuit.
	 *
	 * @return the rotation speed
	 */
	public double getSpeed() {
		return this.rotationSpeed;
	}

	/**
	 * Returns the current rotation in revolutions.
	 *
	 * @return the rotation
	 */
	public double getRotation() {
		return this.rotation;
	}

	/**
	 * Called when one of the RotationConnections changes its inertia. If the
	 * inertia increases, the movement will slow down.
	 *
	 * @param change
	 * the change of inertia
	 */
	public void inertiaChanged(double change) {
		if (change > 0) {
			this.rotationSpeed = this.rotationSpeed * this.inertia / (this.inertia + change);
		}
		this.inertia += change;
	}

	/**
	 * Called when one of the axles (not machines!) changes its resistance.
	 * Only call this from a passive connection!
	 *
	 * @param change the change of resistance.
	 */
	public void resistanceChanged(double change) {
		this.axleResistance += change;
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();
		double torque = 0;
		double resistance = this.axleResistance;

		for (RotationConnection c : this.machines) {
			c.tryUpdate();
			torque += c.getTorque();
			resistance += c.getResistance();
			c.setTorque(0);
		}

		// Adjusting for the tick length of 50ms.
		torque *= 0.05;
		resistance *= 0.05;

		this.prevRotationSpeed = this.rotationSpeed;
		this.rotationSpeed += (torque - resistance) / this.getInertia();
		if (this.rotationSpeed < 0)
			this.rotationSpeed = 0;

		if (this.rotationSpeed != this.prevRotationSpeed) {
			for (RotationConnection c : this.machines)
				this.getWorld().markBlockForUpdate(c.getTE().getPos());
			for (RotationConnection c : this.axles)
				this.getWorld().markBlockForUpdate(c.getTE().getPos());
		}

		this.rotation += this.rotationSpeed / (40 * Math.PI);
		if (this.rotation > 2048)
			this.rotation %= 2048;
	}
}
