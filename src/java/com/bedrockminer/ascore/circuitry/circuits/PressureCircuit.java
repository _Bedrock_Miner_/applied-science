package com.bedrockminer.ascore.circuitry.circuits;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.bedrockminer.ascore.AppliedScience;
import com.bedrockminer.ascore.circuitry.Circuit;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.ConnectionType;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.network.packets.PacketAirLeak;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.Sets;

import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

/**
 * A PressureCircuit manages Connections of the ConnectionType PRESSURE.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class PressureCircuit extends Circuit {

	/** The amount of ticks between two packets */
	public static final int PACKET_DELAY = 3;

	/** The volume of the whole circuit (in m^3) */
	private double volume;
	/** The volume of the air in the whole circuit (in m^3) */
	private double airVolume;

	/** The last tick update packets were sent */
	private long lastPacketTick;

	/** All machines in this circuit */
	private Set<PressureConnection> machines;
	/** All pipes in this circuit */
	private Set<PressureConnection> pipes;
	/** All leaks in this circuit */
	private Set<BlockSide> leaks;

	/**
	 * Instantiates a new PressureCircuit and assembles it.
	 *
	 * @param world the world
	 * @param start the start connection
	 */
	public PressureCircuit(World world, Connection start) {
		super(world, start);
	}

	@Override
	public ConnectionType getCircuitType() {
		return ConnectionType.PRESSURE;
	}

	// Assembly procedure

	@Override
	protected void startAssembly() {
		this.volume = 0;
		this.airVolume = 0;

		this.machines = new HashSet<PressureConnection>();
		this.pipes = new HashSet<PressureConnection>();
		this.leaks = new HashSet<BlockSide>();
	}

	@Override
	protected void foundConnection(Connection connection) {
		if (connection instanceof PressureConnection) {
			if (connection.isPassive())
				this.pipes.add((PressureConnection) connection);
			else
				this.machines.add((PressureConnection) connection);
		}
	}

	@Override
	protected void foundLeak(BlockSide position) {
		this.leaks.add(position);
	}

	@Override
	protected void finishAssembly() {
		Set<PressureConnection> connections = Sets.union(this.machines, this.pipes);
		for (PressureConnection c : connections) {
			this.volume += c.getVolume();
			this.airVolume += c.getStoredAirVolume();
		}
	}

	// Pressure properties

	@Override
	public Set<Connection> getAllConnections() {
		return Sets.<Connection>union(this.machines, this.pipes);
	}

	/**
	 * Returns the current pressure in this circuit in bar. The returned
	 * pressure is absolute, thus the default value is 1.
	 *
	 * @return the pressure
	 */
	public double getPressure() {
		return this.getVolume() > 0 ? this.getAirVolume() / this.getVolume() : 1;
	}

	/**
	 * Returns the volume of this circuit.
	 *
	 * @return the volume
	 */
	public double getVolume() {
		return this.volume;
	}

	/**
	 * Returns the volume of air currently stored in this circuit.
	 *
	 * @return the air volume
	 */
	public double getAirVolume() {
		return this.airVolume;
	}

	/**
	 * Called when one of the pressure connections changes its volume.
	 * If the volume increases, the amount of air will stay the same. If the
	 * volume decreases, some air will be lost.
	 *
	 * @param change the change of volume
	 */
	public void volumeChanged(double change) {
		if (change < 0) {
			this.airVolume -= this.getPressure() * change;
		}
		this.volume += change;
	}

	/**
	 * Returns <code>true</code> if the circuit is leaking.
	 *
	 * @return <code>true</code> if leaking
	 */
	public boolean isLeaking() {
		return this.leaks.size() > 0;
	}

	// Update handling

	@Override
	protected void doUpdate() {
		super.doUpdate();
		double difference = 0;

  		for (PressureConnection c : this.machines) {
			c.tryUpdate();
			difference += c.getAirChange();
			c.setAirChange(0);
		}

		this.airVolume += difference;

		// If there are leaks, the air pressure is lost
		if (this.isLeaking()) {
			double loss = (this.getAirVolume() - this.getVolume()) / this.leaks.size();

			// Notifying players
			if (loss != 0 && this.lastPacketTick + 3 <= this.getWorld().getTotalWorldTime()) {
				for (BlockSide side : this.leaks)
					AppliedScience.packetHandler.sendToAllAround(new PacketAirLeak(side, loss), this.getWorld().provider.getDimensionId(), side.getPos().getX(), side.getPos().getY(), side.getPos().getZ(), 32);

				this.lastPacketTick = this.getWorld().getTotalWorldTime();
			}

			// Accelerating entities
			if (loss != 0) {
				for (BlockSide side : this.leaks) {
					// Getting all entities
					List<Entity> entities = this.getWorld().getEntitiesWithinAABBExcludingEntity(null, new AxisAlignedBB(side.getPos().add(-16, -16, -16), side.getPos().add(16, 16, 16)));
					BlockPos origin = side.getPos().offset(side.getSide().getOpposite());
					for (Entity entity : entities) {
						// Getting distance and direction of entity
						double height = entity.getEntityBoundingBox().maxY - entity.getEntityBoundingBox().minY;
						Vec3 toEntity = new Vec3(entity.posX - origin.getX() - 0.5, entity.posY + height / 2 - origin.getY() - 0.5, entity.posZ - origin.getZ() - 0.5);
						Vec3 facing = new Vec3(side.getSide().getFrontOffsetX(), side.getSide().getFrontOffsetY(), side.getSide().getFrontOffsetZ());
						double angle = Math.acos(Math.abs(toEntity.dotProduct(facing)) / Math.abs(toEntity.lengthVector() * facing.lengthVector()));
						if (toEntity.dotProduct(facing) < 0)
							angle += Math.signum(angle) * Math.PI / 2;
						if (angle < Math.PI / 6 && toEntity.lengthVector() < 16) {
							// Accelerating entity
							Vec3 direction = toEntity.normalize();
							double speed = Math.exp(-0.5 * toEntity.lengthVector() - 0.5) * 0.035 * PressureCircuit.PACKET_DELAY * loss;
							if (loss < 0)
								speed *= 2;
							entity.motionX += direction.xCoord * speed;
							entity.motionY += direction.yCoord * speed;
							entity.motionZ += direction.zCoord * speed;
						}
					}
				}
			}

			// Losing all volume
			this.airVolume = this.getVolume();
		}
	}
}
