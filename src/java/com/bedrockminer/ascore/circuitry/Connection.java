package com.bedrockminer.ascore.circuitry;

import java.util.HashSet;
import java.util.Set;

import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

/**
 * CircuitElements connect themselves via Connections. A Connection is the link
 * between the blocks and TileEntities and the Circuits. Depending on the type,
 * connections do different things and store different data.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class Connection {

	/** The Connection's ID. This must be unique for the enclosing TE. */
	private final byte ID;
	/** The TileEntity controlling the Connection */
	private final TileCircuitElement te;
	/**
	 * If this is <code>true</code>, the Connection will be treated as a passive
	 * Connection. Passive connections never change their values on their own.
	 * An example for passive connections are pipes, while most machines are
	 * active connections.
	 */
	private final boolean passive;
	/**
	 * If <code>true</code>, the Connection can only connect to selected
	 * positions. Otherwise, it will connect to all available positions.
	 */
	private boolean configurable;

	/** The connection's color */
	private EnumDyeColor color = EnumDyeColor.BLACK;
	/** The circuit this connection is part of */
	private Circuit circuit;

	/** A set of block sides this connection can use */
	private Set<BlockSide> availablePositions;
	/** A set of block sides this connection uses */
	private Set<BlockSide> selectedPositions;
	/** If <code>true</code>, the selected positions will be validated */
	private boolean selectedValidatonNeeded;

	/** The last time this Connection was updated */
	private long lastUpdateTick;

	/**
	 * Instantiates a new Connection and assigns an ID to it. The ID must be
	 * unique within the scope of the controlling TileEntity, persistent on
	 * restart of Minecraft and can be any value between 0 and 127. The passive
	 * value can make this connection a passive one. This should be set if the
	 * TileEntity will never change the values of the connection. This can be
	 * used for pipes or storages.
	 *
	 * @param ID
	 * the Connection's ID
	 * @param passive
	 * <code>true</code> if this connection is passive
	 * @param configurable
	 * <code>true</code> to allow changing selected connections instead of using
	 * all available connections.
	 * @param te
	 * the controlling TileEntity
	 */
	public Connection(int ID, boolean passive, boolean configurable, TileCircuitElement te) {
		this.ID = (byte) ID;
		this.passive = passive;
		this.configurable = configurable;
		this.te = te;
	}

	// Connection properties

	/**
	 * Returns the ID of this Connection.
	 *
	 * @return the ID
	 */
	public final byte getID() {
		return this.ID;
	}

	/**
	 * Returns the type of this connection.
	 *
	 * @return the ConnectionType
	 */
	public abstract ConnectionType getConnectionType();

	/**
	 * Returns the TileEntity controlling this Connection.
	 *
	 * @return the TileEntity
	 */
	public final TileCircuitElement getTE() {
		return this.te;
	}

	/**
	 * Returns the color of this connection. Two connections will only connect
	 * if their colors match. Colors do match if they are the same or if either
	 * color is black.
	 *
	 * @return the Connection's color
	 */
	public EnumDyeColor getConnectionColor() {
		return this.color;
	}

	/**
	 * Returns whether this is a passive connection.
	 *
	 * @return <code>true</code> if passive
	 */
	public boolean isPassive() {
		return this.passive;
	}

	/**
	 * Returns whether this is a configurable connection.
	 *
	 * @return <code>true</code> if configurable
	 */
	public boolean isConfigurable() {
		return this.configurable;
	}

	/**
	 * Sets the color of this connection.
	 *
	 * @param color
	 * the color to set
	 */
	public void setColor(EnumDyeColor color) {
		this.color = color;
		if (this.hasCircuit())
			this.getCircuit().invalidate();
		this.getTE().markDirty();
		if (this.getTE().hasWorldObj())
			this.getTE().getWorld().markBlockForUpdate(this.getTE().getPos());
	}

	// Connection handling

	/**
	 * Returns the positions this Connection can use.
	 *
	 * @return all available positions
	 */
	public Set<BlockSide> getAvailablePositions() {
		if (this.isSetUp() && this.availablePositions == null)
			this.availablePositions = this.getTE().getPositionsForConnection(this);
		if (this.availablePositions != null)
			return this.availablePositions;
		return ImmutableSet.<BlockSide> of();
	}

	/**
	 * Invalidates the available positions. They will be reloaded.
	 */
	public void invalidateAvailablePositions() {
		this.availablePositions = null;
	}

	/**
	 * Returns the positions this Connection uses.
	 *
	 * @return all used positions
	 */
	public Set<BlockSide> getSelectedPositions() {
		if (!this.isConfigurable())
			return this.getAvailablePositions();
		if (this.selectedValidatonNeeded) {
			if (this.isSetUp())
				this.selectedValidatonNeeded = false;
			this.setSelectedPositions(this.selectedPositions);
		}
		if (this.getAvailablePositions().size() > 0 && this.selectedPositions == null)
			this.selectedPositions = ImmutableSet.copyOf(this.availablePositions);
		if (this.selectedPositions != null)
			return this.selectedPositions;
		return ImmutableSet.<BlockSide> of();
	}

	/**
	 * Sets the positions this Connection uses. Only positions that are also
	 * included in the available positions will be added.
	 *
	 * @param positions
	 * the positions that this connection can use.
	 */
	public void setSelectedPositions(Set<BlockSide> positions) {
		this.selectedPositions = Sets.intersection(positions, this.getAvailablePositions());
		if (this.hasCircuit())
			this.getCircuit().invalidate();
		this.getTE().markDirty();
		if (this.getTE().hasWorldObj())
			this.getTE().getWorld().markBlockForUpdate(this.getTE().getPos());
	}

	/**
	 * Tests for blocks and TileEntities in the world to find out whether a
	 * connection to the given side is possible.
	 *
	 * @param position
	 * the position to check from.
	 * @return <code>true</code> if a connection from the position is possible
	 */
	public boolean canConnectFrom(BlockSide position) {
		// Check if position is valid and world access is given
		if (!this.getSelectedPositions().contains(position) || !this.isSetUp())
			return false;

		BlockSide look = position.getOpposite();
		Block block = this.getTE().getWorld().getBlockState(look.getPos()).getBlock();
		if (block instanceof ICircuitElement) {
			TileCircuitElement te = ((ICircuitElement) block).getTE(this.getTE().getWorld(), look.getPos());
			if (te != null) {
				return te.getConnection(look, this.getConnectionType(), this.getConnectionColor()) != null;
			}
		}
		return false;
	}

	/**
	 * Returns every position this Connection connects to. When overriding this
	 * method remember to call
	 * {@code getTE().reviewConnections(this, positions)} to allow the
	 * TileEntity to change something.
	 *
	 * @return a set of positions this Connection connects to
	 */
	public Set<BlockSide> getConnectedPositions() {
		Set<BlockSide> positions = new HashSet<BlockSide>();
		for (BlockSide position : this.getSelectedPositions())
			if (this.canConnectFrom(position))
				positions.add(position);
		this.getTE().reviewConnections(this, positions);
		return positions;
	}

	// Circuit handling

	/**
	 * Returns <code>true</code> if the Connection and its TileEntity are ready
	 * to be added to a circuit.
	 *
	 * @return <code>true</code> if ready for a circuit
	 */
	public boolean isSetUp() {
		return this.getTE().hasWorldObj();
	}

	/**
	 * This method needs to store data from the circuit in variables in this
	 * Connection so that the circuit can be re-assembled in the exact same
	 * state. This method should store data only if a circuit exist (
	 * {@link #hasCircuit()} returns <code>true</code>), otherwise the values
	 * should not be changed. The data stored here is the data normally loaded
	 * from NBT data.
	 * <p>
	 * Example:<br>
	 * A Pressure connection stores the amount of air inside of it. The circuit
	 * can count the blocks and add the air volumes up to get the amount of air
	 * in the whole circuit. Thus, the circuit is in the same state as before.
	 */
	public void storeCircuitData() {
	}

	/**
	 * Returns the Circuit this Connection is part of. If no Circuit exists or
	 * if the existing Circuit is invalid, a new Circuit will be assembled.
	 *
	 * @return the circuit
	 */
	public Circuit getCircuit() {
		if (!this.hasCircuit() && this.isSetUp() && !this.getTE().getWorld().isRemote)
			this.assembleCircuit();
		return this.circuit;
	}

	/**
	 * Returns whether the connection has a valid circuit.
	 *
	 * @return <code>true</code> if a valid circuit exists.
	 */
	public boolean hasCircuit() {
		return this.isSetUp() && !this.getTE().getWorld().isRemote && this.circuit != null && this.circuit.isValid();
	}

	/**
	 * Sets the circuit of this Connection. This is called when a circuit is
	 * assembled.
	 *
	 * @param circuit
	 * the circuit
	 */
	void setCircuit(Circuit circuit) {
		this.circuit = circuit;
	}

	/**
	 * Assembles a new circuit. The old circuit is invalidated.
	 */
	public void assembleCircuit() {
		if (this.hasCircuit())
			this.getCircuit().invalidate();
		if (this.isSetUp() && !this.getTE().getWorld().isRemote)
			this.circuit = this.createCircuit();
	}

	/**
	 * This method creates a new Circuit of the same type as the connection. The
	 * Circuit created should automatically assemble itself and load all
	 * relevant data.
	 *
	 * @return the created and assembled Circuit
	 */
	protected abstract Circuit createCircuit();

	// Update handling

	/**
	 * Returns whether the Connection can be updated this tick.
	 *
	 * @return <code>true</code> if an update is possible
	 */
	public boolean canUpdate() {
		return this.isSetUp() && this.getTE().getWorld().getTotalWorldTime() > this.lastUpdateTick;
	}

	/**
	 * If this method is called, the Connection will not be updated a second
	 * time this tick.
	 */
	public void markUpdated() {
		if (this.isSetUp())
			this.lastUpdateTick = this.getTE().getWorld().getTotalWorldTime();
	}

	/**
	 * Updates the Connection if necessary. If the Connection has been updated
	 * already, nothing happens.
	 */
	public void tryUpdate() {
		if (this.canUpdate())
			this.doUpdate();
	}

	/**
	 * Executes the update for the Connection. This should not be called
	 * manually, use {@link #tryUpdate()} instead to avoid multiple update calls
	 * per tick. This method updates the TileEntity first, then the Circuit.
	 */
	protected void doUpdate() {
		this.markUpdated();
		if (!this.getTE().getWorld().isRemote) {
			this.getTE().tryUpdate();
			this.getCircuit().tryUpdate();
		}
	}

	// Saving NBT Data

	/**
	 * Writes the data of this connection to the NBTTagCompound.
	 *
	 * @param nbt
	 * the nbt data
	 */
	public void writeToNBT(NBTTagCompound nbt) {
		nbt.setByte("Color", (byte) this.color.getDyeDamage());

		if (this.isConfigurable())
			nbt.setTag("SelectedPositions", writeBlockSideSet(this.getSelectedPositions()));
	}

	/**
	 * Reads the data of this connection from the NBTTagCompound.
	 *
	 * @param nbt
	 * the nbt data
	 */
	public void readFromNBT(NBTTagCompound nbt) {
		byte color = nbt.getByte("Color");
		this.color = color < 0 || color > 15 ? EnumDyeColor.BLACK : EnumDyeColor.byDyeDamage(color);

		if (this.isConfigurable() && nbt.hasKey("SelectedPositions")) {
			Set<BlockSide> positions = readBlockSideSet(nbt.getTagList("SelectedPositions", 10));
			// If the TE has no world object (while loading) the validation
			// does not work.
			if (this.isSetUp()) {
				this.setSelectedPositions(positions);
			} else {
				this.selectedPositions = positions;
				this.selectedValidatonNeeded = true;
			}
		}
	}

	/**
	 * Writes important data that needs to be sent to the client.
	 *
	 * @param nbt
	 * the nbt data
	 */
	public void writeToNBTForSync(NBTTagCompound nbt) {
		nbt.setByte("Color", (byte) this.color.getDyeDamage());

		if (this.isConfigurable())
			nbt.setTag("SelectedPositions", writeBlockSideSet(this.getSelectedPositions()));
	}

	/**
	 * Reads data that was sent from the server.
	 *
	 * @param nbt
	 * the nbt data
	 */
	public void readFromNBTForSync(NBTTagCompound nbt) {
		byte color = nbt.getByte("Color");
		this.color = color < 0 || color > 15 ? EnumDyeColor.BLACK : EnumDyeColor.byDyeDamage(color);

		if (this.isConfigurable() && nbt.hasKey("SelectedPositions"))
			this.setSelectedPositions(readBlockSideSet(nbt.getTagList("SelectedPositions", 10)));
	}

	/**
	 * Writes the state - especially the possible positions - to the nbt
	 * compound to synchronize the state to a settings gui on client side.
	 *
	 * @param nbt
	 * the nbt tag
	 */
	public void writeToNBTForSettingsGUI(NBTTagCompound nbt) {
		nbt.setTag("Available", writeBlockSideSet(this.getAvailablePositions()));
		nbt.setTag("Selected", writeBlockSideSet(this.getSelectedPositions()));

		nbt.setByte("Color", (byte) this.getConnectionColor().getDyeDamage());
		nbt.setInteger("Type", this.getConnectionType().getID());
	}

	/**
	 * Reads the possible positions and the color from the nbt compound and
	 * changes the state of the connection if necessary.
	 *
	 * @param nbt
	 */
	public void readFromNBTForSettingsGUI(NBTTagCompound nbt) {
		this.setSelectedPositions(readBlockSideSet(nbt.getTagList("Selected", 10)));

		int color = nbt.getByte("Color");
		if (color < 0 || color > 15)
			color = 0;
		this.setColor(EnumDyeColor.byDyeDamage(color));
	}

	// Java object methods

	@Override
	public int hashCode() {
		return this.getTE().getPos().hashCode() + 31 * this.getID();
	}

	@Override
	public String toString() {
		return String.format("Connection {ID=%s, type=%s, color:%s, TE at: %s}", this.getID(), this.getConnectionType().getIdentifier(), this.getConnectionColor(), this.getTE().getPos());
	}

	// Static methods

	public static boolean doColorsMatch(EnumDyeColor A, EnumDyeColor B) {
		return A == EnumDyeColor.BLACK || B == EnumDyeColor.BLACK || A == B;
	}

	public static NBTTagList writeBlockSideSet(Set<BlockSide> set) {
		NBTTagList nbt = new NBTTagList();
		for (BlockSide position : set) {
			NBTTagCompound data = new NBTTagCompound();
			data.setInteger("x", position.getPos().getX());
			data.setInteger("y", position.getPos().getY());
			data.setInteger("z", position.getPos().getZ());
			data.setByte("Side", (byte) position.getSide().getIndex());
			nbt.appendTag(data);
		}
		return nbt;
	}

	public static HashSet<BlockSide> readBlockSideSet(NBTTagList nbt) {
		HashSet<BlockSide> set = new HashSet<BlockSide>();
		for (int i = 0; i < nbt.tagCount(); i++) {
			NBTTagCompound data = nbt.getCompoundTagAt(i);
			int side = data.getByte("Side");
			if (side < 0 || side > 5)
				side = 0;
			set.add(new BlockSide(new BlockPos(data.getInteger("x"), data.getInteger("y"), data.getInteger("z")), EnumFacing.getFront(side)));
		}
		return set;
	}
}
