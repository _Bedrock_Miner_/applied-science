package com.bedrockminer.ascore.circuitry;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.config.Cfg;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.bedrockminer.ascore.util.Log;

import net.minecraft.block.Block;
import net.minecraft.world.World;

/**
 * Connections of multiple devices are handled by Circuits. A Circuit caches all
 * necessary data about the connections involved in the Circuit. With the cached
 * data the Circuit can calculate the behavior of the components.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class Circuit {

	/** The circuit can only be used if this value is <code>true</code> */
	private boolean valid;

	/** The last tick this circuit was updated */
	private long lastUpdateTick;

	/** The amount of connections used in this circuit */
	private int connections;
	/** The amount of active connections used in this circuit */
	private int activeConnections;
	/** The amount if leaks in this circuit */
	private int leaks;

	/** The world */
	private World world;

	/**
	 * Instantiates a new Circuit and assembles it.
	 *
	 * @param world
	 * the world
	 * @param start
	 * the start position for the Circuit assembly
	 */
	public Circuit(World world, Connection start) {
		this.world = world;
		this.assembleCircuit(start);
	}

	// Circuit properties

	/**
	 * Returns the world object.
	 *
	 * @return the world
	 */
	public World getWorld() {
		return this.world;
	}

	/**
	 * Returns whether the Circuit is valid. If this value is <code>false</code>
	 * , the Circuit cannot be used anymore.
	 *
	 * @return <code>true</code> if the Circuit is valid.
	 */
	public boolean isValid() {
		return this.valid;
	}

	/**
	 * Invalidates the Circuit. It can no longer be used afterwards and will be
	 * reassembled.
	 */
	public void invalidate() {
		for (Connection c: this.getAllConnections())
			c.storeCircuitData();
		this.valid = false;
	}

	/**
	 * Returns the amount of circuit elements that have been found.
	 *
	 * @return the amount of elements
	 */
	public int getElementCount() {
		return this.connections;
	}

	/**
	 * Returns the amount of active circuit elements that have been found.
	 *
	 * @return the amount of active elements
	 */
	public int getMachineCount() {
		return this.activeConnections;
	}

	/**
	 * Returns the connection type of this Circuit.
	 *
	 * @return the connection type
	 */
	public abstract ConnectionType getCircuitType();

	/**
	 * Returns all connections this circuit uses.
	 *
	 * @return all connections
	 */
	public abstract Set<Connection> getAllConnections();

	// Circuit assembly

	/**
	 * Assembles the Circuit. This method first calls
	 * {@link #searchBlocks(BlockSide)}, then {@link #finishAssembly()}. It is
	 * automatically called when the Circuit is instantiated.
	 *
	 * @param start
	 */
	private final void assembleCircuit(Connection start) {
		// Setting up assembly analysis
		long before = System.nanoTime();
		this.leaks = 0;
		this.connections = 1;
		this.activeConnections = start.isPassive() ? 0 : 1;

		this.startAssembly();
		this.searchBlocks(start);
		this.finishAssembly();

		// Evaluating assembly process
		long after = System.nanoTime();
		double ms = (after - before) / 1000 / 1000.0;

		String msg = String.format("Assembling %s at %s took %s ms. %s connection(s) assembled, %s of those active. %s leak(s) found.", this.getClass().getSimpleName(), start.getTE().getPos(), ms, this.connections, this.activeConnections, this.leaks);

		if (ms > Cfg.circuitAssemblyWarn.getDouble())
			Log.warn(msg);
		else
			Log.debug(msg);

		this.valid = true;
	}

	protected void searchBlocks(Connection start) {
		Set<Connection> evaluated = new HashSet<Connection>();
		Set<BlockSide> foundLeaks = new HashSet<BlockSide>();
		Queue<Connection> toEvaluate = new LinkedList<Connection>();

		toEvaluate.add(start);
		evaluated.add(start);
		start.getTE().onAddingToCircuit(start);
		this.foundConnection(start);
		start.storeCircuitData();
		start.setCircuit(this);

		while (toEvaluate.size() > 0) {
			Connection connection = toEvaluate.poll();

			Set<BlockSide> sides = connection.getConnectedPositions();

			for (BlockSide side: sides) {
				Block block = this.getWorld().getBlockState(side.getOpposite().getPos()).getBlock();
				if (block instanceof ICircuitElement) {
					TileCircuitElement te = ((ICircuitElement) block).getTE(this.getWorld(), side.getOpposite().getPos());
					if (te != null) {
						Connection c = te.getConnection(side.getOpposite(), this.getCircuitType(), connection.getConnectionColor());
						if (c != null) {
							if (!evaluated.contains(c)) {
								te.onAddingToCircuit(c);
								this.foundConnection(c);
								toEvaluate.add(c);
								evaluated.add(c);
								c.storeCircuitData();
								c.setCircuit(this);

								this.connections++;
								if (!c.isPassive())
									this.activeConnections++;

							}
							// Finished, next one!
							continue;
						}
					}
				}
				// We found a leak if everything else failed
				// (if continue isn't called)
				if (!foundLeaks.contains(side)) {
					this.foundLeak(side);
					this.leaks++;
					foundLeaks.add(side);
				}
			}
		}
	}

	/**
	 * Called at the begin of the assembly procedure. Use this to create
	 * arrays or lists or prepare other important things.
	 */
	protected abstract void startAssembly();

	/**
	 * Called when a Connection is found during assembly. Use this method to
	 * cache the found connection.
	 *
	 * @param connection
	 * the connection
	 */
	protected abstract void foundConnection(Connection connection);

	/**
	 * Called when a leak is found during assembly. Use this method to cache the
	 * found leak.
	 *
	 * @param position
	 * the position of the leak
	 */
	protected abstract void foundLeak(BlockSide position);

	/**
	 * Called after all blocks of the connection have been examined. Use this to
	 * calculate and cache important information about the circuit, like the
	 * amount of pipes or similar.
	 */
	protected abstract void finishAssembly();

	// Update Handling

	/**
	 * Returns whether the Circuit can be updated this tick.
	 *
	 * @return <code>true</code> if an update is possible
	 */
	public boolean canUpdate() {
		return this.isValid() && this.getWorld().getTotalWorldTime() > this.lastUpdateTick;
	}

	/**
	 * If this method is called, the Circuit will not be updated a second time
	 * this tick.
	 */
	public void markUpdated() {
		this.lastUpdateTick = this.getWorld().getTotalWorldTime();
	}

	/**
	 * Updates the Circuit if necessary. If the Circuit has been updated
	 * already, nothing happens.
	 */
	public void tryUpdate() {
		if (this.canUpdate())
			this.doUpdate();
	}

	/**
	 * Executes the update for the Circuit. This should not be called manually,
	 * use {@link #tryUpdate()} instead to avoid multiple update calls per tick.
	 */
	protected void doUpdate() {
		this.markUpdated();
	}
}
