package com.bedrockminer.ascore.modintegration.waila;

import java.util.List;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Optional;

/**
 * Add this interface to a block class to specify display data for Waila.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public interface IWailaBlock {

	/**
	 * Fills the main body of the Waila tooltip with data available to both
	 * client and server side. This method is protected with the @Optional
	 * annotation so that you don't have to worry about using it without Waila
	 * installed.
	 *
	 * @param stack
	 * the displayed itemstack
	 * @param tooltip
	 * the tooltip
	 * @param accessor
	 * the data accessor
	 * @param config
	 * the Waila config
	 * @param nbt
	 * <code>true</code> if nbt data is available
	 */
	@Optional.Method(modid = "Waila")
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt);

	/**
	 * Returns whether the block needs synchronized NBT data.
	 * If this is <code>true</code> and no NBT is available, this will be
	 * displayed in the tooltip.
	 * If this returns <code>true</code>, the TileEntity needs to implement
	 * {@link IWailaNBT} of course.
	 *
	 * @return <code>true</code> if this block needs NBT data.
	 */
	public boolean usesNBTData();
}
