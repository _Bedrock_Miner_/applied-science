package com.bedrockminer.ascore.modintegration.waila;

import net.minecraft.nbt.NBTTagCompound;

/**
 * Add this interface to your TileEntity to enable special NBTData being passed
 * to Waila.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public interface IWailaNBT {

	/**
	 * Writes the data that should be synced to the NBTTagCompound.
	 *
	 * @param nbt the nbt tag
	 */
	public void writeToNBTForWaila(NBTTagCompound nbt);

}
