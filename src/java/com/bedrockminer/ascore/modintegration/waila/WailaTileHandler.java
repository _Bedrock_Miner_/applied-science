package com.bedrockminer.ascore.modintegration.waila;

import java.util.Arrays;
import java.util.List;

import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.tileentity.TileInventoryReference;

import mcp.mobius.waila.Waila;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.block.Block;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.Optional;

@Optional.Interface(iface = "mcp.mobius.waila.api.IWailaDataProvider", modid = "Waila")
public class WailaTileHandler implements IWailaDataProvider {

	@Optional.Method(modid = "Waila")
	public static void registerWailaData(IWailaRegistrar registrar) {
		WailaTileHandler instance = new WailaTileHandler();

		registrar.registerNBTProvider(instance, IWailaNBT.class);
		registrar.registerNBTProvider(instance, TileInventoryReference.class);

		registrar.registerBodyProvider(instance, IWailaBlock.class);
		registrar.registerTailProvider(instance, IWailaBlock.class);
	}

	@Override
	@Optional.Method(modid = "Waila")
	public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return accessor.getStack();
	}

	@Override
	@Optional.Method(modid = "Waila")
	public List<String> getWailaHead(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		return tooltip;
	}

	@Override
	@Optional.Method(modid = "Waila")
	public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound nbt, World world, BlockPos pos) {
		Block block = world.getBlockState(pos).getBlock();
		if (block instanceof ICircuitElement)
			te = ((ICircuitElement) block).getTE(world, pos);

		if (te != null && te instanceof IWailaNBT) {
			((IWailaNBT) te).writeToNBTForWaila(nbt);
			nbt.setBoolean("sync_successful", true);
		}
		return nbt;
	}

	@Override
	@Optional.Method(modid = "Waila")
	public List<String> getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		if (accessor.getBlock() instanceof IWailaBlock) {
			boolean nbt = accessor.getNBTData() != null && accessor.getNBTData().getBoolean("sync_successful");

			((IWailaBlock)accessor.getBlock()).getWailaBody(stack, tooltip, accessor, config, nbt);

			if (((IWailaBlock)accessor.getBlock()).usesNBTData() && Waila.instance.serverPresent && !nbt)
				tooltip.add(I18n.format("tooltip.waila.sync"));
		}
		return tooltip;
	}

	@Override
	@Optional.Method(modid = "Waila")
	public List<String> getWailaTail(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
		if (accessor.getBlock() instanceof IWailaBlock) {
		if (!Waila.instance.serverPresent && ((IWailaBlock) accessor.getBlock()).usesNBTData())
			tooltip.addAll(0, Arrays.asList(I18n.format("tooltip.waila.noserver").split("\\\\n")));
		}
		return tooltip;
	}
}
