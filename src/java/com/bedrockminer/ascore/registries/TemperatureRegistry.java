package com.bedrockminer.ascore.registries;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;

/**
 * Some blocks have a special temperature. This class is used to register these.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TemperatureRegistry {

	private static Map<Pair<Block, Integer>, Double> blocks = new HashMap<Pair<Block, Integer>, Double>();
	private static Map<BiomeGenBase, Double> biomes = new HashMap<BiomeGenBase, Double>();
	private static Map<Pair<Item, Integer>, Double> burnItems = new HashMap<Pair<Item, Integer>, Double>();

	// Registration

	/**
	 * Registers the temperature for a block with its metadata.
	 *
	 * @param block the block
	 * @param meta the metadata
	 * @param temperature the temperature in Celsius
	 */
	public static void putBlockTemperature(Block block, int meta, double temperature) {
		blocks.put(Pair.of(block, Integer.valueOf(meta)), temperature);
	}

	/**
	 * Registers the temperature for a block with all its subtypes.
	 *
	 * @param block the block
	 * @param temperature the temperature in Celsius
	 */
	public static void putBlockTemperature(Block block, double temperature) {
		for (int i = 0; i < 16; i++)
			putBlockTemperature(block, i, temperature);
	}

	/**
	 * Removes the mapping for a block
	 *
	 * @param block the block
	 * @param meta the metadata
	 */
	public static void removeBlock(Block block, int meta) {
		blocks.remove(Pair.of(block, Integer.valueOf(meta)));
	}

	/**
	 * Registers the temperature for a biome
	 *
	 * @param biome the biome
	 * @param temperature the temperature
	 */
	public static void putBiomeTemperature(BiomeGenBase biome, double temperature) {
		biomes.put(biome, temperature);
	}

	/**
	 * Removes the mapping for a biome
	 *
	 * @param biome the biome
	 */
	public static void removeBiome(BiomeGenBase biome) {
		biomes.remove(biome);
	}

	/**
	 * Registers the temperature for burning an itemstack
	 *
	 * @param stack the item
	 * @param temperature the temperature
	 */
	public static void putItemBurnTemp(ItemStack stack, double temperature) {
		burnItems.put(Pair.of(stack.getItem(), Integer.valueOf(stack.getItemDamage())), temperature);
	}

	/**
	 * Registers the temperature for burning an item
	 *
	 * @param item the item
	 * @param temperature the temperature
	 */
	public static void putItemBurnTemp(Item item, double temperature) {
		putItemBurnTemp(new ItemStack(item), temperature);
	}

	/**
	 * Removes the mapping for an itemstack
	 *
	 * @param stack the stack
	 */
	public static void removeItem(ItemStack stack) {
		burnItems.remove(Pair.of(stack.getItem(), Integer.valueOf(stack.getItemDamage())));
	}

	// Getter

	/**
	 * Returns the block temperature for the given position in Celsius.
	 *
	 * @param world the world
	 * @param pos the position
	 * @return the temperature in Celsius
	 */
	public static double getTemperature(World world, BlockPos pos) {
		Block block = world.getBlockState(pos).getBlock();
		int meta = block.getMetaFromState(world.getBlockState(pos));
		Pair<Block, Integer> pair = Pair.of(block, Integer.valueOf(meta));
		if (blocks.containsKey(pair))
			return blocks.get(pair);
		return getAmbientTemperature(world, pos);
	}

	/**
	 * Returns the temperature for a specific block type in Celsius
	 *
	 * @param block the block
	 * @param meta the metadata
	 * @return the temperature in Celsius
	 */
	public static double getTemperature(Block block, int meta) {
		Pair<Block, Integer> pair = Pair.of(block, Integer.valueOf(meta));
		if (blocks.containsKey(pair))
			return blocks.get(pair);
		return getAmbientTemperature();
	}

	/**
	 * Returns the default temperature that is used if nothing else matches.
	 *
	 * @return the ambient temperature
	 */
	public static double getAmbientTemperature() {
		return 18.0;
	}

	/**
	 * Returns the ambient world temperature based on the biomes.
	 *
	 * @param world the world
	 * @param pos the position
	 * @return the ambient temperature in Celsius
	 */
	public static double getAmbientTemperature(World world, BlockPos pos) {
		BiomeGenBase biome = world.getBiomeGenForCoords(pos);
		if (biomes.containsKey(biome))
			return biomes.get(biome);
		return 35.0 * biome.getFloatTemperature(pos) - 10.0;
	}

	/**
	 * Returns the temperature of a burning item.
	 *
	 * @param stack the stack
	 * @return the temperature
	 */
	public static double getItemBurnTemp(ItemStack stack) {
		Pair<Item, Integer> pair = Pair.of(stack.getItem(), Integer.valueOf(stack.getItem().getHasSubtypes() ? stack.getItemDamage() : 0));

		if (burnItems.containsKey(pair))
			return burnItems.get(pair);
		Item item = stack.getItem();
		Block block = Block.getBlockFromItem(item);
		if (block != null) {
			if (block == Blocks.wooden_slab || block.getMaterial() == Material.wood)
                return 600.0;
		}
		if (item instanceof ItemTool && ((ItemTool)item).getToolMaterialName().equals("WOOD")) return 600.0;
        if (item instanceof ItemSword && ((ItemSword)item).getToolMaterialName().equals("WOOD")) return 600.0;
        if (item instanceof ItemHoe && ((ItemHoe)item).getMaterialName().equals("WOOD")) return 600.0;

		return TileEntityFurnace.getItemBurnTime(stack) / 4.0 + 50.0;
	}

	// Registering Vanilla blocks

	public static void registerVanilla() {
		putBlockTemperature(Blocks.torch, 400.0);
		putBlockTemperature(Blocks.fire, 800.0);
		putBlockTemperature(Blocks.lava, 700.0);
		putBlockTemperature(Blocks.flowing_lava, 600.0);
		putBlockTemperature(Blocks.lit_furnace, 500.0);
		putBlockTemperature(Blocks.snow, 0.0);
		putBlockTemperature(Blocks.ice, -10.0);
		putBlockTemperature(Blocks.packed_ice, -15.0);

		putBiomeTemperature(BiomeGenBase.hell, 100.0);

		putItemBurnTemp(Item.getItemFromBlock(Blocks.coal_block), 1200.0);
		putItemBurnTemp(Items.coal, 1100.0);
		putItemBurnTemp(Items.blaze_rod, 1800.0);
		putItemBurnTemp(Items.lava_bucket, 800.0);
		putItemBurnTemp(Item.getItemFromBlock(Blocks.sapling), 200.0);
		putItemBurnTemp(Items.stick, 500.0);
	}
}
