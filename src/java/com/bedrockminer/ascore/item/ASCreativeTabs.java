package com.bedrockminer.ascore.item;

import com.bedrockminer.ascore.block.ASBlocks;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public final class ASCreativeTabs {

	/**
	 * The Applied Science Creative Tab with a pipe as it's icon. (May be
	 * changed in the near future as more items are added)
	 */
	public static final CreativeTabs tabAppliedScience = new CreativeTabs("applied_science") {
		@Override public Item getTabIconItem() {
			return Item.getItemFromBlock(ASBlocks.pressurePipe);
		}
	};
}
