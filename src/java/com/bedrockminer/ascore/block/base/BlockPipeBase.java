package com.bedrockminer.ascore.block.base;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.bedrockminer.ascore.block.ASMaterials;
import com.bedrockminer.ascore.block.itemblock.ItemBlockInterface.IAdditionalInformation;
import com.bedrockminer.ascore.block.itemblock.ItemBlockInterface.IItemColorMultiplier;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.google.common.collect.ImmutableSet;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BlockPipeBase extends BlockTileEntityProvider implements
		ICircuitElement, IItemColorMultiplier, IAdditionalInformation {

	//@formatter:off
	/** The colors to use for the different dye colors */
	public static int[] dye_colors = new int[] {
			0x191919, 0x991010, 0x1f660f, 0x66411a, 0x1132b3, 0x8040b3, 0x328b99, 0x999999,
			0x4d4d4d, 0xf280a6, 0x80cc1a, 0xe5e533, 0x6699d9, 0xc24dd9, 0xd06315, 0xffffff };
	//@formatter:on

	protected BlockPipeBase(String unlocalizedName) {
		super(ASMaterials.pipe);
		this.setUnlocalizedName(unlocalizedName);
	}

	// Basic block properties

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean isFullCube() {
		return false;
	}

	// Render properties

	@Override
	public EnumWorldBlockLayer getBlockLayer() {
		return EnumWorldBlockLayer.CUTOUT;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess world, BlockPos pos, int renderPass) {
		return dye_colors[this.getPipeColor(world, pos).getDyeDamage()];
	}

	@Override
	public int colorMultiplier(ItemStack stack, int renderPass) {
		int color = 0;
		if (stack.hasTagCompound()) {
			color = stack.getTagCompound().getInteger("color");
			if (color < 0 || color > 15)
				color = 0;
		}
		return stack.getItemDamage() < 16 ? dye_colors[color] : 0xffffff;
	}

	// Item properties

	@Override
	public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
		List<ItemStack> list = new LinkedList<ItemStack>();
		TilePipeBase te = this.getTE(world, pos);
		if (te != null) {
			ItemStack stack = new ItemStack(Item.getItemFromBlock(this));
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setByte("color", (byte) te.getPipeColor().getDyeDamage());
			stack.setTagCompound(nbt);
			list.add(stack);
		}
		return list;
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos, EntityPlayer player) {
		return this.getDrops(world, pos, world.getBlockState(pos), 0).get(0);
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip, boolean advanced) {
		EnumDyeColor color = EnumDyeColor.BLACK;
		if (stack.hasTagCompound()) {
			byte c = stack.getTagCompound().getByte("color");
			if (c < 0 || c > 15)
				c = 0;
			color = EnumDyeColor.byDyeDamage(c);
		}
		tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + (color == EnumDyeColor.BLACK ? "universal" : color.getUnlocalizedName()))));
	}

	@Override
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list) {
		ItemStack stack = new ItemStack(this);
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setByte("color", (byte) 0);
		stack.setTagCompound(nbt);
		list.add(stack);
	}

	// Collision box calculation

	//@formatter:off
	@Override
	public void addCollisionBoxesToList(World world, BlockPos pos, IBlockState state, AxisAlignedBB mask, List<AxisAlignedBB> list, Entity collidingEntity) {
		Set<EnumFacing> connections = this.getPipeConnections(world, pos);
		this.setBlockBounds(0.5f - this.getPipeRadius(), 0.5f - this.getPipeRadius(), 0.5f - this.getPipeRadius(), 0.5f + this.getPipeRadius(), 0.5f + this.getPipeRadius(), 0.5f + this.getPipeRadius());
		super.addCollisionBoxesToList(world, pos, state, mask, list, collidingEntity);

		for (EnumFacing facing : connections) {
			this.setBlockBounds(
					facing == EnumFacing.WEST 	? 0.0f : 0.5f - this.getPipeRadius(),
					facing == EnumFacing.DOWN 	? 0.0f : 0.5f - this.getPipeRadius(),
					facing == EnumFacing.NORTH 	? 0.0f : 0.5f - this.getPipeRadius(),
					facing == EnumFacing.EAST 	? 1.0f : 0.5f + this.getPipeRadius(),
					facing == EnumFacing.UP 	? 1.0f : 0.5f + this.getPipeRadius(),
					facing == EnumFacing.SOUTH 	? 1.0f : 0.5f + this.getPipeRadius());

			super.addCollisionBoxesToList(world, pos, state, mask, list, collidingEntity);
		}
		this.setBlockBounds(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, BlockPos pos) {
		Set<EnumFacing> c = this.getPipeConnections(world, pos);
		this.setBlockBounds(
				c.contains(EnumFacing.WEST) 	? 0.0f : 0.5f - this.getPipeRadius(),
				c.contains(EnumFacing.DOWN) 	? 0.0f : 0.5f - this.getPipeRadius(),
				c.contains(EnumFacing.NORTH) 	? 0.0f : 0.5f - this.getPipeRadius(),
				c.contains(EnumFacing.EAST) 	? 1.0f : 0.5f + this.getPipeRadius(),
				c.contains(EnumFacing.UP) 		? 1.0f : 0.5f + this.getPipeRadius(),
				c.contains(EnumFacing.SOUTH) 	? 1.0f : 0.5f + this.getPipeRadius());
	}
	//@formatter:on

	/**
	 * Returns the radius of this pipe. This is used to calculate the bounding
	 * box of the block.
	 *
	 * @return the pipe radius
	 */
	public abstract float getPipeRadius();

	// Tile Entity properties

	@Override
	public abstract TilePipeBase createNewTileEntity(World world, int meta);

	@Override
	public TilePipeBase getTE(IBlockAccess world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof TilePipeBase)
			return (TilePipeBase) te;
		return null;
	}

	public EnumDyeColor getPipeColor(IBlockAccess world, BlockPos pos) {
		TilePipeBase te = this.getTE(world, pos);
		if (te != null)
			return te.getPipeColor();
		return EnumDyeColor.BLACK;
	}

	public Set<EnumFacing> getPipeConnections(IBlockAccess world, BlockPos pos) {
		TilePipeBase te = this.getTE(world, pos);
		if (te != null)
			return te.getPipeConnections();
		return ImmutableSet.<EnumFacing>of();
	}

	// Pipe behavior

	/**
	 * If the player right-clicks a pipe with dye, the pipe's color is changed
	 * accordingly.
	 */
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() instanceof ItemDye) {
			TilePipeBase te = this.getTE(world, pos);
			if (te != null && te.getPipeColor() != EnumDyeColor.byDyeDamage(player.getCurrentEquippedItem().getItemDamage())) {
				te.setPipeColor(EnumDyeColor.byDyeDamage(player.getCurrentEquippedItem().getItemDamage()));
				if (!player.capabilities.isCreativeMode)
					player.getCurrentEquippedItem().stackSize--;
				return true;
			}
		}
		return false;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		TilePipeBase te = this.getTE(world, pos);
		if (te != null) {
			int color = 0;
			if (stack.hasTagCompound()) {
				color = stack.getTagCompound().getInteger("color");
				if (color < 0 || color > 15)
					color = 0;
			}
			te.setPipeColor(EnumDyeColor.byDyeDamage(color));
		}
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state) {
		this.getTE(world, pos).onBreakBlock();
		super.breakBlock(world, pos, state);
	}
}
