package com.bedrockminer.ascore.block.base;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * BlockTileEntityProvider is an advanced version of the vanilla BlockContainer.
 * It re-enables the default block rendering handling, as most TileEntities
 * still have use for it. Furthermore, it ensures the TileEntity is still intact
 * when the block drops its items to allow generating them with data from the
 * TE.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class BlockTileEntityProvider extends BlockContainer {

	public BlockPos renderPosition;

	protected BlockTileEntityProvider(Material materialIn) {
		super(materialIn);
	}

	public BlockTileEntityProvider(Material material, MapColor mapColor) {
		super(material, mapColor);
	}

	/**
	 * Re-enabling the default block rendering
	 */
	@Override
	public int getRenderType() {
		return 3;
	}

	/**
	 * Called to remove a block a player can harvest. This is blocked by
	 * this method, it only simulates that the block has been destroyed.
	 */
	@Override
	public boolean removedByPlayer(World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
		if (willHarvest)
			return true;
		return super.removedByPlayer(world, pos, player, willHarvest);
	}

	/**
	 * Called after the removedByPlayer method returned. This will actually
	 * destroy the block, the other method didn't. Thus, the harvesting method
	 * and the item drops are executed with the block still in place.
	 */
	@Override
	public void harvestBlock(World world, EntityPlayer player, BlockPos pos, IBlockState state, TileEntity te) {
		super.harvestBlock(world, player, pos, state, te);
		world.setBlockToAir(pos);
	}

	/**
	 * Stores the block's position when it is rendered using an ISmartBlockModel
	 */
	@Override
	public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos) {
		this.renderPosition = pos;
		return super.getExtendedState(state, world, pos);
	}
}
