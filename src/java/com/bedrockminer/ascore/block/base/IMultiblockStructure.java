package com.bedrockminer.ascore.block.base;

import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;

/**
 * Blocks that are part of a multiblock structure should implement this
 * interface. It is used by other classes like the TileInventoryReference
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public interface IMultiblockStructure {

	/**
	 * Returns the main block position of this structure.
	 *
	 * @param world the world
	 * @param pos the position
	 * @return the main block position
	 */
	public BlockPos getMainBlock(IBlockAccess world, BlockPos pos);
}
