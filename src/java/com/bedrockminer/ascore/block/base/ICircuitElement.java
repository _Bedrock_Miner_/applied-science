package com.bedrockminer.ascore.block.base;

import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;

import net.minecraft.block.BlockContainer;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;

/**
 * Blocks that can be part of a Circuit (like pipes or machines) need to
 * implement this interface. The blocks need to be subclasses of
 * {@link BlockContainer} and return their TileEntity from the given method if
 * this Interface. Furthermore, the block must call the TileEntity's method
 * {@link TileCircuitElement#onBreakBlock()} when the block is destroyed.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public interface ICircuitElement {

	/**
	 * This method needs to return the TileEntity that manages the connections
	 * for this block. The TileEntity does not necessarily need to be at the
	 * same position as the block, this can be used to create multiblock
	 * structures. This method can return <code>null</code> if, for some reason,
	 * no connections are available. Then the block will be treated as if it had
	 * not implemented this interface.
	 *
	 * @param world
	 * the world
	 * @param pos
	 * the block's position
	 * @return the TileEntity.
	 */
	public TileCircuitElement getTE(IBlockAccess world, BlockPos pos);

}
