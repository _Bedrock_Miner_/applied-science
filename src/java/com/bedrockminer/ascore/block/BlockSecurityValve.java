package com.bedrockminer.ascore.block;

import java.util.List;
import java.util.Set;

import com.bedrockminer.ascore.network.gui.ASGuiHandler;
import com.bedrockminer.ascore.network.gui.ASGuiHandler.GUI;
import com.bedrockminer.ascore.tileentity.TileSecurityValve;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * The security valve releases pressure if a certain treshold is reached.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class BlockSecurityValve extends BlockPressurePipe {

	public BlockSecurityValve(String unlocalizedName) {
		super(unlocalizedName);
	}

	// Tile Entity

	@Override
	public TilePipeBase createNewTileEntity(World world, int meta) {
		return new TileSecurityValve();
	}

	@Override
	public Set<EnumFacing> getPipeConnections(IBlockAccess world, BlockPos pos) {
		TileSecurityValve te = (TileSecurityValve) this.getTE(world, pos);
		if (te != null) {
			Axis orientation = te.getOrientation();
			return ImmutableSet.of(EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, orientation), EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, orientation));
		}
		return ImmutableSet.<EnumFacing>of();
	}

	// Behaviour

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (super.onBlockActivated(world, pos, state, player, side, hitX, hitY, hitZ)) {
			this.updateDirection(world, pos);
			return true;
		}

		if (!world.isRemote)
			ASGuiHandler.openGUI(player, GUI.SECURITY_VALVE, world, pos);
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(world, pos, state, placer, stack);
		this.updateDirection(world, pos);
	}

	public void updateDirection(IBlockAccess world, BlockPos pos) {
		TilePipeBase te = this.getTE(world, pos);
		if (te != null && te instanceof TileSecurityValve) {
			int[] connections = new int[3];
			int axis = ((TileSecurityValve) te).getOrientation().ordinal();

			for (EnumFacing facing : EnumFacing.values()) {
				//Set the orientation for the canConnectFrom method
				((TileSecurityValve)te).setOrientation(facing.getAxis());

				if (((TileSecurityValve)te).getConnection().canConnectFrom(new BlockSide(pos, facing)))
					connections[facing.getAxis().ordinal()]++;
			}
			int value = connections[axis];
			for (int i = 0; i < 3; i++) {
				if (connections[i] > value) {
					value = connections[i];
					axis = i;
				}
			}
			((TileSecurityValve)te).setOrientation(Axis.values()[axis]);
		}
	}

	@Override
	public void onNeighborBlockChange(World world, BlockPos pos, IBlockState state, Block neighborBlock) {
		this.updateDirection(world, pos);
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (this.getPipeColor(accessor.getWorld(), accessor.getPosition()) != EnumDyeColor.BLACK)
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + this.getPipeColor(accessor.getWorld(), accessor.getPosition()).getUnlocalizedName())));
		else
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color.universal")));

		if (nbt) {
			tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
			tooltip.add(I18n.format("container.security_valve.treshold", Units.formatFrom(accessor.getNBTData().getDouble("Treshold"), Pressure.BAR)));
		}
	}
}
