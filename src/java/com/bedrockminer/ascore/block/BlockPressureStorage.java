package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.block.base.BlockTileEntityProvider;
import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.item.ASCreativeTabs;
import com.bedrockminer.ascore.modintegration.waila.IWailaBlock;
import com.bedrockminer.ascore.tileentity.TilePressureStorage;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.units.Volume;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockPressureStorage extends BlockTileEntityProvider
		implements ICircuitElement, IWailaBlock {

	public BlockPressureStorage(String unlocalizedName) {
		super(ASMaterials.machine);
		this.setUnlocalizedName(unlocalizedName);
		this.setHarvestLevel("pickaxe", 2);
		this.setHardness(3.0f);
		this.setResistance(20.0f);
		this.setCreativeTab(ASCreativeTabs.tabAppliedScience);
	}

	// Basic block properties

	@Override
	public EnumWorldBlockLayer getBlockLayer() {
		return EnumWorldBlockLayer.CUTOUT;
	}

	@Override
	public boolean isBlockSolid(IBlockAccess worldIn, BlockPos pos, EnumFacing side) {
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean isFullCube() {
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess world, BlockPos pos, EnumFacing side) {
		return !(world.getBlockState(pos).getBlock() instanceof BlockPressureStorage);
	}

	// Tile Entity and Blockstate

	@Override
	public TileCircuitElement createNewTileEntity(World worldIn, int meta) {
		return new TilePressureStorage();
	}

	@Override
	public TilePressureStorage getTE(IBlockAccess world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof TilePressureStorage)
			return (TilePressureStorage) te;
		return null;
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state) {
		this.getTE(world, pos).onBreakBlock();
		super.breakBlock(world, pos, state);
	}

	// Waila

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		TilePressureStorage te = this.getTE(accessor.getWorld(), accessor.getPosition());

		if (te != null && nbt) {
			tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
			tooltip.add(I18n.format("tooltip.waila.pressure_storage.volume", Units.formatFrom(1, Volume.CUBICMETER)));
		}
	}

	@Override
	public boolean usesNBTData() {
		return true;
	}
}
