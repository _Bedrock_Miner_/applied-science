package com.bedrockminer.ascore.block.itemblock;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

/**
 * This class provides several special features to blocks that are normally only
 * available for Items. To use it, simply register the block with this class
 * as ItemBlock and implement the interfaces from this class.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ItemBlockInterface extends ItemBlock {

	public ItemBlockInterface(Block block) {
		super(block);

		if (block instanceof ISubtypes) {
			this.setMaxDamage(0);
			this.setHasSubtypes(true);
		}
	}

	@Override
	public int getColorFromItemStack(ItemStack stack, int renderPass) {
		if (this.block instanceof IItemColorMultiplier)
			return ((IItemColorMultiplier)this.getBlock()).colorMultiplier(stack, renderPass);
		return 0xffffff;
	}

	@Override
	public int getMetadata(int damage) {
		return (this.block instanceof ISubtypes) ? damage : 0;
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip, boolean advanced) {
		if (this.block instanceof IAdditionalInformation)
			((IAdditionalInformation) this.block).addInformation(stack, player, tooltip, advanced);
	}

	/**
	 * Implement this interface to add a color multiplier for the item form of
	 * the block.
	 */
	public static interface IItemColorMultiplier {
		public int colorMultiplier(ItemStack stack, int renderPass);
	}

	/**
	 * Implement this interface to add additional tooltips to the item.
	 */
	public static interface IAdditionalInformation {
		public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip, boolean advanced);
	}

	/**
	 * Implement this interface to enable metadata for the items.
	 */
	public static interface ISubtypes {}
}
