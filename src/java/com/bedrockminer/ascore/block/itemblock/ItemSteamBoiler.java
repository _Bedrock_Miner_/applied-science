package com.bedrockminer.ascore.block.itemblock;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.block.BlockSteamBoiler;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class ItemSteamBoiler extends ItemBlockInterface {

	public ItemSteamBoiler(Block block) {
		super(block);
	}

	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (world.isRemote) {
			return true;
		} else if (side != EnumFacing.UP) {
			return false;
		} else {
			if (!world.getBlockState(pos).getBlock().isReplaceable(world, pos))
				pos = pos.up();

			EnumFacing direction = EnumFacing.getHorizontal(MathHelper.floor_double(player.rotationYaw * 4.0F / 360.0F + 0.5D) & 3);
			BlockPos back = pos.offset(direction);

			if (player.canPlayerEdit(pos, side, stack) && player.canPlayerEdit(back, side, stack)) {
				boolean canPlaceFront = world.getBlockState(pos).getBlock().isReplaceable(world, pos) || world.isAirBlock(pos);
				boolean canPlaceBack = world.getBlockState(back).getBlock().isReplaceable(world, back) || world.isAirBlock(back);

				if (canPlaceFront && canPlaceBack) {
					IBlockState frontState = ASBlocks.steamBoiler.getDefaultState().withProperty(BlockSteamBoiler.FRONT, true).withProperty(BlockSteamBoiler.FACING, direction);

					if (world.setBlockState(pos, frontState)) {
						IBlockState backState = frontState.withProperty(BlockSteamBoiler.FRONT, false);
						world.setBlockState(back, backState);
						world.playSoundEffect(pos.getX() + 0.5F, pos.getY() + 0.5F, pos.getZ() + 0.5F, this.block.stepSound.getPlaceSound(), (this.block.stepSound.getVolume() + 1.0F) / 2.0F, this.block.stepSound.getFrequency() * 0.8F);
						this.block.onBlockPlacedBy(world, pos, frontState, player, stack);
						this.block.onBlockPlacedBy(world, back, backState, player, stack);
					}

					--stack.stackSize;
					return true;
				}
			}

			return false;
		}
	}

}
