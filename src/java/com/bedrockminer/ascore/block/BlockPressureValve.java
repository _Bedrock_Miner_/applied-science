package com.bedrockminer.ascore.block;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.bedrockminer.ascore.tileentity.TilePressureValve;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.units.Volume;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * The pressure valve can be used to cut off a pressure circuit at some point.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class BlockPressureValve extends BlockPressurePipe {

	public BlockPressureValve(String unlocalizedName) {
		super(unlocalizedName);
	}

	// Tile Entity

	@Override
	public TilePipeBase createNewTileEntity(World world, int meta) {
		return new TilePressureValve();
	}

	@Override
	public Set<EnumFacing> getPipeConnections(IBlockAccess world, BlockPos pos) {
		TilePressureValve te = (TilePressureValve) this.getTE(world, pos);
		if (te != null) {
			Set<BlockSide> positions = te.getConnection().getSelectedPositions();
			Set<EnumFacing> sides = new HashSet<EnumFacing>();
			for (BlockSide bs: positions)
				sides.add(bs.getSide());

			return sides;
		}
		return ImmutableSet.<EnumFacing>of();
	}

	// Behaviour

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (super.onBlockActivated(world, pos, state, player, side, hitX, hitY, hitZ)) {
			this.updateDirection(world, pos);
			return true;
		}

		TilePipeBase te = this.getTE(world, pos);
		if (te != null && te instanceof TilePressureValve) {
			((TilePressureValve) te).setClosed(!((TilePressureValve) te).isClosed());
			world.markBlockForUpdate(pos);
			te.markDirty();
			return true;
		}

		return false;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(world, pos, state, placer, stack);

		TilePipeBase te = this.getTE(world, pos);
		if (te != null && te instanceof TilePressureValve)
			((TilePressureValve)te).setHandleFacing(BlockPistonBase.getFacingFromEntity(world, pos, placer));

		this.updateDirection(world, pos);

		if (te != null && te instanceof TilePressureValve) {
			((TilePressureValve)te).setClosed(false);
			((TilePressureValve)te).redstoneChanged(world.isBlockPowered(pos));
		}
	}

	public void updateDirection(IBlockAccess world, BlockPos pos) {
		TilePipeBase te = this.getTE(world, pos);
		if (te != null && te instanceof TilePressureValve) {
			int[] connections = new int[3];
			int axis = ((TilePressureValve) te).getOrientation().ordinal();

			for (EnumFacing facing : EnumFacing.values()) {
				//Set the orientation for the canConnectFrom method
				((TilePressureValve)te).setOrientation(facing.getAxis());

				if (((TilePressureValve)te).getConnection().canConnectFrom(new BlockSide(pos, facing)))
					connections[facing.getAxis().ordinal()]++;
			}
			int value = connections[axis];
			for (int i = 0; i < 3; i++) {
				if (connections[i] > value) {
					value = connections[i];
					axis = i;
				}
			}
			((TilePressureValve)te).setOrientation(Axis.values()[axis]);

			int i = 0;
			while (((TilePressureValve) te).getOrientation() == ((TilePressureValve) te).getHandleFacing().getAxis()) {
				((TilePressureValve) te).setHandleFacing(EnumFacing.values()[++i]);
			}
		}
	}

	@Override
	public void onNeighborBlockChange(World world, BlockPos pos, IBlockState state, Block neighborBlock) {
		TilePipeBase te = this.getTE(world, pos);

		this.updateDirection(world, pos);

		if (te != null && te instanceof TilePressureValve)
			((TilePressureValve) te).redstoneChanged(world.isBlockPowered(pos));
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (this.getPipeColor(accessor.getWorld(), accessor.getPosition()) != EnumDyeColor.BLACK)
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + this.getPipeColor(accessor.getWorld(), accessor.getPosition()).getUnlocalizedName())));
		else
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color.universal")));

		TilePressureValve te =  (TilePressureValve) this.getTE(accessor.getWorld(), accessor.getPosition());

		if (te != null) {
			tooltip.add(I18n.format("tooltip.waila.valve.state", I18n.format("tooltip.waila.valve.state." + (te.isClosed() ? "closed" : "open"))));

			if (!te.isClosed()) {
				tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
				tooltip.add(I18n.format("tooltip.waila.pipe.volume", Units.formatFrom(te.getPipeVolume(), Volume.CUBICMETER)));
			}
		}
	}
}
