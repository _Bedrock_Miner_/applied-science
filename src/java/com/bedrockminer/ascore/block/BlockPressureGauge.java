package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.network.gui.ASGuiHandler;
import com.bedrockminer.ascore.network.gui.ASGuiHandler.GUI;
import com.bedrockminer.ascore.tileentity.TilePressureGauge;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * The pressure gauge displays the amount of pressure in the pipe.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class BlockPressureGauge extends BlockPressurePipe {

	public BlockPressureGauge(String unlocalizedName) {
		super(unlocalizedName);
	}

	// Tile Entity

	@Override
	public TilePipeBase createNewTileEntity(World world, int meta) {
		return new TilePressureGauge();
	}

	// Behaviour

	@Override
	public int getWeakPower(IBlockAccess world, BlockPos pos, IBlockState state, EnumFacing side) {
		TileEntity te = this.getTE(world, pos);
		if (te != null && te instanceof TilePressureGauge)
			return ((TilePressureGauge)te).getRedstoneOutput();
		return 0;
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (super.onBlockActivated(world, pos, state, player, side, hitX, hitY, hitZ)) {
			return true;
		}
		if (!world.isRemote)
			ASGuiHandler.openGUI(player, GUI.PRESSURE_GAUGE, world, pos);
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(world, pos, state, placer, stack);

		TilePipeBase te = this.getTE(world, pos);
		if (te != null && te instanceof TilePressureGauge) {
			((TilePressureGauge)te).setGaugeFacing(BlockPistonBase.getFacingFromEntity(world, pos, placer));
			((TilePressureGauge)te).setGaugeOrientation(placer.getHorizontalFacing().getOpposite());
		}
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (this.getPipeColor(accessor.getWorld(), accessor.getPosition()) != EnumDyeColor.BLACK)
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + this.getPipeColor(accessor.getWorld(), accessor.getPosition()).getUnlocalizedName())));
		else
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color.universal")));

		if (nbt) {
			tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
		}
	}
}
