package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.block.base.BlockPipeBase;
import com.bedrockminer.ascore.item.ASCreativeTabs;
import com.bedrockminer.ascore.modintegration.waila.IWailaBlock;
import com.bedrockminer.ascore.tileentity.TilePressurePipe;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.units.Volume;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BlockPressurePipe extends BlockPipeBase implements IWailaBlock {

	public BlockPressurePipe(String unlocalizedName) {
		super(unlocalizedName);
		this.setHardness(1.0f);
		this.setResistance(10.0f);
		this.setCreativeTab(ASCreativeTabs.tabAppliedScience);
	}

	@Override
	public float getPipeRadius() {
		return 0.1875f;
	}

	@Override
	public TilePipeBase createNewTileEntity(World world, int meta) {
		return new TilePressurePipe();
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (this.getPipeColor(accessor.getWorld(), accessor.getPosition()) != EnumDyeColor.BLACK)
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + this.getPipeColor(accessor.getWorld(), accessor.getPosition()).getUnlocalizedName())));
		else
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color.universal")));

		TilePressurePipe te = ((TilePressurePipe)this.getTE(accessor.getWorld(), accessor.getPosition()));

		if (nbt && te != null) {
			tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
			tooltip.add(I18n.format("tooltip.waila.pipe.volume", Units.formatFrom(te.getPipeVolume(), Volume.CUBICMETER)));
		}
	}

	@Override
	public boolean usesNBTData() {
		return true;
	}
}
