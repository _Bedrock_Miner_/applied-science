package com.bedrockminer.ascore.block;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public final class ASMaterials{

	/**
	 * The pipe material has the following specifications:
	 * <ul>
	 * <li>not replaceable
	 * <li>not translucent (This should be changed from the block itself)
	 * <li>solid
	 * <li>requires no tool
	 * <li>can be pushed by pistons (actually not, because it's a TileEntity)
	 * </ul>
	 * It is used for pipe and wire blocks.
	 */
	public static final Material pipe = new Material(MapColor.stoneColor);

	/**
	 * The machine material has the following specifications:
	 * <ul>
	 * <li>not replaceable
	 * <li>not translucent
	 * <li>solid
	 * <li>requires tools
	 * <li>can be pushed by pistons (actually not, because it's a TileEntity)
	 * </ul>
	 * It is used for machine blocks.
	 */
	public static final Material machine = new Material(MapColor.ironColor) {
		@Override
		public boolean isToolNotRequired() {
			return false;
		}
	};
}
