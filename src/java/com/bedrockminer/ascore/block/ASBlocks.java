package com.bedrockminer.ascore.block;

import com.bedrockminer.ascore.block.itemblock.ItemBlockInterface;
import com.bedrockminer.ascore.block.itemblock.ItemSteamBoiler;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class ASBlocks {

	// Block objects

	public static BlockPressurePipe pressurePipe;
	public static BlockPressureValve pressureValve;
	public static BlockSecurityValve securityValve;
	public static BlockAirFlowLimiter airFlowLimiter;
	public static BlockPressureGauge pressureGauge;
	public static BlockPressureReducer pressureReducer;
	public static BlockPressureStorage pressureStorage;
	public static BlockSteamBoiler steamBoiler;
	public static BlockCreativePressureGenerator creativePressureGenerator;

	public static BlockRotationAxle rotationAxle;
	public static BlockFlywheel flywheel;
	public static BlockCreativeMotor creativeMotor;

	// Initialization

	public static void init() {
		reg(pressurePipe = new BlockPressurePipe("pressure_pipe"), ItemBlockInterface.class);
		reg(pressureValve = new BlockPressureValve("pressure_valve"), ItemBlockInterface.class);
		reg(securityValve = new BlockSecurityValve("security_valve"), ItemBlockInterface.class);
		reg(airFlowLimiter = new BlockAirFlowLimiter("air_flow_limiter"), ItemBlockInterface.class);
		reg(pressureGauge = new BlockPressureGauge("pressure_gauge"), ItemBlockInterface.class);
		reg(pressureReducer = new BlockPressureReducer("pressure_reducer"), ItemBlockInterface.class);
		reg(pressureStorage = new BlockPressureStorage("pressure_storage"));
		reg(steamBoiler = new BlockSteamBoiler("steam_boiler"), ItemSteamBoiler.class);
		reg(creativePressureGenerator = new BlockCreativePressureGenerator("creative_pressure_generator"));

		reg(rotationAxle = new BlockRotationAxle("rotation_axle"), ItemBlockInterface.class);
		reg(flywheel = new BlockFlywheel("flywheel"));
		reg(creativeMotor = new BlockCreativeMotor("creative_motor"));
	}

	// ========================================================================
	// Utility methods

	public static void reg(Block block) {
		GameRegistry.registerBlock(block, block.getUnlocalizedName().substring(5));
	}

	public static void reg(Block block, Class<? extends ItemBlock> itemclass) {
		GameRegistry.registerBlock(block, itemclass, block.getUnlocalizedName().substring(5));
	}
}
