package com.bedrockminer.ascore.block;

import java.util.List;
import java.util.Set;

import com.bedrockminer.ascore.network.gui.ASGuiHandler;
import com.bedrockminer.ascore.network.gui.ASGuiHandler.GUI;
import com.bedrockminer.ascore.tileentity.TilePressureReducer;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.google.common.collect.ImmutableSet;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/**
 * The pressure reducer only lets air through until a certain pressure is
 * reached.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class BlockPressureReducer extends BlockPressurePipe {

	public BlockPressureReducer(String unlocalizedName) {
		super(unlocalizedName);
	}

	// Tile Entity

	@Override
	public TilePipeBase createNewTileEntity(World world, int meta) {
		return new TilePressureReducer();
	}

	@Override
	public Set<EnumFacing> getPipeConnections(IBlockAccess world, BlockPos pos) {
		TilePressureReducer te = (TilePressureReducer) this.getTE(world, pos);
		if (te != null) {
			EnumFacing orientation = te.getOrientation();
			return ImmutableSet.of(orientation, orientation.getOpposite());
		}
		return ImmutableSet.<EnumFacing>of();
	}

	// Behaviour

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (super.onBlockActivated(world, pos, state, player, side, hitX, hitY, hitZ))
			return true;

		if (!world.isRemote)
			ASGuiHandler.openGUI(player, GUI.PRESSURE_REDUCER, world, pos);
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(world, pos, state, placer, stack);

		EnumFacing direction = BlockPistonBase.getFacingFromEntity(world, pos, placer).getOpposite();
		TilePipeBase te = this.getTE(world, pos);
		if (te != null && te instanceof TilePressureReducer) {
			((TilePressureReducer)te).setOrientation(direction);
		}
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (this.getPipeColor(accessor.getWorld(), accessor.getPosition()) != EnumDyeColor.BLACK)
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + this.getPipeColor(accessor.getWorld(), accessor.getPosition()).getUnlocalizedName())));
		else
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color.universal")));

		if (nbt) {
			tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
			tooltip.add(I18n.format("container.pressure_reducer.limit", Units.formatFrom(accessor.getNBTData().getDouble("Limit"), Pressure.BAR)));
		}
	}
}
