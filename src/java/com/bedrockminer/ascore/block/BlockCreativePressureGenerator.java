package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.block.base.BlockTileEntityProvider;
import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.item.ASCreativeTabs;
import com.bedrockminer.ascore.modintegration.waila.IWailaBlock;
import com.bedrockminer.ascore.network.gui.ASGuiHandler;
import com.bedrockminer.ascore.network.gui.ASGuiHandler.GUI;
import com.bedrockminer.ascore.tileentity.TileCreativePressureGenerator;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.units.Volume;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCreativePressureGenerator extends BlockTileEntityProvider
		implements ICircuitElement, IWailaBlock {

	public BlockCreativePressureGenerator(String unlocalizedName) {
		super(ASMaterials.machine);
		this.setUnlocalizedName(unlocalizedName);
		this.setBlockUnbreakable();
		this.setResistance(2000.0f);
		this.setCreativeTab(ASCreativeTabs.tabAppliedScience);
	}

	@Override
	public TileCircuitElement getTE(IBlockAccess world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof TileCreativePressureGenerator)
			return (TileCreativePressureGenerator) te;
		return null;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileCreativePressureGenerator();
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (!world.isRemote)
			ASGuiHandler.openGUI(player, GUI.CREATIVE_PRESSURE_GENERATOR, world, pos);
		return true;
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state) {
		this.getTE(world, pos).onBreakBlock();
		super.breakBlock(world, pos, state);
	}

	@Override
	public EnumWorldBlockLayer getBlockLayer() {
		return EnumWorldBlockLayer.TRANSLUCENT;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		tooltip.add(I18n.format("tooltip.waila.creative.pressure.pressure", Units.formatFrom(((TileCreativePressureGenerator)accessor.getTileEntity()).getMaximumPressure(), Pressure.BAR)));
		tooltip.add(I18n.format("tooltip.waila.creative.pressure.generation", Units.formatFrom(((TileCreativePressureGenerator)accessor.getTileEntity()).getAirPerTick() * 20.0, Volume.CUBICMETER)));
	}

	@Override
	public boolean usesNBTData() {
		return false;
	}
}
