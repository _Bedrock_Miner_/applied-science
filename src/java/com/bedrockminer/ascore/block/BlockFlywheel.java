package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.block.base.BlockTileEntityProvider;
import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.item.ASCreativeTabs;
import com.bedrockminer.ascore.modintegration.waila.IWailaBlock;
import com.bedrockminer.ascore.tileentity.TileFlywheel;
import com.bedrockminer.ascore.units.RotationSpeed;
import com.bedrockminer.ascore.units.RotationalInertia;
import com.bedrockminer.ascore.units.Torque;
import com.bedrockminer.ascore.units.Units;
import com.bedrockminer.ascore.util.BlockSide;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockFlywheel extends BlockTileEntityProvider
		implements ICircuitElement, IWailaBlock {

	protected BlockFlywheel(String unlocalizedName) {
		super(ASMaterials.machine);
		this.setUnlocalizedName(unlocalizedName);
		this.setHarvestLevel("pickaxe", 2);
		this.setHardness(3.0f);
		this.setResistance(20.0f);
		this.setCreativeTab(ASCreativeTabs.tabAppliedScience);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileFlywheel();
	}

	@Override
	public TileFlywheel getTE(IBlockAccess world, BlockPos pos) {
		return (TileFlywheel) world.getTileEntity(pos);
	}

	@Override
	public int getRenderType() {
		return -1;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean isFullCube() {
		return false;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(world, pos, state, placer, stack);
		this.updateDirection(world, pos);
	}

	public void updateDirection(IBlockAccess world, BlockPos pos) {
		TileFlywheel te = this.getTE(world, pos);
		if (te != null) {
			int[] connections = new int[3];
			int axis = te.getOrientation().ordinal();

			for (EnumFacing facing : EnumFacing.values()) {
				//Set the orientation for the canConnectFrom method
				te.setOrientation(facing.getAxis());

				if (te.getConnection().canConnectFrom(new BlockSide(pos, facing)))
					connections[facing.getAxis().ordinal()]++;
			}
			int value = connections[axis];
			for (int i = 0; i < 3; i++) {
				if (connections[i] > value) {
					value = connections[i];
					axis = i;
				}
			}
			te.setOrientation(Axis.values()[axis]);
		}
	}

	@Override
	public void onNeighborBlockChange(World world, BlockPos pos, IBlockState state, Block neighborBlock) {
		this.updateDirection(world, pos);
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		TileFlywheel te = this.getTE(accessor.getWorld(), accessor.getPosition());
		if (te != null) {
			tooltip.add(I18n.format("container.general.rot_inertia", Units.formatFrom(te.getConnection().getInertia(), RotationalInertia.KGM2)));
			tooltip.add(I18n.format("container.general.resistance", Units.formatFrom(te.getConnection().getResistance(), Torque.NEWTONMETER)));
			tooltip.add(I18n.format("container.general.rotation_speed", Units.formatFrom(te.getConnection().getStoredRotationSpeed(), RotationSpeed.INTERNAL)));
		}
	}

	@Override
	public boolean usesNBTData() {
		return false;
	}
}
