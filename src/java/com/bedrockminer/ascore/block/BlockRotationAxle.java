package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.block.base.BlockPipeBase;
import com.bedrockminer.ascore.circuitry.connections.RotationConnection;
import com.bedrockminer.ascore.item.ASCreativeTabs;
import com.bedrockminer.ascore.modintegration.waila.IWailaBlock;
import com.bedrockminer.ascore.tileentity.TileRotationAxle;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.units.RotationSpeed;
import com.bedrockminer.ascore.units.RotationalInertia;
import com.bedrockminer.ascore.units.Torque;
import com.bedrockminer.ascore.units.Units;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRotationAxle extends BlockPipeBase implements IWailaBlock {

	public BlockRotationAxle(String unlocalizedName) {
		super(unlocalizedName);
		this.setHardness(1.0f);
		this.setResistance(10.0f);
		this.setCreativeTab(ASCreativeTabs.tabAppliedScience);
	}

	@Override
	public float getPipeRadius() {
		return 0.09375f;
	}

	@Override
	public TilePipeBase createNewTileEntity(World world, int meta) {
		return new TileRotationAxle();
	}

	@Override
	public int getRenderType() {
		return -1;
	}

	@Override
	protected BlockState createBlockState() {
		return new BlockState(this);
	}

	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos) {
		return state;
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (this.getPipeColor(accessor.getWorld(), accessor.getPosition()) != EnumDyeColor.BLACK)
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color." + this.getPipeColor(accessor.getWorld(), accessor.getPosition()).getUnlocalizedName())));
		else
			tooltip.add(I18n.format("tooltip.pipe.connection_color", I18n.format("tooltip.color.universal")));

		TileRotationAxle te = (TileRotationAxle) this.getTE(accessor.getWorld(), accessor.getPosition());

		if (te != null) {
			tooltip.add(I18n.format("container.general.rot_inertia", Units.formatFrom(((RotationConnection)te.getConnection()).getInertia(), RotationalInertia.KGM2)));
			tooltip.add(I18n.format("container.general.resistance", Units.formatFrom(((RotationConnection)te.getConnection()).getResistance(), Torque.NEWTONMETER)));
			tooltip.add(I18n.format("container.general.rotation_speed", Units.formatFrom(((RotationConnection) te.getConnection()).getStoredRotationSpeed(), RotationSpeed.INTERNAL)));
		}
	}

	@Override
	public boolean usesNBTData() {
		return false;
	}
}
