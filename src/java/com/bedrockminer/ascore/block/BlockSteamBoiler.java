package com.bedrockminer.ascore.block;

import java.util.List;

import com.bedrockminer.ascore.block.base.BlockTileEntityProvider;
import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.block.base.IMultiblockStructure;
import com.bedrockminer.ascore.item.ASCreativeTabs;
import com.bedrockminer.ascore.modintegration.waila.IWailaBlock;
import com.bedrockminer.ascore.network.gui.ASGuiHandler;
import com.bedrockminer.ascore.network.gui.ASGuiHandler.GUI;
import com.bedrockminer.ascore.tileentity.TileInventoryReference;
import com.bedrockminer.ascore.tileentity.TileSteamBoiler;
import com.bedrockminer.ascore.units.FluidVolume;
import com.bedrockminer.ascore.units.Pressure;
import com.bedrockminer.ascore.units.Temperature;
import com.bedrockminer.ascore.units.Units;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockSteamBoiler extends BlockTileEntityProvider
		implements ICircuitElement, IMultiblockStructure, IWailaBlock {

	public static PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);
	public static PropertyBool FRONT = PropertyBool.create("front");

	public BlockSteamBoiler(String unlocalizedName) {
		super(ASMaterials.machine);
		this.setUnlocalizedName(unlocalizedName);
		this.setHarvestLevel("pickaxe", 2);
		this.setHardness(3.0f);
		this.setResistance(20.0f);
		this.setCreativeTab(ASCreativeTabs.tabAppliedScience);
	}

	// General properties

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public EnumWorldBlockLayer getBlockLayer() {
		return EnumWorldBlockLayer.CUTOUT;
	}

	// Tile Entity properties

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		if (this.getStateFromMeta(meta).getValue(FRONT))
			return new TileSteamBoiler();
		else
			return new TileInventoryReference();
	}

	@Override
	public TileSteamBoiler getTE(IBlockAccess world, BlockPos pos) {
		TileEntity te = world.getTileEntity(this.getMainBlock(world, pos));
		if (te != null && te instanceof TileSteamBoiler)
			return (TileSteamBoiler) te;
		return null;
	}

	// Blockstate properties

	@Override
	protected BlockState createBlockState() {
		return new BlockState(this, FACING, FRONT);
	}

	@Override
	public IBlockState getStateFromMeta(int meta) {
		EnumFacing facing = EnumFacing.getHorizontal(meta & 3);
		return this.getDefaultState().withProperty(FACING, facing).withProperty(FRONT, (meta & 4) > 0);
	}

	@Override
	public int getMetaFromState(IBlockState state) {
		return state.getValue(FACING).getHorizontalIndex() | (state.getValue(FRONT) ? 4 : 0);
	}

	// Behaviour

	@Override
	public BlockPos getMainBlock(IBlockAccess world, BlockPos pos) {
		IBlockState state = world.getBlockState(pos);
		BlockPos main = pos;
		if (!state.getValue(FRONT))
			main = pos.offset(state.getValue(FACING).getOpposite());
		return main;
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state) {
		if (state.getValue(FRONT)) {
			((TileSteamBoiler) world.getTileEntity(pos)).onBreakBlock();
			InventoryHelper.dropInventoryItems(world, pos, ((TileSteamBoiler) world.getTileEntity(pos)));
		}

		BlockPos second = pos.offset(state.getValue(FRONT) ? state.getValue(FACING) : state.getValue(FACING).getOpposite());
		if (world.getBlockState(second).getBlock() == this)
			world.setBlockToAir(second);
		super.breakBlock(world, pos, state);
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() == Items.water_bucket && !world.isRemote) {
			TileSteamBoiler te = this.getTE(world, pos);
			if (te != null && te.getWater() < 1.25 - 0.03125) {
				te.addWater(0.03125);
				if (!player.capabilities.isCreativeMode)
					player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(Items.bucket));
			}
		} else if (!world.isRemote) {
			ASGuiHandler.openGUI(player, GUI.STEAM_BOILER, world, pos);
		}
		return true;
	}

	@Override
	public void getWailaBody(ItemStack stack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config, boolean nbt) {
		if (nbt) {
			tooltip.add(I18n.format("container.steam_boiler.water", Units.formatFrom(accessor.getNBTData().getDouble("Water"), FluidVolume.CUBICMETER)));
			tooltip.add(I18n.format("container.general.temperature", Units.formatFrom(accessor.getNBTData().getDouble("Temperature"), Temperature.CELSIUS)));
			tooltip.add(I18n.format("container.general.pressure", Units.formatFrom(accessor.getNBTData().getDouble("Pressure"), Pressure.BAR)));
		}
	}

	@Override
	public boolean usesNBTData() {
		return true;
	}
}
