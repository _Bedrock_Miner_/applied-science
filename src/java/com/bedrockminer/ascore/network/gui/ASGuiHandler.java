package com.bedrockminer.ascore.network.gui;

import com.bedrockminer.ascore.AppliedScience;
import com.bedrockminer.ascore.block.base.IMultiblockStructure;
import com.bedrockminer.ascore.client.gui.GuiAirFlowLimiter;
import com.bedrockminer.ascore.client.gui.GuiCreativeMotor;
import com.bedrockminer.ascore.client.gui.GuiCreativePressureGenerator;
import com.bedrockminer.ascore.client.gui.GuiPressureGauge;
import com.bedrockminer.ascore.client.gui.GuiPressureReducer;
import com.bedrockminer.ascore.client.gui.GuiSecurityValve;
import com.bedrockminer.ascore.client.gui.GuiSteamBoiler;
import com.bedrockminer.ascore.inventory.ContainerAirFlowLimiter;
import com.bedrockminer.ascore.inventory.ContainerCreativeMotor;
import com.bedrockminer.ascore.inventory.ContainerCreativePressureGenerator;
import com.bedrockminer.ascore.inventory.ContainerPressureGauge;
import com.bedrockminer.ascore.inventory.ContainerPressureReducer;
import com.bedrockminer.ascore.inventory.ContainerSecurityValve;
import com.bedrockminer.ascore.inventory.ContainerSteamBoiler;
import com.bedrockminer.ascore.tileentity.TileAirFlowLimiter;
import com.bedrockminer.ascore.tileentity.TileCreativeMotor;
import com.bedrockminer.ascore.tileentity.TileCreativePressureGenerator;
import com.bedrockminer.ascore.tileentity.TilePressureGauge;
import com.bedrockminer.ascore.tileentity.TilePressureReducer;
import com.bedrockminer.ascore.tileentity.TileSecurityValve;
import com.bedrockminer.ascore.tileentity.TileSteamBoiler;

import net.minecraft.block.Block;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public final class ASGuiHandler implements IGuiHandler {

	public static enum GUI {
		STEAM_BOILER,
		CREATIVE_PRESSURE_GENERATOR,
		SECURITY_VALVE,
		AIR_FLOW_LIMITER,
		PRESSURE_GAUGE,
		PRESSURE_REDUCER,
		CREATIVE_MOTOR;
	}

	@Override
	public Container getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID < 0 || ID >= GUI.values().length)
			return null;

		Block block = world.getBlockState(new BlockPos(x, y, z)).getBlock();
		BlockPos pos = block instanceof IMultiblockStructure ? ((IMultiblockStructure) block).getMainBlock(world, new BlockPos(x, y, z)) : new BlockPos(x, y, z);
		TileEntity te = world.getTileEntity(pos);

		switch (GUI.values()[ID]) {
		case STEAM_BOILER:
			return new ContainerSteamBoiler((TileSteamBoiler) te, player);
		case CREATIVE_PRESSURE_GENERATOR:
			return new ContainerCreativePressureGenerator((TileCreativePressureGenerator) te, player);
		case SECURITY_VALVE:
			return new ContainerSecurityValve((TileSecurityValve) te, player);
		case AIR_FLOW_LIMITER:
			return new ContainerAirFlowLimiter((TileAirFlowLimiter) te, player);
		case PRESSURE_GAUGE:
			return new ContainerPressureGauge((TilePressureGauge) te, player);
		case PRESSURE_REDUCER:
			return new ContainerPressureReducer((TilePressureReducer) te, player);
		case CREATIVE_MOTOR:
			return new ContainerCreativeMotor((TileCreativeMotor) te, player);
		}

		return null;
	}

	@Override
	public GuiScreen getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID < 0 || ID >= GUI.values().length)
			return null;

		Block block = world.getBlockState(new BlockPos(x, y, z)).getBlock();
		BlockPos pos = block instanceof IMultiblockStructure ? ((IMultiblockStructure) block).getMainBlock(world, new BlockPos(x, y, z)) : new BlockPos(x, y, z);
		TileEntity te = world.getTileEntity(pos);

		switch (GUI.values()[ID]) {
		case STEAM_BOILER:
			return new GuiSteamBoiler(new ContainerSteamBoiler((TileSteamBoiler) world.getTileEntity(pos), player));
		case CREATIVE_PRESSURE_GENERATOR:
			return new GuiCreativePressureGenerator(new ContainerCreativePressureGenerator((TileCreativePressureGenerator) world.getTileEntity(pos), player));
		case SECURITY_VALVE:
			return new GuiSecurityValve(new ContainerSecurityValve((TileSecurityValve) world.getTileEntity(pos), player));
		case AIR_FLOW_LIMITER:
			return new GuiAirFlowLimiter(new ContainerAirFlowLimiter((TileAirFlowLimiter) world.getTileEntity(pos), player));
		case PRESSURE_GAUGE:
			return new GuiPressureGauge(new ContainerPressureGauge((TilePressureGauge) world.getTileEntity(pos), player));
		case PRESSURE_REDUCER:
			return new GuiPressureReducer(new ContainerPressureReducer((TilePressureReducer) world.getTileEntity(pos), player));
		case CREATIVE_MOTOR:
			return new GuiCreativeMotor(new ContainerCreativeMotor((TileCreativeMotor) te, player));
		}

		return null;
	}

	public static void openGUI(EntityPlayer player, GUI gui, World world, BlockPos pos) {
		player.openGui(AppliedScience.instance, gui.ordinal(), world, pos.getX(), pos.getY(), pos.getZ());
	}
}
