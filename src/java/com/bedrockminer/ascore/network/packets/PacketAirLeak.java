package com.bedrockminer.ascore.network.packets;

import java.util.Random;

import com.bedrockminer.ascore.circuitry.circuits.PressureCircuit;
import com.bedrockminer.ascore.network.MessageHandler;
import com.bedrockminer.ascore.util.BlockSide;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.Vec3;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * This packet informs the client of an air leak. This will display particles
 * for the client and adjust the players motion if he's standing near the leak.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class PacketAirLeak implements IMessage {

	/** The position of the leak */
	private BlockSide pos;
	/** The volume of air leaking */
	private double volume;

	public PacketAirLeak() {
	}

	public PacketAirLeak(BlockSide pos, double volume) {
		this.pos = pos;
		this.volume = volume;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.pos = new BlockSide(new BlockPos(buf.readInt(), buf.readInt(), buf.readInt()), EnumFacing.getFront(buf.readByte()));
		this.volume = buf.readDouble();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.pos.getPos().getX());
		buf.writeInt(this.pos.getPos().getY());
		buf.writeInt(this.pos.getPos().getZ());
		buf.writeByte(this.pos.getSide().getIndex());
		buf.writeDouble(this.volume);
	}

	// ========================================================================

	public static class Handler extends MessageHandler.Client<PacketAirLeak> {

		@Override
		public IMessage handleClientMessage(EntityPlayer player, PacketAirLeak msg, MessageContext ctx) {
			Random r = player.worldObj.rand;
			double x = msg.pos.getPos().getX() + msg.pos.getSide().getFrontOffsetX() * 0.7 + r.nextGaussian() * 0.1 + 0.5;
			double y = msg.pos.getPos().getY() + msg.pos.getSide().getFrontOffsetY() * 0.7 + r.nextGaussian() * 0.1 + 0.5;
			double z = msg.pos.getPos().getZ() + msg.pos.getSide().getFrontOffsetZ() * 0.7 + r.nextGaussian() * 0.1 + 0.5;

			// Spawning particles
			if (msg.volume > 0) {
				// Pressure release
				double speed = 0.1 + 0.0025 * msg.volume;
				double vx = msg.pos.getSide().getFrontOffsetX() * speed;
				double vy = msg.pos.getSide().getFrontOffsetY() * speed;
				double vz = msg.pos.getSide().getFrontOffsetZ() * speed;
				double inac = 0.2 * speed;

				for (int i = 0; i < 120 * speed - 12; i++)
					player.worldObj.spawnParticle(EnumParticleTypes.CLOUD, x, y, z, vx == 0 ? r.nextGaussian() * inac : vx, vy == 0 ? r.nextGaussian() * inac : vy, vz == 0 ? r.nextGaussian() * inac : vz);

			} else {
				// Vacuum
				double speed = 0.1 - 0.0025 * msg.volume;
				for (int i = 0; i < 120 * speed - 12; i++) {
					double radius = 6;
					Vec3 direction = new Vec3(msg.pos.getSide().getDirectionVec());
					direction = direction.addVector(r.nextGaussian() * 0.5, r.nextGaussian() * 0.5, r.nextGaussian() * 0.5);
					Vec3 motion = new Vec3(-direction.xCoord * speed, -direction.yCoord * speed - 0.06, -direction.zCoord * speed);
					Vec3 start = new Vec3(msg.pos.getPos().getX() + msg.pos.getSide().getFrontOffsetX() + 0.5 - radius * motion.xCoord, msg.pos.getPos().getY() + msg.pos.getSide().getFrontOffsetY() + 0.5 - radius * motion.yCoord, msg.pos.getPos().getZ() + msg.pos.getSide().getFrontOffsetZ() + 0.5 - radius * motion.zCoord);
					player.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, start.xCoord, start.yCoord, start.zCoord, motion.xCoord, motion.yCoord, motion.zCoord);
				}
			}

			// Accelerating the player
			BlockPos origin = msg.pos.getPos().offset(msg.pos.getSide().getOpposite());
			double height = player.getEntityBoundingBox().maxY - player.getEntityBoundingBox().minY;
			Vec3 toPlayer = new Vec3(player.posX - origin.getX() - 0.5, player.posY + height / 2 - origin.getY() - 0.5, player.posZ - origin.getZ() - 0.5);
			Vec3 facing = new Vec3(msg.pos.getSide().getDirectionVec());
			double angle = Math.acos(Math.abs(toPlayer.dotProduct(facing)) / Math.abs(toPlayer.lengthVector() * facing.lengthVector()));
			if (toPlayer.dotProduct(facing) < 0)
				angle += Math.signum(angle) * Math.PI / 2;
			if (angle < Math.PI / 6 && toPlayer.lengthVector() < 16) {
				Vec3 direction = toPlayer.normalize();
				double speed = Math.exp(-0.5 * toPlayer.lengthVector() - 0.5) * 0.04 * PressureCircuit.PACKET_DELAY * msg.volume;
				if (msg.volume < 0)
					speed *= 2;
				player.motionX += direction.xCoord * speed;
				player.motionY += direction.yCoord * speed;
				player.motionZ += direction.zCoord * speed;
			}

			return null;
		}

	}
}
