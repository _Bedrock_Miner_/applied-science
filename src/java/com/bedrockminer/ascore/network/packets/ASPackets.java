package com.bedrockminer.ascore.network.packets;

import com.bedrockminer.ascore.AppliedScience;
import com.bedrockminer.ascore.network.PacketHandler;

import net.minecraftforge.fml.relauncher.Side;

public final class ASPackets {

	public static void init() {
		AppliedScience.packetHandler = new PacketHandler(AppliedScience.MODID);

		AppliedScience.packetHandler.registerPacket(PacketAirLeak.class, new PacketAirLeak.Handler(), Side.CLIENT);

		AppliedScience.packetHandler.registerBidiPacket(PacketGuiSync.class, new PacketGuiSync.Handler());
	}
}
