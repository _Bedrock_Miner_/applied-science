package com.bedrockminer.ascore.network.packets;

import com.bedrockminer.ascore.inventory.ContainerBase;
import com.bedrockminer.ascore.network.MessageHandler;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * The PacketGuiSync is used to send data from the server to the client to sync
 * GUI content.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class PacketGuiSync implements IMessage {

	/** The field ID */
	private int ID;
	/** The data being sent */
	private NBTBase data;

	public PacketGuiSync() {
	}

	public PacketGuiSync(int ID, NBTBase data) {
		this.ID = ID;
		this.data = data;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.ID = buf.readInt();
		this.data = ByteBufUtils.readTag(buf).getTag("");
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.ID);
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setTag("", this.data);
		ByteBufUtils.writeTag(buf, nbt);
	}

	// ========================================================================

	public static class Handler extends MessageHandler.Bidirectional<PacketGuiSync> {

		@Override
		public IMessage handleClientMessage(EntityPlayer player, PacketGuiSync msg, MessageContext ctx) {
			Container container = player.openContainer;
	        if(container instanceof ContainerBase) {
	            ((ContainerBase<?>)container).receiveData(msg.ID, msg.data);
	        }
			return null;
		}

		@Override
		public IMessage handleServerMessage(EntityPlayerMP player, PacketGuiSync msg, MessageContext ctx) {
			Container container = player.openContainer;
	        if(container instanceof ContainerBase) {
	            ((ContainerBase<?>)container).receiveNotification(msg.ID, msg.data, player);
	        }
			return null;
		}
	}
}
