package com.bedrockminer.ascore.util;

import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

public class BlockSide {

	private BlockPos pos;
	private EnumFacing side;

	public BlockSide(BlockPos pos, EnumFacing side) {
		this.pos = pos;
		this.side = side;
	}

	public BlockPos getPos() {
		return this.pos;
	}

	public EnumFacing getSide() {
		return this.side;
	}

	public BlockSide getOpposite() {
		return new BlockSide(this.getPos().offset(this.getSide()), this.getSide().getOpposite());
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof BlockSide && ((BlockSide) obj).getPos().equals(this.getPos()) && ((BlockSide) obj).getSide() == this.getSide();
	}

	@Override
	public int hashCode() {
		return 31 * this.getPos().hashCode() + this.getSide().getIndex();
	}

	@Override
	public String toString() {
		return String.format("Block Side {x=%s, y=%s, z=%s, s=%s}", this.getPos().getX(), this.getPos().getY(), this.getPos().getZ(), this.getSide().getName());
	}
}
