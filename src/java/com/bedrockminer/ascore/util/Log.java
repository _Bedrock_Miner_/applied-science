package com.bedrockminer.ascore.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bedrockminer.ascore.AppliedScience;

/**
 * The logging class. All logging should go through this class.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public final class Log {

	public static Logger LOGGER = LogManager.getLogger(AppliedScience.MODID);

	/**
	 * Logs fatal errors which will crash the game.
	 *
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void fatal(String text, Object... format) {
		log(Level.FATAL, text, format);
	}

	/**
	 * Logs an Error (probably recoverable).
	 *
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void error(String text, Object... format) {
		log(Level.ERROR, text, format);
	}

	/**
	 * Logs general warnings.
	 *
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void warn(String text, Object... format) {
		log(Level.WARN, text, format);
	}

	/**
	 * Logs an information.
	 *
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void info(String text, Object... format) {
		log(Level.INFO, text, format);
	}

	/**
	 * Logs debug messages (only visible in the fml-client/server-latest.log
	 * file)
	 *
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void debug(String text, Object... format) {
		log(Level.DEBUG, text, format);
	}

	/**
	 * Logs trace messages (only visible in the fml-client/server-latest.log
	 * file)
	 *
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void trace(String text, Object... format) {
		log(Level.TRACE, text, format);
	}

	/**
	 * General log method which can log at every level of importance.
	 *
	 * @param level
	 * the level
	 * @param mod
	 * the mod
	 * @param text
	 * the text
	 * @param format
	 * the format
	 */
	public static void log(Level level, String text, Object... format) {
		LOGGER.log(level, String.format(text, format));
	}

	/**
	 * Prints the stack trace of the given throwable to the ERROR output.
	 *
	 * @param t the throwable
	 */
	public static void printStackTrace(Throwable t) {
		StringWriter wr = new StringWriter();
		t.printStackTrace(new PrintWriter(wr));
		String[] lines = wr.toString().split("\r?\n");
		for (String s: lines)
			Log.error(s);
	}

	/**
	 * Logs the call of the currently active method for debugging.
	 * <p>
	 * This code:<br>
	 * <code>public void onUpdate() {
	 *     Log.called();
	 * }</code><br>
	 * Leads to this output:<br>
	 * <code>Called EnclosingClass.onUpdate() [line: line] at system time
	 * currentSystemTime in thread CurrentThread.</code>
	 * @deprecated Marked as deprecated to remind the modder of removing the
	 * method calls
	 * before publishing the mod.
	 */
	@Deprecated
	public static void called() {
		Thread t = Thread.currentThread();
		StackTraceElement top = t.getStackTrace()[2];
		Log.info("Called %s.%s() [line: %s] at system time %s in thread %s.", top.getClassName().substring(top.getClassName().lastIndexOf(".") + 1), top.getMethodName(), top.getLineNumber(), System.currentTimeMillis(), t.getName());
	}

	/**
	 * Prints the currently active method. Useful for errors that were recovered
	 * instead of being thrown as an exception.
	 * <p>
	 * The output is:<br>
	 * <code>at: AnyClass.anyMethod() [line: line].</code><br>
	 * The output is printed as an error message.
	 */
	public static void printCurrentMethod() {
		StackTraceElement top = Thread.currentThread().getStackTrace()[2];
		Log.error("at: %s.%s() [line: %s].", top.getClassName().substring(top.getClassName().lastIndexOf(".") + 1), top.getMethodName(), top.getLineNumber());
	}
}
