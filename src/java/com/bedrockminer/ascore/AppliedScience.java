package com.bedrockminer.ascore;

import com.bedrockminer.ascore.network.PacketHandler;
import com.bedrockminer.ascore.util.Log;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = AppliedScience.MODID, name = AppliedScience.NAME, version = AppliedScience.VERSION, acceptedMinecraftVersions = AppliedScience.MCVERSION, guiFactory = AppliedScience.GUIFACTORY)
public final class AppliedScience {

	public static final String MODID = "ascore";
	public static final String NAME = "Applied Science";
	public static final String VERSION = "0.0-a.16";
	public static final String MCVERSION = "[1.8.9]";

	public static final String GUIFACTORY = "com.bedrockminer.ascore.client.gui.config.ASConfigGui";
	public static final String CLIENTPROXY = "com.bedrockminer.ascore.ClientProxy";
	public static final String COMMONPROXY = "com.bedrockminer.ascore.CommonProxy";

	@Instance
	public static AppliedScience instance = new AppliedScience();

	@SidedProxy(modId = MODID, clientSide = CLIENTPROXY, serverSide = COMMONPROXY)
	public static CommonProxy proxy;

	public static PacketHandler packetHandler;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		Log.info("Initializing Applied Science v. %s", VERSION);
		proxy.preInit(e);
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}
}

