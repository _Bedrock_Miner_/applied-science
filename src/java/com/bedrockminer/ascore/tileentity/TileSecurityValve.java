package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.AppliedScience;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.circuits.PressureCircuit;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.network.packets.PacketAirLeak;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;

/**
 * The security valve releases pressure if a certain treshold is reached.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TileSecurityValve extends TilePressurePipe {

	/** The pipe's orientation */
	private Axis orientation = Axis.X;
	/** The pressure treshold */
	private double pressureTreshold = 5;

	/**
	 * Instantiates a new TileSecurityValve
	 */
	public TileSecurityValve() {
		super();
	}

	@Override
	public Connection createPipeConnection() {
		return new PressureConnection(0, false, false, this);
	}

	// General properties

	/**
	 * Returns the orientation of this pipe
	 *
	 * @return the orientation
	 */
	public Axis getOrientation() {
		return this.orientation;
	}

	/**
	 * Sets the pipe's orientation
	 *
	 * @param orientation
	 * the orientation to set
	 */
	public void setOrientation(Axis orientation) {
		this.orientation = orientation;
		this.getConnection().invalidateAvailablePositions();

		if (!this.getWorld().isRemote) {
			if (this.getConnection().hasCircuit())
				this.getConnection().getCircuit().invalidate();
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns the pressure where this pipe will start releasing air.
	 *
	 * @return the pressure treshold
	 */
	public double getPressureTreshold() {
		return this.pressureTreshold;
	}

	/**
	 * Sets the treshold where this pipe will start releasing air.
	 *
	 * @param treshold
	 * the pressure treshold to set
	 */
	public void setPressureTreshold(double treshold) {
		if (treshold < 1)
			treshold = 1;
		this.pressureTreshold = treshold;
	}

	/**
	 * Returns the maximum pressure treshold for this pipe.
	 *
	 * @return the maximum pressure treshold
	 */
	public double getMaxPressureTreshold() {
		return 101.0;
	}

	@Override
	public double getPipeVolume() {
		return 4 * PIPE_RADIUS * PIPE_RADIUS;
	}

	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(
					new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, this.getOrientation())),
					new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, this.getOrientation())));

		return ImmutableSet.<BlockSide>of();
	}

	@Override
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
		positions.clear();
		positions.addAll(this.getPositionsForConnection(connection));
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote) {
			PressureCircuit c = ((PressureCircuit) this.getConnection().getCircuit());
			if (c != null && c.getPressure() >= this.getPressureTreshold() && c.getPressure() != 1) {
				double aimVolume = this.getPressureTreshold() * c.getVolume();
				double release = (c.getAirVolume() - aimVolume) / 2.0 + 0.25;

				((PressureConnection)this.getConnection()).setAirChange(-release);
				EnumFacing face = this.getOrientation() == Axis.Y ? EnumFacing.HORIZONTALS[this.getWorld().rand.nextInt(4)] : EnumFacing.UP;
				AppliedScience.packetHandler.sendToAllAround(new PacketAirLeak(new BlockSide(this.getPos(), face), release), this.getWorld().provider.getDimensionId(), this.getPos().getX(), this.getPos().getY(), this.getPos().getZ(), 32);
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
		nbt.setDouble("PressureTreshold", this.getPressureTreshold());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];
		this.pressureTreshold = nbt.getDouble("PressureTreshold");

		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];

		this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForWaila(NBTTagCompound nbt) {
		super.writeToNBTForWaila(nbt);
		nbt.setDouble("Treshold", this.getPressureTreshold());
	}
}
