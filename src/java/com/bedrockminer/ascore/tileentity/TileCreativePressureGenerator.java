package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.circuits.PressureCircuit;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class TileCreativePressureGenerator extends TileCircuitElement {

	/** The PressureConnection of this generator */
	private PressureConnection connection;

	/** The maximum pressure this generator will generate */
	private double maximumPressure = 9.5;
	/** The amount of air this generator generates per tick */
	private double airPerTick = 0.1;

	/**
	 * Instantiates a new TileCreativePressureGenerator
	 */
	public TileCreativePressureGenerator() {
		super();
		this.add(this.connection = new PressureConnection(0, false, true, this));
		this.connection.setVolume(1.0);
	}

	/**
	 * Returns the maximum pressure this generator will generate
	 *
	 * @return the maximum pressure
	 */
	public double getMaximumPressure() {
		return this.maximumPressure;
	}

	/**
	 * Returns the amount of air per tick this generator generates
	 *
	 * @return the amount of air per tick
	 */
	public double getAirPerTick() {
		return this.airPerTick;
	}

	/**
	 * Returns the very maximum pressure that can be generated.
	 *
	 * @return the very maximum pressure in bar.
	 */
	public double getPressureTreshold() {
		return 101.0;
	}

	/**
	 * Returns the very maximum amount of air per tick that can be generated.
	 *
	 * @return the very maximum air generation in m^3/t
	 */
	public double getAirTreshold() {
		return 5.0;
	}

	/**
	 * Sets the maximum pressure this generator will generate
	 *
	 * @param pressure
	 * the maximum pressure
	 */
	public void setMaximumPressure(double pressure) {
		if (pressure < 0) pressure = 0;
		if (pressure > this.getPressureTreshold()) pressure = this.getPressureTreshold();
		this.maximumPressure = pressure;
		this.getWorld().markBlockForUpdate(this.getPos());
	}

	/**
	 * Sets the amount of air per tick this generator generates
	 *
	 * @param air
	 * the amount of air per tick
	 */
	public void setAirPerTick(double air) {
		if (air < 0) air = 0;
		if (air > this.getAirTreshold()) air = this.getAirTreshold();
		this.airPerTick = air;
		this.getWorld().markBlockForUpdate(this.getPos());
	}

	@Override
	public boolean canPlayerModify(EntityPlayer player) {
		return super.canPlayerModify(player) && player.capabilities.isCreativeMode;
	}

	/**
	 * Returns the PressureConnection of this generator.
	 *
	 * @return the connection
	 */
	public PressureConnection getConnection() {
		return this.connection;
	}

	//@formatter:off
	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(
					new BlockSide(this.getPos(), EnumFacing.NORTH),
					new BlockSide(this.getPos(), EnumFacing.EAST),
					new BlockSide(this.getPos(), EnumFacing.WEST),
					new BlockSide(this.getPos(), EnumFacing.SOUTH),
					new BlockSide(this.getPos(), EnumFacing.UP),
					new BlockSide(this.getPos(), EnumFacing.DOWN));

		return ImmutableSet.<BlockSide>of();
	}
	//@formatter:on

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote && this.getMaximumPressure() > 0 && this.getAirPerTick() > 0) {
			PressureCircuit pc = this.getConnection().getCircuit();
			if (pc != null && pc.isValid()) {
				if (pc.getVolume() > this.getConnection().getVolume()) {
					double maxVolume = this.getMaximumPressure() * pc.getVolume() - pc.getAirVolume();
					if (maxVolume < 0)
						maxVolume = 0;
					this.getConnection().setAirChange(Math.min(this.getAirPerTick(), maxVolume));
				} else {
					this.getConnection().setAirChange(pc.getVolume() - pc.getAirVolume());
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setDouble("MaxPressure", this.maximumPressure);
		nbt.setDouble("AirPerTick", this.airPerTick);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		this.maximumPressure = nbt.getDouble("MaxPressure");
		this.airPerTick = nbt.getDouble("AirPerTick");
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setDouble("MaxPressure", this.maximumPressure);
		nbt.setDouble("AirPerTick", this.airPerTick);
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		this.maximumPressure = nbt.getDouble("MaxPressure");
		this.airPerTick = nbt.getDouble("AirPerTick");
	}
}
