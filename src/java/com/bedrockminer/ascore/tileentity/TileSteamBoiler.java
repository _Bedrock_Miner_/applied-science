package com.bedrockminer.ascore.tileentity;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import com.bedrockminer.ascore.block.ASBlocks;
import com.bedrockminer.ascore.block.BlockSteamBoiler;
import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.modintegration.waila.IWailaNBT;
import com.bedrockminer.ascore.registries.TemperatureRegistry;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.IChatComponent;

public class TileSteamBoiler extends TileCircuitElement
		implements IWailaNBT, ISidedInventory {

	/** The influence of the boiler itself in temperature calculation */
	public static final double BOILER_MASS = 500.0;

	/** The pressure output */
	private PressureConnection output;

	private ItemStack[] inventory = new ItemStack[this.getSizeInventory()];
	/** The internal temperature */
	private double temperature = Double.NaN;
	/** The amount of water in the boiler */
	private double water;
	/** The item currently used as heat source */
	private ItemStack heatSource;
	/** The burn time left. Once this reaches 0, the next item will be used */
	private int burnTime;
	/** Client-side control if the boiler is burning */
	private boolean isBurning;

	public TileSteamBoiler() {
		this.add(this.output = new PressureConnection(0, false, true, this));
		this.output.setVolume(1.25);
	}

	// Getter and setter

	/**
	 * Returns the output connection for the pressure
	 *
	 * @return the output connection
	 */
	public PressureConnection getOutput() {
		return this.output;
	}

	/**
	 * Returns the temperature in the boiler in Celsius
	 *
	 * @return the temperature
	 */
	public double getTemperature() {
		return this.temperature;
	}

	/**
	 * Returns the ambient temperature around the boiler.
	 *
	 * @return the ambient temperature
	 */
	public double getAmbientTemperature() {
		BlockPos front = this.getPos();
		BlockPos back = this.getPos().offset(this.getWorld().getBlockState(this.getPos()).getValue(BlockSteamBoiler.FACING));

		double sum = 0;
		int count = 0;
		int minX = Math.min(front.getX(), back.getX()) - 1;
		int minY = front.getY() - 1;
		int minZ = Math.min(front.getZ(), back.getZ()) - 1;
		int maxX = Math.max(front.getX(), back.getX()) + 1;
		int maxY = front.getY();
		int maxZ = Math.max(front.getZ(), back.getZ()) + 1;

		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				for (int z = minZ; z <= maxZ; z++) {
					BlockPos pos = new BlockPos(x, y, z);

					boolean inboiler = pos.equals(front) || pos.equals(back);
					boolean belowboiler = pos.equals(front.down()) || pos.equals(back.down());
					boolean down = (y == minY);
					boolean corner = (x == minX || x == maxX) && (z == minZ || z == maxZ);

					int factor = (inboiler ? 0 : (belowboiler ? 6 : (down ? (corner ? 1 : 2) : (corner ? 2 : 3))));
					sum += factor * TemperatureRegistry.getTemperature(this.getWorld(), pos);
					count += factor;
				}
			}
		}

		return sum / count;
	}

	/**
	 * Returns the temperature of the burn material.
	 *
	 * @return the temperature of the burn material
	 */
	public double getBurnMaterialTemperature() {
		if (this.getHeatSource() == null)
			return this.getAmbientTemperature();

		int time = TileEntityFurnace.getItemBurnTime(this.getHeatSource());
		double percentage = -(0.8 / time / time) * (this.getBurnTime() - time / 2.0) * (this.getBurnTime() - time / 2.0) + 1.0;

		return TemperatureRegistry.getItemBurnTemp(this.getHeatSource()) * percentage;
	}

	/**
	 * Returns the amount of water in the boiler in m^3
	 *
	 * @return the amount of water
	 */
	public double getWater() {
		return this.water;
	}

	/**
	 * Returns the item currently used as a heat source
	 *
	 * @return the item
	 */
	public ItemStack getHeatSource() {
		return this.heatSource;
	}

	/**
	 * Returns the burn time left for the current item in ticks
	 *
	 * @return the burn time
	 */
	public int getBurnTime() {
		return this.burnTime;
	}

	/**
	 * Returns whether the boiler is burning.
	 *
	 * @return <code>true</code> if burning
	 */
	public boolean isBurning() {
		if (this.getWorld().isRemote)
			return this.isBurning;
		else
			return this.getBurnTime() > 0;
	}

	/**
	 * Adds the given amount of water. This cools the boiler down.
	 *
	 * @param amount
	 * the amount of water
	 */
	public void addWater(double amount) {
		double oldwater = this.water;

		this.water += amount;

		this.temperature = ((oldwater + BOILER_MASS / 1000.0) * this.getTemperature() + amount * TemperatureRegistry.getAmbientTemperature(this.getWorld(), this.getPos())) / (this.water + BOILER_MASS / 1000.0);

		this.getWorld().markBlockForUpdate(this.getPos());
	}

	// Connection handling

	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getOutput()) {
			IBlockState state = this.getWorld().getBlockState(this.getPos());
			if (state.getBlock() != ASBlocks.steamBoiler)
				return ImmutableSet.<BlockSide> of();
			Set<BlockSide> positions = new HashSet<BlockSide>();
			for (EnumFacing side : EnumFacing.values()) {
				if (side == EnumFacing.DOWN)
					continue;
				if (side != state.getValue(BlockSteamBoiler.FACING))
					positions.add(new BlockSide(this.getPos(), side));
				if (side != state.getValue(BlockSteamBoiler.FACING).getOpposite())
					positions.add(new BlockSide(this.getPos().offset(state.getValue(BlockSteamBoiler.FACING)), side));
			}
			return positions;
		}
		return ImmutableSet.<BlockSide> of();
	}

	// Inventory methods

	@Override
	public String getName() {
		return "container.steam_boiler";
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	@Override
	public IChatComponent getDisplayName() {
		return new ChatComponentTranslation(this.getName());
	}

	@Override
	public int getSizeInventory() {
		return 4;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if (index < 0 || index >= this.getSizeInventory())
			return null;
		return this.inventory[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if (this.getStackInSlot(index) != null) {
			ItemStack itemstack;

			if (this.getStackInSlot(index).stackSize <= count) {
				return this.removeStackFromSlot(index);
			} else {
				itemstack = this.getStackInSlot(index).splitStack(count);

				if (this.getStackInSlot(index).stackSize <= 0) {
					this.setInventorySlotContents(index, null);
				} else {
					// Just to show that changes happened
					this.setInventorySlotContents(index, this.getStackInSlot(index));
				}

				this.markDirty();
				return itemstack;
			}
		} else {
			return null;
		}
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, null);
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if (index < 0 || index >= this.getSizeInventory())
			return;

		if (stack != null && stack.stackSize > this.getInventoryStackLimit())
			stack.stackSize = this.getInventoryStackLimit();

		if (stack != null && stack.stackSize == 0)
			stack = null;

		this.inventory[index] = stack;
		this.markDirty();
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		Block block = this.getWorld().getBlockState(this.getPos()).getBlock();
		if (block instanceof ICircuitElement && ((ICircuitElement)block).getTE(this.getWorld(), this.getPos()) != this)
			return false;
		if (!(block instanceof ICircuitElement) && this.getWorld().getTileEntity(this.getPos()) != this)
			return false;

		return player.getDistanceSq(this.pos.add(0.5, 0.5, 0.5)) <= 64;
	}

	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		if (index >= 0 && index < 3)
			return TileEntityFurnace.isItemFuel(stack) || stack.getItem() == Items.bucket;
		if (index == 3)
			return stack.getItem() == Items.water_bucket || stack.getItem() == Items.bucket;
		return false;
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
		for (int i = 0; i < this.getSizeInventory(); i++)
			this.setInventorySlotContents(i, null);
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		return new int[]{0, 1, 2, 3};
	}

	@Override
	public boolean canInsertItem(int index, ItemStack stack, EnumFacing direction) {
		return this.isItemValidForSlot(index, stack);
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		return stack.getItem() == Items.bucket;
	}

	// Update handling

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (Double.isNaN(this.temperature)) {
			this.temperature = TemperatureRegistry.getAmbientTemperature(this.getWorld(), this.getPos());
		}

		if (!this.getWorld().isRemote) {
			// Update water
			if (this.getStackInSlot(3) != null && this.getStackInSlot(3).getItem() == Items.water_bucket && this.getWater() <= 1.25 - 0.03125) {
				this.setInventorySlotContents(3, new ItemStack(Items.bucket));
				this.addWater(0.03125);
				this.markDirty();
			}
			// Update burning material
			if (this.getHeatSource() != null) {
				if (--this.burnTime <= 0) {
					this.heatSource = null;
					this.getWorld().markBlockForUpdate(this.getPos());
				}
			}
			// Use new item only if there is some water
			if (this.getHeatSource() == null && this.getWater() > 0) {
				for (int i = 0; i < 3; i++) {
					ItemStack stack = this.getStackInSlot(i);
					if (stack != null && stack.stackSize > 0 && TileEntityFurnace.isItemFuel(stack)) {
						this.heatSource = this.decrStackSize(i, 1);
						this.burnTime = TileEntityFurnace.getItemBurnTime(this.heatSource);
						if (this.heatSource.getItem().hasContainerItem(this.heatSource) && this.getStackInSlot(i) == null)
							this.setInventorySlotContents(i, this.heatSource.getItem().getContainerItem(this.heatSource));
						this.getWorld().markBlockForUpdate(this.getPos());
						break;
					}
				}
			}

			if (this.getWorld().getTotalWorldTime() % 200 == 0)
				this.getWorld().markBlockForUpdate(this.getPos());

			// Update temperature
			int ambientFactor = this.getOutput().getCircuit().isLeaking() ? 10 : 1;
			double outsideTemp = (ambientFactor * this.getAmbientTemperature() + 2 * this.getBurnMaterialTemperature()) / (2.0 + ambientFactor);

			// The temperature changes slower if more water is in the boiler
			// 0.5 is the efficiency/speed factor
			// 0.05 is the factor for the calculation ticks (20/s)
			this.temperature += 0.05 * 0.5 * (outsideTemp - this.temperature) / (this.water * 1000.0 + BOILER_MASS);

			// Update steam production
			double boilingPoint = 100 + 33 * Math.log(this.getOutput().getPressure());
			double tempDiff = this.temperature - boilingPoint;
			if (tempDiff > 0 && this.water > 0) {
				// Calculation: energy = water * 4.19 * temp difference
				// steamed = 0.25 * energy / 2257
				// 4.19 is the heat capacity of water
				// 2257 is the energy needed to turn a liter of 100 Celsius water to
				// steam

				double maxPressure = Math.exp((this.temperature - 100.0) * 0.03030303030303);
				if (this.getOutput().getCircuit().isLeaking())
					maxPressure = Double.MAX_VALUE;
				double maxGasProduction = maxPressure * this.getOutput().getCircuit().getVolume() - this.getOutput().getCircuit().getAirVolume();
				double steamed = Math.min(Math.min(this.water, (this.water + BOILER_MASS / 1000.0) * tempDiff * 4.19 / 2257.0), maxGasProduction * 3E-4);

				if (this.water <= 1E-8)
					steamed = this.water;
				this.temperature -= steamed / (this.water + BOILER_MASS / 1000.0) * 2257.0 / 4.19;
				this.water -= steamed;
				this.getOutput().setAirChange(1673 * steamed);
			}
		} else {
			if (this.isBurning()) {
				Random r = this.getWorld().rand;
				for (int i = 0; i < 2; i++) {
					EnumFacing direction = this.getWorld().getBlockState(this.getPos()).getValue(BlockSteamBoiler.FACING);
					BlockPos min = this.getPos().add(direction == EnumFacing.WEST ? 1 : 0, 0, direction == EnumFacing.NORTH ? 1 : 0);
					BlockPos max = min.add(1 + direction.getFrontOffsetX(), 0, 1 + direction.getFrontOffsetZ());
					EnumFacing side = EnumFacing.getHorizontal(r.nextInt(4));
					double x = side == EnumFacing.WEST ? min.getX() : (side == EnumFacing.EAST ? max.getX() : r.nextDouble() * (max.getX() - min.getX()) + min.getX()) + 0.1 * r.nextGaussian();
					double y = r.nextDouble() * 0.25 + min.getY() + 0.1 * r.nextGaussian();
					double z = side == EnumFacing.NORTH ? min.getZ() : (side == EnumFacing.SOUTH ? max.getZ() : r.nextDouble() * (max.getZ() - min.getZ()) + min.getZ()) + 0.1 * r.nextGaussian();

					this.getWorld().spawnParticle(r.nextDouble() < 0.1 ? EnumParticleTypes.FLAME : EnumParticleTypes.SMOKE_NORMAL, x, y, z, 0, 0.001, 0);
				}
			}
		}
	}

	// Writing to NBT

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);

		NBTTagList list = new NBTTagList();
		for (int i = 0; i < this.getSizeInventory(); ++i) {
			if (this.getStackInSlot(i) != null) {
				NBTTagCompound stackTag = new NBTTagCompound();
				stackTag.setByte("Slot", (byte) i);
				this.getStackInSlot(i).writeToNBT(stackTag);
				list.appendTag(stackTag);
			}
		}
		nbt.setTag("Items", list);

		nbt.setDouble("Water", this.water);
		nbt.setDouble("Temperature", this.temperature);
		nbt.setInteger("BurnTime", this.burnTime);
		NBTTagCompound item = new NBTTagCompound();
		if (this.heatSource != null)
			this.heatSource.writeToNBT(item);
		else
			item.setString("id", "missingno");
		nbt.setTag("BurnItem", item);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);

		NBTTagList list = nbt.getTagList("Items", 10);
		for (int i = 0; i < list.tagCount(); ++i) {
			NBTTagCompound stackTag = list.getCompoundTagAt(i);
			int slot = stackTag.getByte("Slot") & 255;
			this.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(stackTag));
		}

		this.water = nbt.getDouble("Water");
		this.temperature = nbt.getDouble("Temperature");
		this.burnTime = nbt.getInteger("BurnTime");
		NBTTagCompound item = nbt.getCompoundTag("BurnItem");
		this.heatSource = ItemStack.loadItemStackFromNBT(item);
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setBoolean("Burning", this.isBurning());
		nbt.setDouble("Water", this.water);
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		this.isBurning = nbt.getBoolean("Burning");
		this.water = nbt.getDouble("Water");
	}

	@Override
	public void writeToNBTForWaila(NBTTagCompound nbt) {
		nbt.setDouble("Water", this.getWater());
		nbt.setDouble("Temperature", this.getTemperature());
		nbt.setDouble("Pressure", this.getOutput().getPressure());
	}
}
