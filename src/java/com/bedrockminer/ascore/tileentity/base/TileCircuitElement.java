package com.bedrockminer.ascore.tileentity.base;

import java.util.HashSet;
import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.ConnectionType;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;

/**
 * Every circuit element needs to have an instance of this TileEntity as it's
 * controller. The TileEntity manages the connections of the element and takes
 * care that they update frequently.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class TileCircuitElement extends TileEntity
		implements ITickable {

	/** The last time this TE was updated */
	private long lastUpdateTick;
	/** The connections controlled by this TE */
	private Set<Connection> connections;

	/**
	 * Instantiates a new TileCircuitElement. Use the Constructor to create the
	 * Connections for your TileEntity.
	 */
	public TileCircuitElement() {
		super();
		this.connections = new HashSet<Connection>();
	}

	// Connection handling

	/**
	 * Adds a new connection to this TileEntity. The connection must have this
	 * TileEntity set as it's parent and an ID that is not used for this TE.
	 *
	 * @param c
	 * the connection to add
	 * @throws IllegalArgumentException
	 * if parent or ID of the Connection are invalid
	 */
	public void add(Connection c) {
		if (c.getTE() != this)
			throw new IllegalArgumentException("Cannot add a Connection with a different parent to this TileEntity");
		if (this.getConnectionByID(c.getID()) != null)
			throw new IllegalArgumentException(String.format("The Connection ID %s already exist for this TileEntity", c.getID()));

		this.connections.add(c);
	}

	/**
	 * Returns a set of all connections this TileEntity controls.
	 *
	 * @return all controlled connections
	 */
	public Set<Connection> getAllConnections() {
		return ImmutableSet.copyOf(this.connections);
	}

	/**
	 * Returns a Connection by its ID or <code>null</code> if no connection with
	 * that ID exist within the scope of this TE.
	 *
	 * @param ID
	 * the ID to look for
	 * @return the connection
	 */
	public Connection getConnectionByID(int ID) {
		for (Connection c : this.getAllConnections())
			if (c.getID() == ID)
				return c;
		return null;
	}

	/**
	 * Calls the update for every connection
	 */
	public void updateAllConnections() {
		for (Connection c : this.getAllConnections())
			c.tryUpdate();
	}

	/**
	 * This method needs to return every position the given connection can be
	 * used at. The connection will connect to other matching connections next
	 * to the given positions. The returned value should be cached because this
	 * method is called very frequently.
	 *
	 * @param connection
	 * the connection
	 * @return the positions
	 */
	public abstract Set<BlockSide> getPositionsForConnection(Connection connection);

	/**
	 * Searches for a matching connection at the given position. The connection
	 * matches if it has the same type and a matching color.
	 *
	 * @param position
	 * the position to check. This must be a position contained in the available
	 * positions of the connection to find.
	 * @param type
	 * the type
	 * @param color
	 * the color
	 * @return A connection or <code>null</code> if none was found.
	 */
	public Connection getConnection(BlockSide position, ConnectionType type, EnumDyeColor color) {
		for (Connection c : this.getAllConnections()) {
			if (c.getSelectedPositions().contains(position) && c.getConnectionType() == type && Connection.doColorsMatch(c.getConnectionColor(), color))
				return c;
		}
		return null;
	}

	/**
	 * This method can be used to change the positions a Connection connects to.
	 * This can be used to force some connections to be present, which will
	 * cause leaks if not connected. This is also used by pipes to make a pipe
	 * straight if only one or no side is really connected.
	 *
	 * @param connection
	 * the connection
	 * @param positions
	 * the positions the connection connects to
	 */
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
	}

	/**
	 * Called when the block is destroyed. Used to invalidate the circuits this
	 * TE is connected to.
	 */
	public void onBreakBlock() {
		for (Connection c : this.getAllConnections()) {
			if (c.hasCircuit())
				c.getCircuit().invalidate();
		}
	}

	/**
	 * Called right before connections of the TE are added to a circuit.
	 *
	 * @param connection
	 * the connection
	 */
	public void onAddingToCircuit(Connection connection) {
	}

	/**
	 * If this returns true, the given player has the right to modify the state
	 * of this TileEntity.
	 *
	 * @param player
	 * the player to check
	 * @return true to grant access
	 */
	public boolean canPlayerModify(EntityPlayer player) {
		return player.getDistanceSq(this.getPos()) <= 64;
	}

	// NBT Data

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);

		if (!this.worldObj.isRemote) {
			NBTTagList list = new NBTTagList();
			for (Connection c : this.getAllConnections()) {
				NBTTagCompound data = new NBTTagCompound();
				data.setByte("ID", c.getID());
				c.writeToNBT(data);
				list.appendTag(data);
			}
			nbt.setTag("Connections", list);
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);

		NBTTagList list = nbt.getTagList("Connections", 10);
		for (int i = 0; i < list.tagCount(); i++) {
			NBTTagCompound data = list.getCompoundTagAt(i);
			int ID = data.getInteger("ID");
			Connection c = this.getConnectionByID(ID);
			if (c != null)
				c.readFromNBT(data);
		}
	}

	/**
	 * Writes important data that needs to be sent to the client.
	 *
	 * @param nbt
	 * the nbt data
	 */
	public void writeToNBTForSync(NBTTagCompound nbt) {
		NBTTagList list = new NBTTagList();
		for (Connection c : this.getAllConnections()) {
			NBTTagCompound data = new NBTTagCompound();
			data.setByte("ID", c.getID());
			c.writeToNBTForSync(data);
			list.appendTag(data);
		}
		nbt.setTag("Connections", list);
	}

	/**
	 * Reads data that was sent from the server.
	 *
	 * @param nbt
	 * the nbt data
	 */
	public void readFromNBTForSync(NBTTagCompound nbt) {
		NBTTagList list = nbt.getTagList("Connections", 10);
		for (int i = 0; i < list.tagCount(); i++) {
			NBTTagCompound data = list.getCompoundTagAt(i);
			int ID = data.getInteger("ID");
			Connection c = this.getConnectionByID(ID);
			if (c != null)
				c.readFromNBTForSync(data);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBTForSync(nbt);
		return new S35PacketUpdateTileEntity(this.getPos(), this.getBlockMetadata(), nbt);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
		this.setPos(pkt.getPos());
		this.updateContainingBlockInfo();
		this.readFromNBTForSync(pkt.getNbtCompound());
	}

	// Update handling

	@Override
	public void update() {
		this.tryUpdate();
	}

	/**
	 * Returns whether the TileEntity can be updated this tick.
	 *
	 * @return <code>true</code> if an update is possible
	 */
	public boolean canUpdate() {
		return this.hasWorldObj() && this.getWorld().getTotalWorldTime() > this.lastUpdateTick;
	}

	/**
	 * If this method is called, the TileEntity will not be updated a second
	 * time this tick.
	 */
	public void markUpdated() {
		if (this.hasWorldObj())
			this.lastUpdateTick = this.getWorld().getTotalWorldTime();
	}

	/**
	 * Updates the TileEntity if necessary. If the TileEntity has been updated
	 * already, nothing happens.
	 */
	public void tryUpdate() {
		if (this.canUpdate())
			this.doUpdate();
	}

	/**
	 * Executes the update for the TileEntity. This should not be called
	 * manually, use {@link #tryUpdate()} instead to avoid multiple update calls
	 * per tick.
	 */
	protected void doUpdate() {
		this.markUpdated();
		if (!this.getWorld().isRemote)
			for (Connection c : this.getAllConnections())
				c.tryUpdate();
	}
}
