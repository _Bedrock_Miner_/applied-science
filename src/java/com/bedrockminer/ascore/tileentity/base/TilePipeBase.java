package com.bedrockminer.ascore.tileentity.base;

import java.util.HashSet;
import java.util.Set;

import com.bedrockminer.ascore.block.base.ICircuitElement;
import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.block.Block;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;

/**
 * Pipes have one single passive connection that can be connected on every side
 * of the TileEntity. They also have a color assigned to them. The color is
 * stored inside the connection, but can be accessed from the TileEntity.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class TilePipeBase extends TileCircuitElement {

	/** The connection used by this pipe */
	private Connection connection;

	/**
	 * Instantiates a new TilePipeBase and creates the connection for the pipe.
	 */
	public TilePipeBase() {
		this.add(this.connection = this.createPipeConnection());
	}

	// Circuit properties

	/**
	 * Returns the connection this pipe uses.
	 *
	 * @return the connection
	 */
	public final Connection getConnection() {
		return this.connection;
	}

	//@formatter:off
	/**
	 * This method returns every position the given connection can be
	 * used at. The connection will connect to other matching connections next
	 * to the given positions. The returned set contains every direction around
	 * the TileEntity.
	 *
	 * @param connection
	 * the connection
	 * @return the positions
	 */
	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(
					new BlockSide(this.getPos(), EnumFacing.NORTH),
					new BlockSide(this.getPos(), EnumFacing.EAST),
					new BlockSide(this.getPos(), EnumFacing.WEST),
					new BlockSide(this.getPos(), EnumFacing.SOUTH),
					new BlockSide(this.getPos(), EnumFacing.UP),
					new BlockSide(this.getPos(), EnumFacing.DOWN));

		return ImmutableSet.<BlockSide>of();
	}
	//@formatter:on

	/**
	 * This method is called when the object is constructed to create a new
	 * connection that will be used by the pipe. The connection needs to be
	 * passive.
	 *
	 * @return the connection
	 */
	public abstract Connection createPipeConnection();

	@Override
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
		if (connection == this.getConnection()) {
			// Make the pipe straight if only one side is connected.
			if (positions.size() == 1)
				positions.add(new BlockSide(positions.iterator().next().getPos(), positions.iterator().next().getSide().getOpposite()));

			// Create a connection if no side of the pipe is connected.
			if (positions.size() == 0) {
				// Checking the side with the least blocked connections
				// (blocked by different color but same type)
				Axis best = Axis.X;
				int leastblocked = 2;
				for (Axis test : Axis.values()) {
					BlockPos posA = this.getPos().offset(EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, test));
					BlockPos posB = this.getPos().offset(EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, test));
					Block blockA = this.getWorld().getBlockState(posA).getBlock();
					Block blockB = this.getWorld().getBlockState(posB).getBlock();
					int blocked = 0;
					if (blockA instanceof ICircuitElement && ((ICircuitElement) blockA).getTE(this.getWorld(), posA) != null) {
						// Getting a connection for block A and testing it's
						// color
						Connection c = ((ICircuitElement) blockA).getTE(this.getWorld(), posA).getConnection(new BlockSide(posA, EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, test)), this.getConnection().getConnectionType(), EnumDyeColor.BLACK);
						if (c != null && !Connection.doColorsMatch(c.getConnectionColor(), this.getPipeColor()))
							blocked++;
					}
					if (blockB instanceof ICircuitElement && ((ICircuitElement) blockB).getTE(this.getWorld(), posB) != null) {
						// Getting a connection for block B and testing it's
						// color
						Connection c = ((ICircuitElement) blockB).getTE(this.getWorld(), posB).getConnection(new BlockSide(posB, EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, test)), this.getConnection().getConnectionType(), EnumDyeColor.BLACK);
						if (c != null && !Connection.doColorsMatch(c.getConnectionColor(), this.getPipeColor()))
							blocked++;
					}

					if (blocked < leastblocked) {
						best = test;
						leastblocked = blocked;
					}
				}

				positions.add(new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, best)));
				positions.add(new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, best)));
			}
		}
	}

	/**
	 * Returns the sides the pipe is connected to.
	 *
	 * @return the sides with connections
	 */
	public Set<EnumFacing> getPipeConnections() {
		Set<EnumFacing> facings = new HashSet<EnumFacing>();
		Set<BlockSide> sides = this.getConnection().getConnectedPositions();
		for (BlockSide side : sides)
			if (side.getPos().equals(this.getPos()))
				facings.add(side.getSide());
		return facings;
	}

	// Pipe properties

	/**
	 * Returns the color of this pipe.
	 *
	 * @return the color
	 */
	public EnumDyeColor getPipeColor() {
		return this.getConnection().getConnectionColor();
	}

	/**
	 * Sets the color of this pipe.
	 *
	 * @param color
	 * the color to set
	 */
	public void setPipeColor(EnumDyeColor color) {
		this.getConnection().setColor(color);
		if (this.hasWorldObj())
			this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
	}

	// Saving NBT Data

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		this.getConnection().writeToNBT(nbt);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		this.getConnection().readFromNBT(nbt);
	}
}
