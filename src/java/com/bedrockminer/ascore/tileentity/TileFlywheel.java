package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.connections.RotationConnection;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;

public class TileFlywheel extends TileCircuitElement {

	/** The flywheel's orientation */
	private Axis orientation = Axis.X;
	/** The connection */
	private RotationConnection connection;

	public TileFlywheel() {
		this.add(this.connection = new RotationConnection(0, true, false, this));
		this.connection.setInertia(211.0);
		this.connection.setResistance(5.5);
	}

	/**
	 * Returns the rotation connection.
	 *
	 * @return the connection
	 */
	public RotationConnection getConnection() {
		return this.connection;
	}

	/**
	 * Returns the orientation of this flywheel.
	 *
	 * @return the orientation
	 */
	public Axis getOrientation() {
		return this.orientation;
	}

	/**
	 * Sets the orientation of this flywheel.
	 *
	 * @param orientation the orientation to set
	 */
	public void setOrientation(Axis orientation) {
		this.orientation = orientation;
		this.getConnection().invalidateAvailablePositions();
		if (!this.getWorld().isRemote) {
			if (this.getConnection().hasCircuit())
				this.getConnection().getCircuit().invalidate();
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, this.getOrientation())), new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, this.getOrientation())));
		return ImmutableSet.<BlockSide>of();
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];
		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];
		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();
		if (this.getWorld().isRemote)
			this.getConnection().tryUpdate();
	}
}
