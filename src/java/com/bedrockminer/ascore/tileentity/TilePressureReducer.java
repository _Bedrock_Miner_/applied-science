package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.circuits.PressureCircuit;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;

/**
 * The pressure reducer reduces the pressure on the output side.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TilePressureReducer extends TilePressurePipe {

	/** The pipe's orientation */
	private EnumFacing orientation = EnumFacing.EAST;
	/** The maximum pressure in bar */
	private double pressure = 5;

	/** The second pressure connection */
	private PressureConnection secondConnection;

	/**
	 * Instantiates a new TileSecurityValve
	 */
	public TilePressureReducer() {
		super();
		this.add(this.secondConnection = new PressureConnection(1, false, false, this));
	}

	@Override
	public Connection createPipeConnection() {
		return new PressureConnection(0, false, false, this);
	}

	/**
	 * Returns the first connection used for high pressure input.
	 *
	 * @return the first connection
	 */
	public PressureConnection getInput() {
		return (PressureConnection) this.getConnection();
	}

	/**
	 * Returns the second connection used for low pressure output.
	 *
	 * @return the second connection
	 */
	public PressureConnection getOutput() {
		return this.secondConnection;
	}

	@Override
	public void onAddingToCircuit(Connection connection) {
		this.getInput().setVolume(this.getPipeVolume() / 2);
		this.getOutput().setVolume(this.getPipeVolume() / 2);
	}

	// General properties

	/**
	 * Returns the orientation of this pipe
	 *
	 * @return the orientation
	 */
	public EnumFacing getOrientation() {
		return this.orientation;
	}

	/**
	 * Sets the pipe's orientation
	 *
	 * @param orientation
	 * the orientation to set
	 */
	public void setOrientation(EnumFacing orientation) {
		this.orientation = orientation;
		this.getInput().invalidateAvailablePositions();
		this.getOutput().invalidateAvailablePositions();

		if (!this.getWorld().isRemote) {
			if (this.getConnection().hasCircuit())
				this.getConnection().getCircuit().invalidate();
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns the pressure limit of this pipe.
	 *
	 * @return the pressure limit
	 */
	public double getPressureLimit() {
		return this.pressure;
	}

	/**
	 * Sets the pressure limit of this pipe.
	 *
	 * @param limit
	 * the pressure limit to set
	 */
	public void setPressureLimit(double limit) {
		if (limit < 1)
			limit = 1;
		this.pressure = limit;
	}

	@Override
	public double getPipeVolume() {
		return 4 * PIPE_RADIUS * PIPE_RADIUS;
	}

	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getInput())
			return ImmutableSet.of(new BlockSide(this.getPos(), this.getOrientation().getOpposite()));
		if (connection == this.getOutput())
			return ImmutableSet.of(new BlockSide(this.getPos(), this.getOrientation()));

		return ImmutableSet.<BlockSide> of();
	}

	@Override
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
		positions.clear();
		positions.addAll(this.getPositionsForConnection(connection));
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote) {
			PressureCircuit cI = this.getInput().getCircuit();
			PressureCircuit cO = this.getOutput().getCircuit();

			// Similar to the Air Flow Limiter
			if (cI.getPressure() < cO.getPressure() || (cO.getPressure() < this.getPressureLimit() && cI.getPressure() > cO.getPressure())) {
				double pges = (cI.getAirVolume() + cO.getAirVolume()) / (cI.getVolume() + cO.getVolume());
				if (cI.isLeaking() || cO.isLeaking())
					pges = 1;
				double dI = cI.getVolume() * (pges - cI.getPressure());
				double dO = cO.getVolume() * (pges - cO.getPressure());
				if (cI.isLeaking())
					dI = -dO;
				if (cO.isLeaking())
					dO = -dI;
				double flowlimit = 0.05 * (1 + Math.abs((cI.getPressure() + cO.getPressure()) / 2 - 1));

				if (cI.getPressure() < cO.getPressure()) {
					this.getInput().setAirChange(MathHelper.clamp_double(dI, -flowlimit, flowlimit));
					this.getOutput().setAirChange(MathHelper.clamp_double(dO, -flowlimit, flowlimit));

				} else if (cO.getPressure() < this.getPressureLimit()) {
					double maximum = this.getPressureLimit() * cO.getVolume() - cO.getAirVolume();

					this.getInput().setAirChange(MathHelper.clamp_double(dI, Math.max(-flowlimit, -maximum), flowlimit));
					this.getOutput().setAirChange(MathHelper.clamp_double(dO, -flowlimit, Math.min(flowlimit, maximum)));
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
		nbt.setDouble("PressureLimit", this.getPressureLimit());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 5 ? EnumFacing.EAST : EnumFacing.values()[b];
		this.pressure = nbt.getDouble("PressureLimit");

		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 5 ? EnumFacing.EAST : EnumFacing.values()[b];

		this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForWaila(NBTTagCompound nbt) {
		nbt.setDouble("Pressure", this.getInput().getPressure());
		nbt.setDouble("Limit", this.getPressureLimit());
	}
}
