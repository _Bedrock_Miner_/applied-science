package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.modintegration.waila.IWailaNBT;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class TilePressureStorage extends TileCircuitElement
		implements IWailaNBT {

	/** The connection used by this pipe */
	private PressureConnection connection;

	/**
	 * Instantiates a new TilePressureStorage
	 */
	public TilePressureStorage() {
		this.add(this.connection = new PressureConnection(0, true, false, this));
		this.connection.setVolume(1);
	}

	// Circuit properties

	/**
	 * Returns the connection this pipe uses.
	 *
	 * @return the connection
	 */
	public PressureConnection getConnection() {
		return this.connection;
	}

	//@formatter:off
	/**
	 * This method returns every position the given connection can be
	 * used at. The connection will connect to other matching connections next
	 * to the given positions. The returned set contains every direction around
	 * the TileEntity.
	 *
	 * @param connection
	 * the connection
	 * @return the positions
	 */
	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(
					new BlockSide(this.getPos(), EnumFacing.NORTH),
					new BlockSide(this.getPos(), EnumFacing.EAST),
					new BlockSide(this.getPos(), EnumFacing.WEST),
					new BlockSide(this.getPos(), EnumFacing.SOUTH),
					new BlockSide(this.getPos(), EnumFacing.UP),
					new BlockSide(this.getPos(), EnumFacing.DOWN));

		return ImmutableSet.<BlockSide>of();
	}
	//@formatter:on

	// Update

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote) {
			// The storage can blow up if the pressure is too high
			if (this.getConnection().getPressure() > this.getMaximumPressure()) {

				int diff = 4 * (int) (this.getConnection().getPressure() - this.getMaximumPressure());
				if (diff < 0) diff = 0;
				if (diff > 255) diff = 255;

				if (this.getWorld().rand.nextInt(256 - diff) == 0) {
					this.getWorld().setBlockToAir(this.getPos());
					this.getWorld().createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), (float) (1.5 + 0.2 * (this.getConnection().getPressure() - 0.9 * this.getMaximumPressure())), true);
				}
			}
		}
	}

	public double getMaximumPressure() {
		return 10;
	}

	// Saving NBT Data

	@Override
	public void writeToNBTForWaila(NBTTagCompound nbt) {
		nbt.setDouble("Pressure", this.getConnection().getPressure());
	}
}
