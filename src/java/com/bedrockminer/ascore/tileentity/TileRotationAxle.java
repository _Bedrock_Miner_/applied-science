package com.bedrockminer.ascore.tileentity;

import java.util.Iterator;
import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.connections.RotationConnection;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;
import com.bedrockminer.ascore.util.BlockSide;

/**
 * An axle transmitting rotational energy.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TileRotationAxle extends TilePipeBase {

	public TileRotationAxle() {
		super();
	}

	@Override
	public Connection createPipeConnection() {
		return new RotationConnection(0, true, false, this);
	}

	@Override
	public void onAddingToCircuit(Connection connection) {
		if (connection == this.getConnection()) {
			Set<BlockSide> connections = this.getConnection().getConnectedPositions();
			int connectionCount = connections.size();
			((RotationConnection)this.getConnection()).setInertia(0.24 * connectionCount);
			Iterator<BlockSide> it = connections.iterator();
			if (connectionCount == 2 && it.next().getSide().getOpposite() == it.next().getSide())
				((RotationConnection)this.getConnection()).setResistance(0.1 * connectionCount);
			else
				((RotationConnection)this.getConnection()).setResistance(0.2 + 0.1 * connectionCount);
		}
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();
		if (this.getWorld().isRemote) {
			this.getConnection().tryUpdate();
			this.onAddingToCircuit(this.getConnection());
		}
	}
}
