package com.bedrockminer.ascore.tileentity;

import com.bedrockminer.ascore.block.base.IMultiblockStructure;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IChatComponent;

/**
 * The class TileInventoryReference is used for multiblock structures to refer
 * to an inventory that is stored in a TileEntity at a different location.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TileInventoryReference extends TileEntity implements ISidedInventory {

	public boolean hasReference() {
		Block block = this.getWorld().getBlockState(this.getPos()).getBlock();
		if (block instanceof IMultiblockStructure) {
			TileEntity te = this.getWorld().getTileEntity(((IMultiblockStructure) block).getMainBlock(this.getWorld(), this.getPos()));
			if (te != null && te instanceof IInventory)
				return true;
		}
		return false;
	}

	public IInventory getReference() {
		Block block = this.getWorld().getBlockState(this.getPos()).getBlock();
		if (block instanceof IMultiblockStructure) {
			TileEntity te = this.getWorld().getTileEntity(((IMultiblockStructure) block).getMainBlock(this.getWorld(), this.getPos()));
			if (te != null && te instanceof IInventory)
				return (IInventory) te;
		}
		return null;
	}

	@Override
	public String getName() {
		if (this.hasReference())
			return this.getReference().getName();
		return "";
	}

	@Override
	public boolean hasCustomName() {
		if (this.hasReference())
			return this.getReference().hasCustomName();
		return false;
	}

	@Override
	public IChatComponent getDisplayName() {
		if (this.hasReference())
			return this.getReference().getDisplayName();
		return new ChatComponentText("");
	}

	@Override
	public int getSizeInventory() {
		if (this.hasReference())
			return this.getReference().getSizeInventory();
		return 0;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if (this.hasReference())
			return this.getReference().getStackInSlot(index);
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if (this.hasReference())
			return this.getReference().decrStackSize(index, count);
		return null;
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		if (this.hasReference())
			return this.getReference().removeStackFromSlot(index);
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if (this.hasReference())
			this.getReference().setInventorySlotContents(index, stack);
	}

	@Override
	public int getInventoryStackLimit() {
		if (this.hasReference())
			return this.getReference().getInventoryStackLimit();
		return 0;
	}

	@Override
	public void markDirty() {
		if (this.hasReference())
			this.getReference().markDirty();
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		if (this.hasReference())
			return this.getReference().isUseableByPlayer(player);
		return false;
	}

	@Override
	public void openInventory(EntityPlayer player) {
		if (this.hasReference())
			this.getReference().openInventory(player);
	}

	@Override
	public void closeInventory(EntityPlayer player) {
		if (this.hasReference())
			this.getReference().closeInventory(player);
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		if (this.hasReference())
			return this.getReference().isItemValidForSlot(index, stack);
		return false;
	}

	@Override
	public int getField(int id) {
		if (this.hasReference())
			return this.getReference().getField(id);
		return 0;
	}

	@Override
	public void setField(int id, int value) {
		if (this.hasReference())
			this.getReference().setField(id, value);
	}

	@Override
	public int getFieldCount() {
		if (this.hasReference())
			return this.getReference().getFieldCount();
		return 0;
	}

	@Override
	public void clear() {
		if (this.hasReference())
			this.getReference().clear();
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		if (this.hasReference() && this.getReference() instanceof ISidedInventory)
			return ((ISidedInventory) this.getReference()).getSlotsForFace(side);
		int [] slots = new int[this.getSizeInventory()];
		for (int i = 0; i < this.getSizeInventory(); i++)
			slots[i] = i;
		return slots;
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		if (this.hasReference()) {
			if (this.getReference() instanceof ISidedInventory) {
				return ((ISidedInventory) this.getReference()).canInsertItem(index, itemStackIn, direction);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		if (this.hasReference()) {
			if (this.getReference() instanceof ISidedInventory) {
				return ((ISidedInventory) this.getReference()).canExtractItem(index, stack, direction);
			}
			return true;
		}
		return false;
	}
}
