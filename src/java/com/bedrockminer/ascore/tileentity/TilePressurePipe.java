package com.bedrockminer.ascore.tileentity;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.modintegration.waila.IWailaNBT;
import com.bedrockminer.ascore.tileentity.base.TilePipeBase;

import net.minecraft.nbt.NBTTagCompound;

/**
 * A pipe transporting pressure.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TilePressurePipe extends TilePipeBase implements IWailaNBT {

	public static final double PIPE_RADIUS = 0.125;

	public TilePressurePipe() {
		super();
	}

	@Override
	public Connection createPipeConnection() {
		return new PressureConnection(0, true, false, this);
	}

	/**
	 * Returns the pressure in this pipe in bar. The returned
	 * pressure is absolute, thus the default value is 1.
	 * <p>
	 * <em>SERVER SIDE ONLY!</em>
	 *
	 * @return the pressure in this pipe.
	 */
	public double getPressure() {
		if (this.getWorld().isRemote)
			return 1.0;
		return ((PressureConnection) this.getConnection()).getPressure();
	}

	/**
	 * Returns the pipe's volume in m^3
	 *
	 * @return the volume
	 */
	public double getPipeVolume() {
		return 8 * Math.pow(PIPE_RADIUS, 3) + 4 * this.getConnection().getConnectedPositions().size() * PIPE_RADIUS * PIPE_RADIUS * (0.5 - PIPE_RADIUS);
	}

	/**
	 * Returns the maximum pressure this pipe can handle. If the pressure is
	 * higher, the pipe may explode.
	 *
	 * @return the maximum pressure
	 */
	public double getMaximumPressure() {
		return 10.0;
	}

	@Override
	public void onAddingToCircuit(Connection connection) {
		((PressureConnection)this.getConnection()).setVolume(this.getPipeVolume());
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();
		if (!this.getWorld().isRemote) {
			// The pipe can blow up if the pressure is too high
			if (this.getPressure() > this.getMaximumPressure()) {

				int diff = 4 * (int) (this.getPressure() - this.getMaximumPressure());
				if (diff < 0) diff = 0;
				if (diff > 127) diff = 127;

				if (this.getWorld().rand.nextInt(128 - diff) == 0) {
					this.getWorld().setBlockToAir(this.getPos());
					this.getWorld().createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), (float) (1.0 + 0.15 * (this.getPressure() - 0.9 * this.getMaximumPressure())), true);
				}
			}
		}
	}

	@Override
	public void writeToNBTForWaila(NBTTagCompound nbt) {
		nbt.setDouble("Pressure", ((PressureConnection)this.getConnection()).getPressure());
	}
}
