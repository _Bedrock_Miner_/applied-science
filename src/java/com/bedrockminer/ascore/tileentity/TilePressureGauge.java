package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.util.BlockSide;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

/**
 * The pressure gauge displays the current pressure to the user.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TilePressureGauge extends TilePressurePipe {

	/** The gauge's direction */
	private EnumFacing gaugeFacing = EnumFacing.NORTH;
	/** The gauge's orientation */
	private EnumFacing gaugeOrientation = EnumFacing.NORTH;
	/** The gauge's maximum display pressure */
	private double gaugeMaximum = 11;
	/** The pressure last tick */
	private double lastPressure = 0;
	/** The synchronized pressure */
	private double pressure;
	/** The last synchronization tick */
	private long lastSync;

	/** The minimum pressure for the gauge to emit a redstone signal */
	private double minRSPressure = this.getMaximumPressure();
	/** The maximum pressure for the gauge to emit a redstone signal */
	private double maxRSPressure = this.getMaximumPressure();
	/** The redstone mode. true is on/off, false is floating value */
	private boolean RSBinary = true;

	/**
	 * Instantiates a new TilePressureGauge
	 */
	public TilePressureGauge() {
		super();
	}

	@Override
	public Connection createPipeConnection() {
		return new PressureConnection(0, true, false, this);
	}

	// General properties

	/**
	 * Sets the facing of the gauge display.
	 *
	 * @param facing the gauge display
	 */
	public void setGaugeFacing(EnumFacing facing) {
		this.gaugeFacing = facing;
		if (!this.getWorld().isRemote) {
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns the facing of the gauge display.
	 *
	 * @return the gauge facing
	 */
	public EnumFacing getGaugeFacing() {
		return this.gaugeFacing;
	}

	/**
	 * Sets the gauge's orientation. This will be used only if the facing is
	 * UP or DOWN. It can be any horizontal facing.
	 *
	 * @param orientation the gauge's orientation
	 */
	public void setGaugeOrientation(EnumFacing orientation) {
		this.gaugeOrientation = orientation;
		if (!this.getWorld().isRemote) {
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns the gauge's orientation.
	 *
	 * @return the gauge's orientation
	 */
	public EnumFacing getGaugeOrientation() {
		return this.gaugeOrientation;
	}

	/**
	 * Sets the maximum pressure value the gauge can display in absolute
	 * bars. The scale always starts with atmosphere pressure and reaches up
	 * to this value.
	 *
	 * @param maximum the maximum pressure
	 */
	public void setGaugeMaximum(double maximum) {
		this.gaugeMaximum = maximum;
		if (!this.getWorld().isRemote)
			this.getWorld().markBlockForUpdate(this.getPos());
	}

	/**
	 * Returns the maximum pressure value the gauge can display in absolute
	 * bars. The scale always starts with atmosphere pressure and reaches up
	 * to this value.
	 *
	 * @return the maximum display value
	 */
	public double getGaugeMaximum() {
		return this.gaugeMaximum;
	}

	public int getRedstoneOutput() {
		if (this.isRedstoneBinary()) {
			if (this.getPressure() >= this.getMinRedstonePressure() && this.getPressure() <= this.getMaxRedstonePressure())
				return 15;
			return 0;
		} else {
			return (int) Math.floor((this.getPressure() - this.getMinRedstonePressure()) / (this.getMaxRedstonePressure() - this.getMinRedstonePressure() + 1) * 15) + 1;
		}
	}

	/**
	 * Returns whether the redstone output is binary (true) (on/off) or floating
	 * (false)
	 *
	 * @return whether the redstone is binary
	 */
	public boolean isRedstoneBinary() {
		return this.RSBinary;
	}

	/**
	 * Sets whether the redstone output is binary (true) (on/off) or floating
	 * (false)
	 *
	 * @param binary whether the redstone is binary
	 */
	public void setRedstoneBinary(boolean binary) {
		this.RSBinary = binary;
		if (!this.getWorld().isRemote)
			this.getWorld().notifyNeighborsOfStateChange(this.getPos(), this.getBlockType());
	}

	/**
	 * Returns the minimum pressure needed to produce a redstone signal.
	 *
	 * @return the minimum pressure
	 */
	public double getMinRedstonePressure() {
		return this.minRSPressure;
	}

	/**
	 * Sets the minimum pressure needed to produce a redstone signal.
	 *
	 * @param pressure the minimum pressure
	 */
	public void setMinRedstonePressure(double pressure) {
		this.minRSPressure = pressure;
		if (!this.getWorld().isRemote)
			this.getWorld().notifyNeighborsOfStateChange(this.getPos(), this.getBlockType());
	}

	/**
	 * Returns the maximum pressure allowed for a redstone signal.
	 *
	 * @return the maximum pressure
	 */
	public double getMaxRedstonePressure() {
		return this.maxRSPressure;
	}

	/**
	 * Sets the maximum pressure allowed for redstone signal.
	 *
	 * @param pressure the maximum pressure
	 */
	public void setMaxRedstonePressure(double pressure) {
		this.maxRSPressure = pressure;
		if (!this.getWorld().isRemote)
			this.getWorld().notifyNeighborsOfStateChange(this.getPos(), this.getBlockType());
	}

	@Override
	public double getPipeVolume() {
		return 4 * PIPE_RADIUS * PIPE_RADIUS;
	}

	@Override
	public double getPressure() {
		if (this.getWorld().isRemote)
			return this.pressure;
		return super.getPressure();
	}

	@Override
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote) {
			if (this.lastPressure != ((PressureConnection)this.getConnection()).getPressure()) {
				this.getWorld().notifyNeighborsOfStateChange(this.getPos(), this.getBlockType());
				if (this.lastSync < this.getWorld().getTotalWorldTime() - 5L) {
					this.getWorld().markBlockForUpdate(this.getPos());
					this.lastPressure = ((PressureConnection)this.getConnection()).getPressure();
					this.lastSync = this.getWorld().getTotalWorldTime();
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setByte("Gauge", (byte) this.gaugeFacing.ordinal());
		nbt.setByte("GaugeOrientation", (byte) this.gaugeOrientation.ordinal());
		nbt.setDouble("GaugeMaximum", this.gaugeMaximum);
		nbt.setBoolean("RedstoneBinary", this.RSBinary);
		nbt.setDouble("MinRedstonePressure", this.minRSPressure);
		nbt.setDouble("MaxRedstonePressure", this.maxRSPressure);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		byte b = nbt.getByte("Gauge");
		this.gaugeFacing = b < 0 || b > 5 ? EnumFacing.NORTH : EnumFacing.values()[b];
		b = nbt.getByte("GaugeOrientation");
		this.gaugeOrientation = b < 2 || b > 5 ? EnumFacing.NORTH : EnumFacing.values()[b];

		this.gaugeMaximum = nbt.getDouble("GaugeMaximum");
		this.RSBinary = nbt.getBoolean("RedstoneBinary");
		this.minRSPressure = nbt.getDouble("MinRedstonePressure");
		this.maxRSPressure = nbt.getDouble("MaxRedstonePressure");

		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setByte("Gauge", (byte) this.gaugeFacing.ordinal());
		nbt.setByte("GaugeOrientation", (byte) this.gaugeOrientation.ordinal());
		nbt.setDouble("GaugeMaximum", this.gaugeMaximum);
		nbt.setDouble("Pressure", this.getPressure());
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		byte b = nbt.getByte("Gauge");
		this.gaugeFacing = b < 0 || b > 5 ? EnumFacing.NORTH : EnumFacing.values()[b];
		b = nbt.getByte("GaugeOrientation");
		this.gaugeOrientation = b < 2 || b > 5 ? EnumFacing.NORTH : EnumFacing.values()[b];
		this.pressure = nbt.getDouble("Pressure");
		this.gaugeMaximum = nbt.getDouble("GaugeMaximum");

		this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
		this.getConnection().invalidateAvailablePositions();
	}
}
