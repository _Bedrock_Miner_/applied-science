package com.bedrockminer.ascore.tileentity;

import net.minecraftforge.fml.common.registry.GameRegistry;

public final class ASTileEntities {

	public static void init() {
		GameRegistry.registerTileEntity(TilePressurePipe.class, "pressure_pipe");
		GameRegistry.registerTileEntity(TilePressureValve.class, "pressure_valve");
		GameRegistry.registerTileEntity(TileSecurityValve.class, "security_valve");
		GameRegistry.registerTileEntity(TileAirFlowLimiter.class, "air_flow_limiter");
		GameRegistry.registerTileEntity(TilePressureGauge.class, "pressure_gauge");
		GameRegistry.registerTileEntity(TilePressureReducer.class, "pressure_reducer");
		GameRegistry.registerTileEntity(TilePressureStorage.class, "pressure_storage");
		GameRegistry.registerTileEntity(TileSteamBoiler.class, "steam_boiler");
		GameRegistry.registerTileEntity(TileCreativePressureGenerator.class, "creative_pressure_generator");

		GameRegistry.registerTileEntity(TileRotationAxle.class, "rotation_axle");
		GameRegistry.registerTileEntity(TileFlywheel.class, "flywheel");
		GameRegistry.registerTileEntity(TileCreativeMotor.class, "creative_motor");

		GameRegistry.registerTileEntity(TileInventoryReference.class, "inventory_reference");
	}

}
