package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.circuits.PressureCircuit;
import com.bedrockminer.ascore.circuitry.connections.PressureConnection;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;
import net.minecraft.util.MathHelper;

/**
 * The air flow limiter limits the amount of air flowing through it
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TileAirFlowLimiter extends TilePressurePipe {

	/** The pipe's orientation */
	private Axis orientation = Axis.X;
	/** The maximum flow in m3/t */
	private double flow = 0.05;

	/** The second pressure connection */
	private PressureConnection secondConnection;

	/**
	 * Instantiates a new TileSecurityValve
	 */
	public TileAirFlowLimiter() {
		super();
		this.add(this.secondConnection = new PressureConnection(1, false, false, this));
	}

	@Override
	public Connection createPipeConnection() {
		return new PressureConnection(0, false, false, this);
	}

	/**
	 * Returns the first connection.
	 *
	 * @return the first connection
	 */
	public PressureConnection getConnectionA() {
		return (PressureConnection) this.getConnection();
	}

	/**
	 * Returns the second connection.
	 *
	 * @return the second connection
	 */
	public PressureConnection getConnectionB() {
		return this.secondConnection;
	}

	@Override
	public void onAddingToCircuit(Connection connection) {
		this.getConnectionA().setVolume(this.getPipeVolume() / 2);
		this.getConnectionB().setVolume(this.getPipeVolume() / 2);
	}

	// General properties

	/**
	 * Returns the orientation of this pipe
	 *
	 * @return the orientation
	 */
	public Axis getOrientation() {
		return this.orientation;
	}

	/**
	 * Sets the pipe's orientation
	 *
	 * @param orientation
	 * the orientation to set
	 */
	public void setOrientation(Axis orientation) {
		this.orientation = orientation;
		this.getConnectionA().invalidateAvailablePositions();
		this.getConnectionB().invalidateAvailablePositions();

		if (!this.getWorld().isRemote) {
			if (this.getConnection().hasCircuit())
				this.getConnection().getCircuit().invalidate();
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns the flow limit of this pipe.
	 *
	 * @return the flow limit
	 */
	public double getFlowLimit() {
		return this.flow;
	}

	/**
	 * Sets the flow limit of this pipe.
	 *
	 * @param limit
	 * the flow limit to set
	 */
	public void setFlowLimit(double limit) {
		this.flow = limit;
	}

	/**
	 * Returns the maximum flow limit for this pipe.
	 *
	 * @return the maximum flow limit
	 */
	public double getMaxFlowLimit() {
		return 5.0;
	}

	@Override
	public double getPipeVolume() {
		return 4 * PIPE_RADIUS * PIPE_RADIUS;
	}

	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnectionA())
			return ImmutableSet.of(new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, this.getOrientation())));
		if (connection == this.getConnectionB())
			return ImmutableSet.of(new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, this.getOrientation())));

		return ImmutableSet.<BlockSide> of();
	}

	@Override
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
		positions.clear();
		positions.addAll(this.getPositionsForConnection(connection));
	}

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote) {
			PressureCircuit cA = this.getConnectionA().getCircuit();
			PressureCircuit cB = this.getConnectionB().getCircuit();
			if (cA.getPressure() != cB.getPressure()) {
				double pges = (cA.getAirVolume() + cB.getAirVolume()) / (cA.getVolume() + cB.getVolume());
				if (cA.isLeaking() || cB.isLeaking())
					pges = 1;
				double dA = cA.getVolume() * (pges - cA.getPressure());
				double dB = cB.getVolume() * (pges - cB.getPressure());
				if (cA.isLeaking())
					dA = -dB;
				if (cB.isLeaking())
					dB = -dA;

				double pflow = 1 + Math.abs((cA.getPressure() + cB.getPressure()) / 2 - 1);
				this.getConnectionA().setAirChange(MathHelper.clamp_double(dA, -this.getFlowLimit() * pflow, this.getFlowLimit() * pflow));
				this.getConnectionB().setAirChange(MathHelper.clamp_double(dB, -this.getFlowLimit() * pflow, this.getFlowLimit() * pflow));
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
		nbt.setDouble("FlowLimit", this.getFlowLimit());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];
		this.flow = nbt.getDouble("FlowLimit");

		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];

		this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForWaila(NBTTagCompound nbt) {
		double pA = this.getConnectionA().getPressure();
		double pB = this.getConnectionB().getPressure();
		nbt.setDouble("Pressure", Math.max(pA, pB));
		nbt.setDouble("Limit", this.getFlowLimit());
	}
}
