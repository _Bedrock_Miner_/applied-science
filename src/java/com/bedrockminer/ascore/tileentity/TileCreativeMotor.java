package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.circuitry.circuits.RotationCircuit;
import com.bedrockminer.ascore.circuitry.connections.RotationConnection;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class TileCreativeMotor extends TileCircuitElement {

	/** The RotationConnection of this generator */
	private RotationConnection connection;

	/** The maximum speed this generator will reach */
	private double maximumSpeed = 2 * Math.PI;
	/** The torque applied until the maximum is reached */
	private double torque = 200;

	/**
	 * Instantiates a new TileCreativePressureGenerator
	 */
	public TileCreativeMotor() {
		super();
		this.add(this.connection = new RotationConnection(0, false, true, this));
		this.connection.setResistance(2.0D);
		this.connection.setInertia(50.0D);
	}

	/**
	 * Returns the maximum speed this generator will reach
	 *
	 * @return the maximum speed
	 */
	public double getMaximumSpeed() {
		return this.maximumSpeed;
	}

	/**
	 * Returns the very maximum speed that can be reached.
	 *
	 * @return the very maximum speed
	 */
	public double getSpeedTreshold() {
		return 1000 * Math.PI;
	}

	/**
	 * Returns the torque this generator applies to the circuit
	 *
	 * @return the torque
	 */
	public double getTorque() {
		return this.torque;
	}

	/**
	 * Returns the maximum torque that can be applied.
	 *
	 * @return the maximum torque
	 */
	public double getTorqueTreshold() {
		return 10000;
	}

	/**
	 * Sets the maximum speed this generator will reach
	 *
	 * @param speed
	 * the maximum speed
	 */
	public void setMaximumSpeed(double speed) {
		if (speed < 0) speed = 0;
		if (speed > this.getSpeedTreshold()) speed = this.getSpeedTreshold();
		this.maximumSpeed = speed;
		this.getWorld().markBlockForUpdate(this.getPos());
	}

	/**
	 * Sets the torque this generator applies
	 *
	 * @param torque
	 * the torque
	 */
	public void setTorque(double torque) {
		if (torque < 0) torque = 0;
		if (torque > this.getTorqueTreshold()) torque = this.getTorqueTreshold();
		this.torque = torque;
		this.getWorld().markBlockForUpdate(this.getPos());
	}

	@Override
	public boolean canPlayerModify(EntityPlayer player) {
		return super.canPlayerModify(player) && player.capabilities.isCreativeMode;
	}

	/**
	 * Returns the RotationConnection of this generator.
	 *
	 * @return the connection
	 */
	public RotationConnection getConnection() {
		return this.connection;
	}

	//@formatter:off
	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(
					new BlockSide(this.getPos(), EnumFacing.NORTH),
					new BlockSide(this.getPos(), EnumFacing.EAST),
					new BlockSide(this.getPos(), EnumFacing.WEST),
					new BlockSide(this.getPos(), EnumFacing.SOUTH),
					new BlockSide(this.getPos(), EnumFacing.UP),
					new BlockSide(this.getPos(), EnumFacing.DOWN));

		return ImmutableSet.<BlockSide>of();
	}
	//@formatter:on

	@Override
	protected void doUpdate() {
		super.doUpdate();

		if (!this.getWorld().isRemote && this.getMaximumSpeed() > 0 && this.getTorque() > 0) {
			RotationCircuit rc = this.getConnection().getCircuit();
			if (rc != null && rc.isValid()) {
				if (rc.getElementCount() > 1) {
					this.getConnection().setResistance(2);
					double resistance = 0;
					Set<Connection> connections = rc.getAllConnections();
					for (Connection c: connections)
						resistance += ((RotationConnection)c).getResistance();
					double maxTorque = (this.getMaximumSpeed() - rc.getSpeed()) * rc.getInertia() + resistance;
					if (maxTorque < 0)
						maxTorque = 0;
					if (this.getConnection().getRotationSpeed() < this.getMaximumSpeed())
						this.getConnection().setTorque(Math.min(this.getTorque(), maxTorque));
				} else {
					this.getConnection().setResistance(this.getConnection().getRotationSpeed() / this.getConnection().getInertia());
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setDouble("MaxSpeed", this.maximumSpeed);
		nbt.setDouble("Torque", this.torque);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		this.maximumSpeed = nbt.getDouble("MaxSpeed");
		this.torque = nbt.getDouble("Torque");
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setDouble("MaxSpeed", this.maximumSpeed);
		nbt.setDouble("Torque", this.torque);
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		this.maximumSpeed = nbt.getDouble("MaxSpeed");
		this.torque = nbt.getDouble("Torque");
	}
}
