package com.bedrockminer.ascore.tileentity;

import java.util.Set;

import com.bedrockminer.ascore.circuitry.Connection;
import com.bedrockminer.ascore.util.BlockSide;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing.AxisDirection;

/**
 * The pressure valve can be used to cut off a pressure circuit at some point.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class TilePressureValve extends TilePressurePipe {

	/** <code>true</code> if the valve is closed */
	private boolean closed;
	/** <code>true</code> if the valve is redstone powered */
	private boolean powered;
	/** The pipe's orientation */
	private Axis orientation = Axis.X;
	/** The handle's side */
	private EnumFacing handle = EnumFacing.UP;

	/**
	 * Instantiates a new TilePressureValve
	 */
	public TilePressureValve() {
		super();
	}

	// General properties

	/**
	 * Returns whether the valve is closed.
	 *
	 * @return <code>true</code> if closed
	 */
	public boolean isClosed() {
		return this.closed;
	}

	/**
	 * Sets this valve closed.
	 *
	 * @param closed <code>true</code> to set the valve closed
	 */
	public void setClosed(boolean closed) {
		this.closed = closed;

		if (!this.getWorld().isRemote) {
			this.getConnection().assembleCircuit();
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns <code>true</code> if the valve is redstone powered.
	 *
	 * @return <code>true</code> if powered.
	 */
	public boolean isPowered() {
		return this.powered;
	}

	/**
	 * Called when a neighbor block changed. If the redstone power switched,
	 * the valve opens or closes respectively.
	 *
	 * @param powered <code>true</code> if the valve is redstone powered.
	 */
	public void redstoneChanged(boolean powered) {
		if (powered != this.powered)
			this.setClosed(!powered);
		this.powered = powered;
	}

	/**
	 * Returns the orientation of this pipe
	 *
	 * @return the orientation
	 */
	public Axis getOrientation() {
		return this.orientation;
	}

	/**
	 * Sets the pipe's orientation
	 *
	 * @param orientation the orientation to set
	 */
	public void setOrientation(Axis orientation) {
		this.orientation = orientation;
		this.getConnection().invalidateAvailablePositions();

		if (!this.getWorld().isRemote) {
			if (this.getConnection().hasCircuit())
				this.getConnection().getCircuit().invalidate();
			this.getWorld().markBlockForUpdate(this.getPos());
			this.markDirty();
		}
	}

	/**
	 * Returns the direction of the handle
	 *
	 * @return the handle direction
	 */
	public EnumFacing getHandleFacing() {
		return this.handle;
	}

	/**
	 * Sets the direction of the handle
	 *
	 * @param direction the handle direction to set
	 */
	public void setHandleFacing(EnumFacing direction) {
		this.handle = direction;
	}

	@Override
	public double getPressure() {
		return this.isClosed() ? 1 : super.getPressure();
	}

	@Override
	public double getPipeVolume() {
		return 4 * PIPE_RADIUS * PIPE_RADIUS;
	}

	//@formatter:off
	@Override
	public Set<BlockSide> getPositionsForConnection(Connection connection) {
		if (connection == this.getConnection())
			return ImmutableSet.of(
					new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.POSITIVE, this.getOrientation())),
					new BlockSide(this.getPos(), EnumFacing.getFacingFromAxis(AxisDirection.NEGATIVE, this.getOrientation())));

		return ImmutableSet.<BlockSide>of();
	}

	@Override
	public void reviewConnections(Connection connection, Set<BlockSide> positions) {
		if (connection == this.getConnection()) {
			if (this.isClosed()) {
				positions.clear();
			} else {
				positions.clear();
				positions.addAll(this.getPositionsForConnection(connection));
			}
		}
	}

	@Override
	protected void doUpdate() {
		this.markUpdated();
		if (!this.getWorld().isRemote)
			this.getConnection().tryUpdate();

		if (!this.getWorld().isRemote && !this.isClosed()) {
			// The pipe can blow up if the pressure is too high
			if (this.getPressure() > this.getMaximumPressure()) {

				int diff = 4 * (int) (this.getPressure() - this.getMaximumPressure());
				if (diff < 0) diff = 0;
				if (diff > 127) diff = 127;

				if (this.getWorld().rand.nextInt(128 - diff) == 0) {
					this.getWorld().setBlockToAir(this.getPos());
					this.getWorld().createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), (float) (1.0 + 0.15 * (this.getPressure() - 0.9 * this.getMaximumPressure())), true);
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setBoolean("Closed", this.closed);
		nbt.setBoolean("Powered", this.powered);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
		nbt.setByte("Handle", (byte) this.handle.ordinal());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];
		b = nbt.getByte("Handle");
		this.handle = b < 0 || b > 5 ? EnumFacing.UP : EnumFacing.values()[b];
		this.closed = nbt.getBoolean("Closed");
		this.powered = nbt.getBoolean("Powered");

		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void readFromNBTForSync(NBTTagCompound nbt) {
		super.readFromNBTForSync(nbt);
		byte b = nbt.getByte("Orientation");
		this.orientation = b < 0 || b > 2 ? Axis.X : Axis.values()[b];
		b = nbt.getByte("Handle");
		this.handle = b < 0 || b > 5 ? EnumFacing.UP : EnumFacing.values()[b];
		this.closed = nbt.getBoolean("Closed");

		this.getWorld().markBlockRangeForRenderUpdate(this.getPos(), this.getPos());
		this.getConnection().invalidateAvailablePositions();
	}

	@Override
	public void writeToNBTForSync(NBTTagCompound nbt) {
		super.writeToNBTForSync(nbt);
		nbt.setBoolean("Closed", this.closed);
		nbt.setByte("Orientation", (byte) this.orientation.ordinal());
		nbt.setByte("Handle", (byte) this.handle.ordinal());
	}
}
