package com.bedrockminer.ascore.units;

/**
 * A complex unit is not formatted automatically, but can format itself.
 * @author _Bedrock_Miner_ (minedbedrock@gmail.com)
 */
public interface IComplexUnit extends IUnit {

	/**
	 * Formats the value.
	 *
	 * @param value the value in the internal unit
	 * @return the formatted value
	 */
	public String format(double value);
}
