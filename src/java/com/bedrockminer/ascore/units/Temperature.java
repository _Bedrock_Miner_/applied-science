package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum Temperature implements IUnit {
	CELSIUS("Celsius", 1, 0, "%s \u00B0C"),
	FAHRENHEIT("Fahrenheit", 1.8, 32.0, "%s \u00B0F"),
	KELVIN("Kelvin", 1, 273.15, "%s K");

	public static final DecimalFormat FORMAT = new DecimalFormat("+#.0;-#");

	private double factor;
	private double linear;
	private String name;
	private String format;

	private Temperature(String name, double factor, double linear, String format) {
		this.name = name;
		this.factor = factor;
		this.linear = linear;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return this.linear;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return FORMAT;
	}

	@Override
	public Temperature getUnitByName(String name) {
		for (Temperature p: values()) {
			if (p.getUnitName().equalsIgnoreCase(name))
				return p;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		Temperature p = this.getUnitByName(Cfg.unitTemperature.getString());
		if (p == null)
			p = Temperature.CELSIUS;
		return p;
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}
}
