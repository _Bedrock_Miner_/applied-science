package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum Torque implements IUnit {
	NEWTONMETER("Newton Meter", 1, "%s Nm"),
	FOOTPOUNDS("Foot Pounds Force", 0.737562, "%s lbf ft");

	private double factor;
	private String name;
	private String format;

	private Torque(String name, double factor, String format) {
		this.name = name;
		this.factor = factor;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return 0;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return null;
	}

	@Override
	public Torque getUnitByName(String name) {
		for (Torque v: values()) {
			if (v.getUnitName().equalsIgnoreCase(name))
				return v;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		Torque v = this.getUnitByName(Cfg.unitTorque.getString());
		if (v == null)
			v = Torque.NEWTONMETER;
		return v;
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}
}
