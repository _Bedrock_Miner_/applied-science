package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum FluidVolume implements IUnit {
	CUBICMETER("Cubic Meter", 1, "%s m\u00b3"),
	LITER("Liter", 1000, "%s l"),
	BUCKETS("Buckets", 32, "%s B"),
	MILLIBUCKETS("Millibuckets", 32000, "%s mB"),
	USGALLON("US Gallon", 264.17205, "%s gal"),
	UKGALLON("UK Gallon", 219.96925, "%s gal");

	private static final DecimalFormat MILLIBUCKETSFORMAT = new DecimalFormat("#");
	private static final DecimalFormat LITERFORMAT = new DecimalFormat("#.#");

	private double factor;
	private String name;
	private String format;

	private FluidVolume(String name, double factor, String format) {
		this.name = name;
		this.factor = factor;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return 0;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return this == FluidVolume.MILLIBUCKETS ? MILLIBUCKETSFORMAT : (this == LITER ? LITERFORMAT : null);
	}

	@Override
	public FluidVolume getUnitByName(String name) {
		for (FluidVolume v: values()) {
			if (v.getUnitName().equalsIgnoreCase(name))
				return v;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		FluidVolume v = this.getUnitByName(Cfg.unitFluidVolume.getString());
		if (v == null)
			v = FluidVolume.CUBICMETER;
		return v;
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}
}
