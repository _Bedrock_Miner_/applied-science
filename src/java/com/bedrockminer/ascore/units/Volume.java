package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum Volume implements IUnit {
	CUBICMETER("Cubic Meter", 1, "%s m\u00b3"),
	LITER("Liter", 1000, "%s l"),
	USGALLON("US Gallon", 264.17205, "%s gal"),
	UKGALLON("UK Gallon", 219.96925, "%s gal");

	private double factor;
	private String name;
	private String format;

	private Volume(String name, double factor, String format) {
		this.name = name;
		this.factor = factor;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return 0;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return null;
	}

	@Override
	public Volume getUnitByName(String name) {
		for (Volume v: values()) {
			if (v.getUnitName().equalsIgnoreCase(name))
				return v;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		Volume v = this.getUnitByName(Cfg.unitVolume.getString());
		if (v == null)
			v = Volume.CUBICMETER;
		return v;
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}
}
