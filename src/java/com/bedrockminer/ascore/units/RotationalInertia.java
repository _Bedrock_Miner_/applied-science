package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum RotationalInertia implements IUnit {

	KGM2("Kilogram Square Meter", 1, "%s kg m\u00b2"),
	LBFT2("Pound Square Feet", 23.73036, "%s lb ft\u00b2");

	private double factor;
	private String name;
	private String format;

	private RotationalInertia(String name, double factor, String format) {
		this.name = name;
		this.factor = factor;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return 0;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return null;
	}

	@Override
	public RotationalInertia getUnitByName(String name) {
		for (RotationalInertia v: values()) {
			if (v.getUnitName().equalsIgnoreCase(name))
				return v;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		RotationalInertia v = this.getUnitByName(Cfg.unitRotInertia.getString());
		if (v == null)
			v = RotationalInertia.KGM2;
		return v;
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}
}
