package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

/**
 * This class is used to convert physical units into each other and format them.
 * This is used when displaying the status of machines. Users can select in the
 * Config which units they want to use.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class Units {

	/** The default decimal format */
	public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

	/**
	 * Converts the value to the given unit and formats it following the
	 * formatting rules.
	 *
	 * @param value
	 * the value to format. The value's unit must be the internal unit
	 * @param convert
	 * the unit to convert this value to
	 * @return the converted and formatted unit
	 */
	public static String format(double value, IUnit convert) {
		if (convert instanceof IComplexUnit)
			return ((IComplexUnit) convert).format(value);
		DecimalFormat d = convert.getSpecialNumberFormat();
		if (d == null)
			d = DECIMAL_FORMAT;
		return String.format(convert.getFormatString(), d.format(convertInternalTo(value, convert)));
	}

	/**
	 * Converts the value to the given unit and formats it with the given
	 * formatter.
	 *
	 * @param value
	 * the value to format. The value's unit must be the internal unit
	 * @param convert
	 * the unit to convert this value to
	 * @param format
	 * the number formatter
	 * @return the converted and formatted unit
	 */
	public static String format(double value, IUnit convert, DecimalFormat format) {
		if (convert instanceof IComplexUnit)
			return ((IComplexUnit) convert).format(value);
		return String.format(convert.getFormatString(), format.format(convertInternalTo(value, convert)));
	}

	/**
	 * Converts the value to the unit currently selected in the config and
	 * formats it. The unit type is defined by the internal unit passed as an
	 * argument.
	 *
	 * @param value
	 * the value to format
	 * @param internal
	 * the internal unit
	 * @return the converted and formatted unit
	 */
	public static String formatFrom(double value, IUnit internal) {
		return format(value, getUnitFromConfig(internal));
	}

	/**
	 * Converts the value to the unit currently selected in the config and
	 * formats it with the given formatter. The unit type is defined by the
	 * internal unit passed as an argument.
	 *
	 * @param value
	 * the value to format
	 * @param internal
	 * the internal unit
	 * @param format
	 * the formatter
	 * @return the converted and formatted unit
	 */
	public static String formatFrom(double value, IUnit internal, DecimalFormat format) {
		return format(value, getUnitFromConfig(internal), format);
	}

	/**
	 * Converts the given value from the internal to the given unit. This does
	 * not work for complex units like {@link Time#HMS} format.
	 *
	 * @param value
	 * the value (internal unit)
	 * @param unit
	 * the unit
	 * @return the converted value
	 */
	public static double convertInternalTo(double value, IUnit convert) {
		return convert.getFactor() * value + convert.getLinearShift();
	}

	/**
	 * Converts the given value difference from the internal to the given unit.
	 * This does not work for complex units like {@link Time#HMS} format.
	 *
	 * @param value
	 * the value difference (internal unit)
	 * @param unit
	 * the unit
	 * @return the converted value difference
	 */
	public static double convertInternalDiffTo(double value, IUnit convert) {
		return convert.getFactor() * value;
	}

	/**
	 * Converts the given value from the given internal unit to the user
	 * selected unit.
	 *
	 * @param value
	 * the value (internal unit)
	 * @param internal
	 * the internal unit
	 * @return the converted value
	 */
	public static double convertFromInternal(double value, IUnit internal) {
		return convertInternalTo(value, getUnitFromConfig(internal));
	}

	/**
	 * Converts the given value difference from the given internal unit to the
	 * user selected unit.
	 *
	 * @param value
	 * the value difference (internal unit)
	 * @param internal
	 * the internal unit
	 * @return the converted value difference
	 */
	public static double convertDiffFromInternal(double value, IUnit internal) {
		return convertInternalDiffTo(value, getUnitFromConfig(internal));
	}

	/**
	 * Converts the given value from the given to the internal unit. This does
	 * not work for complex units like {@link Time#HMS} format.
	 *
	 * @param value
	 * the value (given unit)
	 * @param unit
	 * the unit
	 * @return the converted value
	 */
	public static double convertUserTo(double value, IUnit convert) {
		return (value - convert.getLinearShift()) / convert.getFactor();
	}

	/**
	 * Converts the given value difference from the given to the internal unit.
	 * This does not work for complex units like {@link Time#HMS} format.
	 *
	 * @param value
	 * the value difference (given unit)
	 * @param unit
	 * the unit
	 * @return the converted value difference
	 */
	public static double convertUserDiffTo(double value, IUnit convert) {
		return value / convert.getFactor();
	}

	/**
	 * Converts the given value from the user selected unit to the given
	 * internal unit.
	 *
	 * @param value
	 * the value (user unit)
	 * @param internal
	 * the internal unit
	 * @return the converted value
	 */
	public static double convertFromUser(double value, IUnit internal) {
		return convertUserTo(value, getUnitFromConfig(internal));
	}

	/**
	 * Converts the given value difference from the user selected unit to the
	 * given internal unit.
	 *
	 * @param value
	 * the value difference (user unit)
	 * @param internal
	 * the internal unit
	 * @return the converted value difference
	 */
	public static double convertDiffFromUser(double value, IUnit internal) {
		return convertUserDiffTo(value, getUnitFromConfig(internal));
	}

	/**
	 * Returns the unit currently selected in the config file
	 *
	 * @param internal
	 * the internal unit
	 * @return the selected unit
	 */
	public static IUnit getUnitFromConfig(IUnit internal) {
		return internal.getUnitFromConfig();
	}
}
