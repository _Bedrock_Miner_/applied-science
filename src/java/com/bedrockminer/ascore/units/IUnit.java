package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

/**
 * This interface needs to be implemented by every unit used in the mod.
 * It defines basic methods necessary for the calculation.
 * <p>
 * <em>Each unit needs an internal unit with a factor of 1 and a linear shift
 * of 0. This must be the unit the system uses for calculations</em>
 *
 * @author _Bedrock_Miner_ (minedbedrock@gmail.com)
 */
public interface IUnit {

	/**
	 * Returns the conversion factor between this and the internal unit.<br>
	 * {@code this_unit = getFactor() * internal_unit}
	 *
	 * @return the conversion factor
	 */
	public double getFactor();

	/**
	 * Returns a linear value shift between this and the internal unit. The unit
	 * of the linear shift is this unit, not the internal one.
	 *
	 * @return the linear shift
	 */
	public double getLinearShift();

	/**
	 * Returns the full unit name.
	 *
	 * @return the unit name
	 */
	public String getUnitName();

	/**
	 * This string is used to add prefixes or suffixes to the unit's value.
	 * It should contain the unit's abbreviation as a prefix or suffix.
	 * <p>
	 * Example:<br>
	 * Unit "Kilogram": Format String "%s kg"<br>
	 * Unit "Meter":	Format String "%s m"
	 *
	 * @return the format string
	 */
	public String getFormatString();

	/**
	 * If this unit uses a specific DecimalFormat, this method needs to return
	 * it. Otherwise, just return <code>null</code> to use the default
	 * DecimalFormat.
	 *
	 * @return a special DecimalFormat
	 */
	public DecimalFormat getSpecialNumberFormat();

	/**
	 * Returns a unit by its name
	 *
	 * @param name the name
	 * @return the unit
	 */
	public IUnit getUnitByName(String name);

	/**
	 * Returns the unit currently selected in the config.
	 *
	 * @return the unit
	 */
	public IUnit getUnitFromConfig();
}
