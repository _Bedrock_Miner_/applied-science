package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum Time implements IComplexUnit {
	TICKS("Ticks"),
	SECONDS("Seconds"),
	HMS("Hours/Minutes/Seconds");

	private String name;

	private Time(String name) {
		this.name = name;
	}

	@Override
	public double getFactor() {
		return 0;
	}

	@Override
	public double getLinearShift() {
		return 0;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return "";
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return null;
	}

	@Override
	public Time getUnitByName(String name) {
		for (Time p: values()) {
			if (p.getUnitName().equalsIgnoreCase(name))
				return p;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		Time p = this.getUnitByName(Cfg.unitTime.getString());
		if (p == null)
			p = Time.HMS;
		return p;
	}

	@Override
	public String format(double value) {
		switch (this) {
		case HMS:
			if (value > 72000) // hours
				return String.format("%sh %smin %ss", ((int)value) / 72000, ((int)value) % 72000 / 1200, ((int)value) % 1200 / 20);
			if (value > 1200) // minutes
				return String.format("%smin %ss", ((int)value) / 1200, ((int)value) % 1200 / 20);
			//$FALL-THROUGH$  seconds
		case SECONDS:
			return String.format("%ss", (int) (value / 20));
		case TICKS:
			return String.format("%sT", (int) value);
		}
		return Double.toString(value);
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}

}
