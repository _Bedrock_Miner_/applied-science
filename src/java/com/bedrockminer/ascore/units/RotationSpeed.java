package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum RotationSpeed implements IUnit {

	INTERNAL("internal", 1, "%s rad/s"),
	RPM("Revolutions per Minute", 30 / Math.PI, "%s rpm"),
	RPS("Revolutions per Second", 0.5 / Math.PI, "%s rev/s"),
	RADPS("Radian per Second", 1 / Math.PI, "%s\u00d7\u03c0 rad/s");

	private double factor;
	private String name;
	private String format;

	private RotationSpeed(String name, double factor, String format) {
		this.name = name;
		this.factor = factor;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return 0;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		return null;
	}

	@Override
	public RotationSpeed getUnitByName(String name) {
		for (RotationSpeed p: values()) {
			if (p.getUnitName().equalsIgnoreCase(name))
				return p;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		RotationSpeed p = this.getUnitByName(Cfg.unitRotationSpeed.getString());
		if (p == null)
			p = RotationSpeed.RPM;
		return p;
	}

	public static String[] getNames() {
		String[] names = new String[values().length - 1];
		for (int i = 1; i < values().length; i++) {
			names[i - 1] = values()[i].getUnitName();
		}
		return names;
	}

}
