package com.bedrockminer.ascore.units;

import java.text.DecimalFormat;

import com.bedrockminer.ascore.config.Cfg;

public enum Pressure implements IUnit {
	BAR("Bar", 1, 0, "%s bar"),
	RELATIVEBAR("Bar (relative)", 1, -1, "%s bar"),
	PSI("Psi", 14.50377, 0, "%s psi"),
	RELATIVEPSI("Psi (relative)", 14.50377, -14.50377, "%s psi");

	public static final DecimalFormat RELATIVE_FORMAT = new DecimalFormat("+#.###;-#");

	private double factor;
	private double linear;
	private String name;
	private String format;

	private Pressure(String name, double factor, double linear, String format) {
		this.name = name;
		this.factor = factor;
		this.linear = linear;
		this.format = format;
	}

	@Override
	public double getFactor() {
		return this.factor;
	}

	@Override
	public double getLinearShift() {
		return this.linear;
	}

	@Override
	public String getUnitName() {
		return this.name;
	}

	@Override
	public String getFormatString() {
		return this.format;
	}

	@Override
	public DecimalFormat getSpecialNumberFormat() {
		if (this == Pressure.RELATIVEBAR || this == RELATIVEPSI)
			return RELATIVE_FORMAT;
		return null;
	}

	@Override
	public Pressure getUnitByName(String name) {
		for (Pressure p: values()) {
			if (p.getUnitName().equalsIgnoreCase(name))
				return p;
		}
		return null;
	}

	@Override
	public IUnit getUnitFromConfig() {
		Pressure p = this.getUnitByName(Cfg.unitPressure.getString());
		if (p == null)
			p = Pressure.BAR;
		return p;
	}

	public static String[] getNames() {
		String[] names = new String[values().length];
		for (int i = 0; i < values().length; i++) {
			names[i] = values()[i].getUnitName();
		}
		return names;
	}

}
