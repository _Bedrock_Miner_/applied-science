package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.tileentity.TileCreativeMotor;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;

/**
 * The Container for the Creative Motor.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerCreativeMotor extends ContainerBase<TileCreativeMotor> {

	// Synchronized Data
	public double maxSpeed;
	public double torque;
	public double speed;

	// Synchronized Connection Data
	public boolean connectionNotConnected;
	public NBTTagCompound connectionData;

	public ContainerCreativeMotor(TileCreativeMotor te, EntityPlayer player) {
		super(te, player);
	}

	@Override
	protected void initInventorySlots() {
	}

	@Override
	protected int getSyncFieldCount() {
		return 5;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getMaximumSpeed());
		case 1:
			return new NBTTagDouble(this.te().getTorque());
		case 2:
			return new NBTTagDouble(this.te().getConnection().getRotationSpeed());
		case 3:
			NBTTagCompound nbt = new NBTTagCompound();
			this.te().getConnection().writeToNBTForSettingsGUI(nbt);
			return nbt;
		case 4:
			return new NBTTagByte(this.te().getConnection().getCircuit().getElementCount() > 1 ? (byte) 0 : (byte) 1);
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch (ID) {
			case 0:
				this.maxSpeed = ((NBTTagDouble)data).getDouble();
				break;
			case 1:
				this.torque = ((NBTTagDouble)data).getDouble();
				break;
			case 2:
				this.speed = ((NBTTagDouble)data).getDouble();
				break;
			case 3:
				this.connectionData = (NBTTagCompound) data;
				break;
			case 4:
				this.connectionNotConnected = ((NBTTagByte)data).getByte() > 0;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		try {
			switch (ID) {
			case 0:
				if (this.te().canPlayerModify(sender))
					this.te().setMaximumSpeed(((NBTTagDouble)data).getDouble());
				break;
			case 1:
				if (this.te().canPlayerModify(sender))
					this.te().setTorque(((NBTTagDouble)data).getDouble());
				break;
			case 2:
				if (this.te().canPlayerModify(sender))
					this.te().getConnection().readFromNBTForSettingsGUI((NBTTagCompound) data);
				break;
			}
		} catch (ClassCastException ignored) {}
	}

}
