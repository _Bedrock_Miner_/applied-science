package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.inventory.slots.SlotWaterBucket;
import com.bedrockminer.ascore.tileentity.TileSteamBoiler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.tileentity.TileEntityFurnace;

/**
 * The container for the TileSteamBoiler
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerSteamBoiler extends ContainerBase<TileSteamBoiler> {

	// Synchronized data
	public double water;
	public double pressure;
	public double temperature;
	public double environmentTemperature;
	public double burnMaterialTemperature;
	public int burnTime;
	public ItemStack heatSource;
	// Synchronized connection data
	public boolean pressureOutLeaking;
	public boolean pressureOutNotConnected;
	public NBTTagCompound pressureOutData;

	/**
	 * Instantiates a new ContainerSteamBoiler
	 *
	 * @param te
	 * the TileEntity
	 */
	public ContainerSteamBoiler(TileSteamBoiler te, EntityPlayer player) {
		super(te, player);
	}

	// Inventory

	// 0-2: Fuel, 3: Water, 4-30: Main Inventory, 31-39: Hotbar

	@Override
	protected void initInventorySlots() {
		this.addSlotToContainer(new SlotFurnaceFuel(this.te(), 0, 26, 60));
		this.addSlotToContainer(new SlotFurnaceFuel(this.te(), 1, 44, 60));
		this.addSlotToContainer(new SlotFurnaceFuel(this.te(), 2, 62, 60));
		this.addSlotToContainer(new SlotWaterBucket(this.te(), 3, 12, 22));
		this.addPlayerInventory(8, 94);

		for (int i = 0; i < 3; i++) {
			this.getSlot(i).setBackgroundName("ascore:gui/icons/slot_coal");
		}
		this.getSlot(3).setBackgroundName("ascore:gui/icons/slot_bucket");
	}

	// Synchronization

	@Override
	protected int getSyncFieldCount() {
		return 10;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getWater());
		case 1:
			return new NBTTagDouble(this.te().getOutput().getPressure());
		case 2:
			return new NBTTagDouble(this.te().getTemperature());
		case 3:
			return new NBTTagDouble(this.te().getAmbientTemperature());
		case 4:
			return new NBTTagDouble(this.te().getBurnMaterialTemperature());
		case 5:
			return new NBTTagInt(this.te().getBurnTime());
		case 6:
			NBTTagCompound heatSource = new NBTTagCompound();
			if (this.te().getHeatSource() != null)
				this.te().getHeatSource().writeToNBT(heatSource);
			return heatSource;
		case 7:
			return new NBTTagByte((byte) (this.te().getOutput().getCircuit().isLeaking() ? 1 : 0));
		case 8:
			return new NBTTagByte((byte) (this.te().getOutput().getCircuit().getElementCount() <= 1 ? 1 : 0));
		case 9:
			NBTTagCompound outputConnection = new NBTTagCompound();
			this.te().getOutput().writeToNBTForSettingsGUI(outputConnection);
			return outputConnection;
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch (ID) {
			case 0:
				this.water = ((NBTTagDouble) data).getDouble();
				break;
			case 1:
				this.pressure = ((NBTTagDouble) data).getDouble();
				break;
			case 2:
				this.temperature = ((NBTTagDouble) data).getDouble();
				break;
			case 3:
				this.environmentTemperature = ((NBTTagDouble) data).getDouble();
				break;
			case 4:
				this.burnMaterialTemperature = ((NBTTagDouble) data).getDouble();
				break;
			case 5:
				this.burnTime = ((NBTTagInt) data).getInt();
				break;
			case 6:
				this.heatSource = ItemStack.loadItemStackFromNBT((NBTTagCompound) data);
				break;
			case 7:
				this.pressureOutLeaking = ((NBTTagByte) data).getByte() != 0;
				break;
			case 8:
				this.pressureOutNotConnected = ((NBTTagByte) data).getByte() != 0;
				break;
			case 9:
				this.pressureOutData = (NBTTagCompound) data;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		try {
			switch (ID) {
			case 0:
				if (this.te().canPlayerModify(sender))
					this.te().getOutput().readFromNBTForSettingsGUI((NBTTagCompound) data);
				break;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int index) {
		ItemStack previous = null;
		Slot slot = this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack current = slot.getStack();
			previous = current.copy();

			if (index < 4) {
				if (!this.mergeItemStack(current, 4, 40, true))
					return null;
			} else if (TileEntityFurnace.isItemFuel(current)) {
				if (!this.mergeItemStack(current, 0, 3, false))
					return null;
			} else if (current.getItem() == Items.water_bucket) {
				if (!this.mergeItemStack(current, 3, 4, false))
					return null;
			}

			if (current.stackSize == 0)
				slot.putStack((ItemStack) null);
			else
				slot.onSlotChanged();

			if (current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(player, current);
		}
		return previous;
	}

	// Additional data

	public int getOverallBurnTime() {
		int burnTime = this.burnTime;
		for (int i = 0; i < 3; i++) {
			ItemStack stack = this.getSlot(i).getStack();
			if (stack != null)
				burnTime += TileEntityFurnace.getItemBurnTime(stack) * stack.stackSize;
		}
		return burnTime;
	}

	public double getAdjustTemperature() {
		int ambientFactor = this.pressureOutLeaking ? 10 : 1;
		return (ambientFactor * this.environmentTemperature + 2 * this.burnMaterialTemperature) / (2.0 + ambientFactor);
	}
}
