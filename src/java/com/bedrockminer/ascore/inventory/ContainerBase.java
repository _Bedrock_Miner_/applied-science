package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.AppliedScience;
import com.bedrockminer.ascore.network.packets.PacketGuiSync;
import com.bedrockminer.ascore.tileentity.base.TileCircuitElement;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.nbt.NBTBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * This is the base class for every GUI container in AppliedScience. It takes
 * care for the synchronization, so the user doesn't need to worry about this.
 *
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public abstract class ContainerBase<T extends TileEntity> extends Container {

	/** The TileEntity */
	private T te;
	/** The player */
	private EntityPlayer player;
	/** The last values for the synchronized fields. Used to detect changes */
	private NBTBase[] lastValues;
	/** The last time all values have been synchronized */
	private long lastSyncAll;
	/** The amount of complete syncs in short intervals for startup */
	private int startupSyncs = 5;

	/**
	 * Instantiates a new ContainerBase.
	 *
	 * @param te
	 * the TileEntity
	 */
	public ContainerBase(T te, EntityPlayer player) {
		this.te = te;
		this.lastValues = new NBTBase[this.getSyncFieldCount()];
		this.player = player;
		this.lastSyncAll = player.worldObj.getTotalWorldTime() + 1L;

		this.initInventorySlots();
	}

	/**
	 * Returns the TileEntity this Container uses
	 *
	 * @return the TileEntity
	 */
	public T te() {
		return this.te;
	}

	/**
	 * Returns the player using this container
	 *
	 * @return the player
	 */
	public EntityPlayer player() {
		return this.player;
	}

	/**
	 * This method is called when the container is created to initialize the
	 * slots for this container.
	 */
	protected abstract void initInventorySlots();

	/**
	 * Returns the amount of fields that need to be synchronized with the
	 * client.
	 *
	 * @return the amount of fields to synchronize
	 */
	protected abstract int getSyncFieldCount();

	/**
	 * Returns the field with the given ID encoded as an NBT object. It is
	 * useful to keep the nbt data as small as possible by using nbt primitives
	 * instead of NBTTagCompounds. You should never return <code>null</code>.
	 *
	 * @param ID
	 * the field ID
	 * @return the encoded field data
	 */
	protected abstract NBTBase getField(int ID);

	/**
	 * Called when a packet with data for a synchronized field arrives. This
	 * should be used on client side to cache the data.
	 *
	 * @param ID
	 * the field ID
	 * @param data
	 * the data
	 */
	public abstract void receiveData(int ID, NBTBase data);

	/**
	 * Called when the client notified the Server about something.
	 *
	 * @param ID
	 * the notification ID
	 * @param data
	 * the data
	 * @param sender
	 * the sender of the notification
	 */
	public abstract void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender);

	/**
	 * Sends the content of the field with the given ID to the client.
	 *
	 * @param ID
	 * the ID of the field
	 */
	protected void sendField(int ID) {
		if (this.player().worldObj.isRemote || ID < 0 || ID >= this.getSyncFieldCount())
			return;
		for (ICrafting crafter : this.crafters) {
			if (crafter instanceof EntityPlayerMP)
				AppliedScience.packetHandler.sendTo(new PacketGuiSync(ID, this.getField(ID)), (EntityPlayerMP) crafter);
		}
	}

	/**
	 * Sends a notification from the client to the server.
	 *
	 * @param ID
	 * the notification ID
	 * @param data
	 * the data to send. This must not be null
	 */
	@SideOnly(Side.CLIENT)
	public void sendNotification(int ID, NBTBase data) {
		if (this.player().worldObj.isRemote)
			AppliedScience.packetHandler.sendToServer(new PacketGuiSync(ID, data));
	}

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		if (!this.player().worldObj.isRemote) {
			for (int i = 0; i < this.getSyncFieldCount(); i++) {
				NBTBase now = this.getField(i);
				if (((this.lastValues[i] == null || !now.equals(this.lastValues[i])) && this.player.worldObj.getTotalWorldTime() % 5 == 0) || this.lastSyncAll == this.player().worldObj.getTotalWorldTime()) {
					this.sendField(i);
					this.lastValues[i] = now;
				}
			}

			if (this.lastSyncAll == this.player().worldObj.getTotalWorldTime()) {
				if (this.startupSyncs-- > 0)
					this.lastSyncAll = this.player().worldObj.getTotalWorldTime() + 10L;
				else
					this.lastSyncAll = this.player().worldObj.getTotalWorldTime() + 200L;
			}
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		if (this.te() instanceof IInventory)
			return ((IInventory) this.te()).isUseableByPlayer(player);
		if (this.te() instanceof TileCircuitElement)
			return ((TileCircuitElement) this.te()).canPlayerModify(player);
		return player.getDistanceSq(this.te().getPos()) <= 64;
	}

	/**
	 * Adds the player's inventory at the given position
	 */
	protected void addPlayerInventory(int x, int y) {
		InventoryPlayer inv = this.player().inventory;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				this.addSlotToContainer(new Slot(inv, j + i * 9 + 9, x + j * 18, y + i * 18));
			}
		}
		for (int i = 0; i < 9; i++) {
			this.addSlotToContainer(new Slot(inv, i, x + i * 18, y + 58));
		}
	}
}
