package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.tileentity.TileAirFlowLimiter;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;

/**
 * The Container for the Air Flow Limiter.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerAirFlowLimiter extends ContainerBase<TileAirFlowLimiter> {

	// Synchronized Data
	public double limit;
	public double pressureA;
	public double pressureB;
	public double avgPressure;

	/**
	 * Instantiates a new ContainerAirFlowLimiter.
	 *
	 * @param te
	 * @param player
	 */
	public ContainerAirFlowLimiter(TileAirFlowLimiter te, EntityPlayer player) {
		super(te, player);
	}

	@Override
	protected void initInventorySlots() {
	}

	@Override
	protected int getSyncFieldCount() {
		return 4;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getFlowLimit());
		case 1:
			return new NBTTagDouble(this.te().getConnectionA().getPressure());
		case 2:
			return new NBTTagDouble(this.te().getConnectionB().getPressure());
		case 3:
			if (this.te().getConnectionA().getCircuit().isLeaking() || this.te().getConnectionB().getCircuit().isLeaking())
				return new NBTTagDouble(1);
			return new NBTTagDouble((this.te().getConnectionA().getAirVolume() + this.te().getConnectionB().getAirVolume()) / (this.te().getConnectionA().getVolume() + this.te().getConnectionB().getVolume()));
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch (ID) {
			case 0:
				this.limit = ((NBTTagDouble)data).getDouble();
				break;
			case 1:
				this.pressureA = ((NBTTagDouble)data).getDouble();
				break;
			case 2:
				this.pressureB = ((NBTTagDouble)data).getDouble();
				break;
			case 3:
				this.avgPressure = ((NBTTagDouble)data).getDouble();
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		if (ID == 0 && this.te().canPlayerModify(sender) && data instanceof NBTTagDouble) {
			this.te().setFlowLimit(((NBTTagDouble) data).getDouble());
		}
	}
}
