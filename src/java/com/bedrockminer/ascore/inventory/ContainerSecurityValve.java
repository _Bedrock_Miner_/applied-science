package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.tileentity.TileSecurityValve;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;

/**
 * The Container for the Security Valve.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerSecurityValve extends ContainerBase<TileSecurityValve> {

	// Synchronized Data
	public double treshold;
	public double pressure;

	/**
	 * Instantiates a new ContainerSecurityValve.
	 *
	 * @param te
	 * @param player
	 */
	public ContainerSecurityValve(TileSecurityValve te, EntityPlayer player) {
		super(te, player);
	}

	@Override
	protected void initInventorySlots() {
	}

	@Override
	protected int getSyncFieldCount() {
		return 2;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getPressureTreshold());
		case 1:
			return new NBTTagDouble(this.te().getPressure());
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch (ID) {
			case 0:
				this.treshold = ((NBTTagDouble)data).getDouble();
				break;
			case 1:
				this.pressure = ((NBTTagDouble)data).getDouble();
				break;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		if (ID == 0 && this.te().canPlayerModify(sender) && data instanceof NBTTagDouble) {
			this.te().setPressureTreshold(((NBTTagDouble) data).getDouble());
		}
	}
}
