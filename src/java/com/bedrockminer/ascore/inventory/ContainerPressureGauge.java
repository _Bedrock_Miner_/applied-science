package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.tileentity.TilePressureGauge;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;

/**
 * The Container for the Pressure Gauge.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerPressureGauge extends ContainerBase<TilePressureGauge> {

	// Synchronized fields
	public double pressure;
	public double maxDisplay;
	public boolean redstoneBinary;
	public double minRedstonePressure;
	public double maxRedstonePressure;

	/**
	 * Instantiates a new ContainerPressureGauge.
	 *
	 * @param te
	 * @param player
	 */
	public ContainerPressureGauge(TilePressureGauge te, EntityPlayer player) {
		super(te, player);
	}

	@Override
	protected void initInventorySlots() {
	}

	@Override
	protected int getSyncFieldCount() {
		return 5;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getPressure());
		case 1:
			return new NBTTagDouble(this.te().getGaugeMaximum());
		case 2:
			return new NBTTagByte(this.te().isRedstoneBinary() ? (byte) 1 : (byte) 0);
		case 3:
			return new NBTTagDouble(this.te().getMinRedstonePressure());
		case 4:
			return new NBTTagDouble(this.te().getMaxRedstonePressure());
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch(ID) {
			case 0:
				this.pressure = ((NBTTagDouble)data).getDouble();
				break;
			case 1:
				this.maxDisplay = ((NBTTagDouble)data).getDouble();
				break;
			case 2:
				this.redstoneBinary = ((NBTTagByte)data).getByte() != 0;
				break;
			case 3:
				this.minRedstonePressure = ((NBTTagDouble)data).getDouble();
				break;
			case 4:
				this.maxRedstonePressure = ((NBTTagDouble)data).getDouble();
				break;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		try {
			switch (ID) {
			case 0:
				this.te().setGaugeMaximum(((NBTTagDouble)data).getDouble());
				break;
			case 1:
				this.te().setRedstoneBinary(((NBTTagByte)data).getByte() != 0);
				break;
			case 2:
				this.te().setMinRedstonePressure(((NBTTagDouble)data).getDouble());
				break;
			case 3:
				this.te().setMaxRedstonePressure(((NBTTagDouble)data).getDouble());
			}
		} catch (ClassCastException ignored) {}
	}
}
