package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.tileentity.TileCreativePressureGenerator;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;

/**
 * The Container for the Pressure Generator
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerCreativePressureGenerator extends ContainerBase<TileCreativePressureGenerator> {

	// Synchronized Data
	public double maxPressure;
	public double airOutput;
	public double pressure;

	// Synchronized Connection Data
	public boolean connectionNotConnected;
	public NBTTagCompound connectionData;

	public ContainerCreativePressureGenerator(TileCreativePressureGenerator te, EntityPlayer player) {
		super(te, player);
	}

	@Override
	protected void initInventorySlots() {
	}

	@Override
	protected int getSyncFieldCount() {
		return 5;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getMaximumPressure());
		case 1:
			return new NBTTagDouble(this.te().getAirPerTick());
		case 2:
			return new NBTTagDouble(this.te().getConnection().getPressure());
		case 3:
			return new NBTTagByte((byte) (this.te().getConnection().getCircuit().getElementCount() <= 1 ? 1 : 0));
		case 4:
			NBTTagCompound nbt = new NBTTagCompound();
			this.te().getConnection().writeToNBTForSettingsGUI(nbt);
			return nbt;
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch (ID) {
			case 0:
				this.maxPressure = ((NBTTagDouble) data).getDouble();
				break;
			case 1:
				this.airOutput = ((NBTTagDouble) data).getDouble();
				break;
			case 2:
				this.pressure = ((NBTTagDouble) data).getDouble();
				break;
			case 3:
				this.connectionNotConnected = ((NBTTagByte) data).getByte() != 0;
				break;
			case 4:
				this.connectionData = (NBTTagCompound) data;
				break;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		try {
			switch (ID) {
			case 0:
				if (this.te().canPlayerModify(sender))
					this.te().setMaximumPressure(((NBTTagDouble)data).getDouble());
				break;
			case 1:
				if (this.te().canPlayerModify(sender))
					this.te().setAirPerTick(((NBTTagDouble)data).getDouble());
				break;
			case 2:
				if (this.te().canPlayerModify(sender))
					this.te().getConnection().readFromNBTForSettingsGUI((NBTTagCompound) data);
				break;
			}
		} catch (ClassCastException ignored) {}
	}
}
