package com.bedrockminer.ascore.inventory;

import com.bedrockminer.ascore.tileentity.TilePressureReducer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagInt;

/**
 * The Container for the Pressure Reducer.
 * @author _Bedrock_Miner_ (minerbedrock@gmail.com)
 */
public class ContainerPressureReducer extends ContainerBase<TilePressureReducer> {

	// Synchronized Data
	public double limit;
	public double pressureIn;
	public double pressureOut;

	/**
	 * Instantiates a new ContainerPressureReducer.
	 *
	 * @param te
	 * @param player
	 */
	public ContainerPressureReducer(TilePressureReducer te, EntityPlayer player) {
		super(te, player);
	}

	@Override
	protected void initInventorySlots() {
	}

	@Override
	protected int getSyncFieldCount() {
		return 3;
	}

	@Override
	protected NBTBase getField(int ID) {
		switch (ID) {
		case 0:
			return new NBTTagDouble(this.te().getPressureLimit());
		case 1:
			return new NBTTagDouble(this.te().getInput().getPressure());
		case 2:
			return new NBTTagDouble(this.te().getOutput().getPressure());
		}
		return new NBTTagInt(0);
	}

	@Override
	public void receiveData(int ID, NBTBase data) {
		try {
			switch (ID) {
			case 0:
				this.limit = ((NBTTagDouble)data).getDouble();
				break;
			case 1:
				this.pressureIn = ((NBTTagDouble)data).getDouble();
				break;
			case 2:
				this.pressureOut = ((NBTTagDouble)data).getDouble();
				break;
			}
		} catch (ClassCastException ignored) {}
	}

	@Override
	public void receiveNotification(int ID, NBTBase data, EntityPlayerMP sender) {
		if (ID == 0 && this.te().canPlayerModify(sender) && data instanceof NBTTagDouble) {
			this.te().setPressureLimit(((NBTTagDouble) data).getDouble());
		}
	}
}
