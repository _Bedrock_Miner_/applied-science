package com.bedrockminer.ascore.inventory.slots;

import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotWaterBucket extends Slot {

	public SlotWaterBucket(IInventory inventory, int index, int xPosition, int yPosition) {
		super(inventory, index, xPosition, yPosition);
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return stack.getItem() == Items.water_bucket || stack.getItem() == Items.bucket;
	}

	@Override
	public int getItemStackLimit(ItemStack stack) {
		return 1;
	}
}
