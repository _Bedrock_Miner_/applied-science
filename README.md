Applied Science
===============

This is the source code of the Applied Science mod.  
It is licenced under GPLv3, so have fun with it.

The mod is in a very early stage of development, so it will take some time until
I will release it.

---------------

If you are interested, you can take a look at the code and
maybe contribute to it via pull requests. If you have questions about the mod you
can email me at <minerbedrock@gmail.com>.

I know that this mod currently does next to nothing, but slowly it will become
an interesting mod with multiple great features.  
I will also *try* to create an API once the mod is bigger to allow addons to be
created.
